// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import mediaQueries from "@commonground/design-system/dist/mediaQueries";
import styled, { keyframes, css } from "styled-components";

interface ToasterProps {
  baseHeight: number;
  nth: number;
  unmount: boolean;
  variant: "success" | "error";
  ref: any;
}

const onMount = keyframes`
  0% { transform: translateX(100px); opacity:0;}
  100% { transform: none; opacity:1;}
`;

const onUnmount = keyframes`
  0% { transform: none; opacity:1;}
  100% { transform: translateX(100px); opacity:0;}
`;

export const Toaster = styled.dialog<ToasterProps>`
  animation: ${({ unmount }) => (unmount ? onUnmount : onMount)};
  animation-duration: ${({ unmount }) => (unmount ? "0.6s" : "0.3s")};
  background-color: ${({ variant, theme: { tokens } }) =>
    variant === "success" ? tokens.colorSuccessLight : tokens.colorErrorLight};
  border: 0;
  border-left: 0.25rem solid
    ${({ variant, theme: { tokens } }) =>
      variant === "success" ? tokens.colorSuccess : tokens.colorError};
  bottom: ${({ baseHeight = 0, nth = 0 }) => 32 + (16 + baseHeight) * nth}px;
  box-shadow: rgb(0 0 0 / 25%) 3px 4px 6px 0px;
  display: flex;
  margin: 0 0 0 auto;
  padding: 1rem 1rem 1rem 3rem;
  position: fixed;
  right: 2rem;
  width: 400px;
  z-index: 100000;

  p:first-child {
    font-weight: 700;
    margin: 0;
    font-size: 1rem;
  }

  > svg {
    :first-child {
      position: absolute;
      left: 1rem;
      color: ${({ variant, theme: { tokens } }) =>
        variant === "success" ? tokens.colorSuccess : tokens.colorError};
    }
    :last-child {
      cursor: pointer;
      margin-left: auto;
    }
  }
`;
