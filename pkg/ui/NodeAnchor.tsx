// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { UiNodeAnchorAttributes } from "@ory/client";
import { UiNode } from "@ory/client";
import { Button } from "@commonground/design-system";
import { t } from "../../translations";

interface Props {
  node: UiNode;
  attributes: UiNodeAnchorAttributes;
}

export const NodeAnchor = ({ node, attributes }: Props) => {
  return (
    <Button
      data-testid={`node/anchor/${attributes.id}`}
      onClick={(e) => {
        e.stopPropagation();
        e.preventDefault();
        window.location.href = attributes.href;
      }}
    >
      {t(attributes.title.text)}
    </Button>
  );
};
