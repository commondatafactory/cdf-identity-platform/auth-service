// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Box, Dialog, DialogContent, IconButton } from "@mui/material";
import { Icon } from "@commonground/design-system";
import { useEffect, useState } from "react";

import { createUser } from "../../services/create-user";
import { CreateUserForm, Values } from "./CreateUserForm";
import { getUsers } from "../../services/get-users";
import { isEmailValid } from "../../services/check-email-validity";
import { updatePermissions } from "../../services/update-permissions";
import { getSubdivisionsForActiveOrganization } from "../../utils/filters";
import { t } from "../../../translations";

import addUserIcon from "../../../public/images/user-add-line.svg";
import CloseIcon from "@mui/icons-material/Close";

import authModel from "../../../authorization-model.json";

interface FormValues {
  label: string;
  value: string;
}

export const CreateUserModal = ({
  isActive,
  onConfirm,
  onCancel,
  currentUser,
}) => {
  const [formError, setFormError] = useState("");

  const [grantableOrganizations, setGrantableOrganizations] = useState<
    FormValues[]
  >([]);
  const [grantableData, setGrantableData] = useState<FormValues[]>([]);

  useEffect(() => {
    // NOTE: filter to get active user organization
    const filteredUserOrganizations = currentUser.permissions.filter(
      (permission) =>
        permission.namespace === authModel.organizations.namespace &&
        permission.relation === authModel.organizations.relations.access
    );

    const mappedAvailableAdministrators = filteredUserOrganizations.map(
      ({ label, id }) => ({
        label,
        value: id,
      })
    );

    // TODO: don't map too early, map when needed
    setGrantableOrganizations(mappedAvailableAdministrators);

    if (!filteredUserOrganizations.length) {
      return;
    }

    // NOTE: filter to get relevant themes based on active organization
    const filteredAvailableRoles = getSubdivisionsForActiveOrganization(
      filteredUserOrganizations[0].object,
      currentUser.permissions
    );

    const mappedAvailableRoles = filteredAvailableRoles.map(
      ({ label, id }) => ({
        label,
        value: id,
      })
    );

    setGrantableData(mappedAvailableRoles);
  }, []);

  const handleSubmit = async (values: Values, isExistingUser) => {
    // TODO: values can include an undefined value and be submitted. this should not be possible.
    // TODO: Refactor front-end and include back-end validation messages

    setFormError("");

    const isValidMail = isEmailValid(String(values?.email || ""));

    if (!isValidMail) {
      setFormError(
        "Het opgegeven e-mailadres wordt niet herkent als valide e-mailadres."
      );
      return;
    }

    if (!isExistingUser && (!values.firstName || !values.surname)) {
      setFormError("Vul de naam in van de aan te maken gebruiker.");
      return;
    }

    const rolePermissions =
      values.data?.map((value) => ({
        namespace: authModel.subdivisions.namespace,
        object: value,
        relation: authModel.subdivisions.relations.issuer,
      })) || [];

    if (isExistingUser) {
      const query = new URLSearchParams({ email: values.email }).toString();
      const getExistingUser = (await getUsers(query))?.[0];

      if (!getExistingUser) {
        setFormError(
          "Kon momenteel de gebruiker niet ophalen. Probeer het later nog eens."
        );
        return;
      }

      const updatedPermissions = await updatePermissions({
        permissions: rolePermissions.map((permission) => ({
          ...permission,
          subject_id: getExistingUser.id,
        })),
        userId: getExistingUser.id,
      });

      if (updatedPermissions?.error) {
        setFormError(
          "Kon momenteel de gebruiker niet updaten. Probeer het later nog eens."
        );
        return;
      }

      setFormError("");
      onConfirm(true);
      return;
    }

    const traits = {
      email: values.email,
      name: {
        first: values.firstName,
        last: values.surname,
      },
    };

    values.administrators = [grantableOrganizations?.[0]?.value];

    const administratorPermissions = values.administrators.map(
      (organization) => ({
        namespace: authModel.organizations.namespace,
        object: organization,
        relation: authModel.organizations.relations.issuer,
      })
    );

    const grantPermissions = [...rolePermissions, ...administratorPermissions];

    const newUser = await createUser({
      traits,
      permissions: grantPermissions,
    });

    if (!newUser || newUser?.error) {
      setFormError(
        newUser?.error?.reason
          ? t(newUser.error.reason)
          : "Fout bij het aanmaken van een nieuwe gebruiker. Als dit aanhoudt, neem dan contact op met een systeem beheerder."
      );
      return;
    }

    setFormError("");
    onConfirm(false);
  };

  const handleClose = () => {
    onCancel();
    setFormError("");
  };

  return (
    <Dialog
      open={isActive}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth="lg"
      fullWidth
    >
      <IconButton
        aria-label="close"
        onClick={handleClose}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>

      <DialogContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            m: "auto",
            width: "640px",
            paddingTop: "24px",
            paddingBottom: "24px",
          }}
        >
          <h2>
            <Icon size="x-large" as={addUserIcon} /> Nieuwe gebruiker toevoegen
          </h2>

          <CreateUserForm
            onSubmit={handleSubmit}
            grantableOrganizations={grantableOrganizations}
            grantableData={grantableData}
            currentUser={currentUser}
            handleClose={handleClose}
            formError={formError}
            editUser={null}
            setFormError={setFormError}
          />
        </Box>
      </DialogContent>
    </Dialog>
  );
};
