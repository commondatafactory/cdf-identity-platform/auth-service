// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled, { css } from "styled-components";
import Link from "next/link";

const styling = css`
  color: #757575;
  cursor: pointer;
  display: block;
  margin-left: auto;
  margin-top: 1rem;
  text-decoration: none;
  width: fit-content;
`;

export const OtherLinkExternal = styled.a`
  ${styling}
`;

export const OtherLink = styled(Link)`
  ${styling}
`;
