// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { fetchWithTimeout } from "./fetchWithTimeout";

export const getUsers = async (query) => {
  try {
    const currentUserResponse = await fetchWithTimeout(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users?${query}`,
      {
        credentials: "include",
      }
    );

    if (!currentUserResponse.ok) {
      throw currentUserResponse.statusText
    }

    const users = await currentUserResponse.json();

    // Note: filter duplicate permissions
    return users.map((identity) => ({
      ...identity,
      permissions: identity.permissions.filter(
        (currentPermission, i, array) =>
          i ===
          array.findIndex(
            (permission) =>
              permission.relation === currentPermission.relation &&
              permission.object === currentPermission.object
          )
      ),
    }));
  } catch (e) {
    console.error("Get users error", e);
    return null;
  }
};
