// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { fetchWithTimeout } from "./fetchWithTimeout";

export const getUser = async (id) => {
  try {
    const currentUserResponse = await fetchWithTimeout(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users/${id}`,
      {
        credentials: "include",
      }
    );
    if (!currentUserResponse.ok) {
      return;
    }

    return await currentUserResponse.json();
  } catch (e) {
    console.log("Get user error", e);
  }
};
