// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import Head from "next/head";
import { Button } from "@commonground/design-system";
import { FormProvider } from "react-hook-form";
import { SingleTextInput } from "@commonground/design-system/";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { SettingsFlow } from "@ory/client";

import ory from "../../pkg/sdk";
import { CurrentUserSession } from "../types";
import { handleFlowError } from "../../pkg/errors";
import { useToaster } from "../providers/ToasterProvider";
import { t } from "../../translations";
import { NextPageWithLayout } from "./_app";
import { SidebarLayout } from "../components/layout/SidebarLayout";
import { UsePasswordSchema, schema } from "../hooks/use-password-schema";
import { ProgressIndicators } from "../components/progress-indicators/ProgressIndicators";
import {
  recoveryProgressions,
  verifyProgressions,
} from "../services/page-progressions";
import {
  isCurrentUserTwoFactorConfigured,
  isUserTwoFactorRequired,
} from "../services/user-passes-two-factor-check";
import { FormContainer } from "../components/layout/SmallFormContainer.styled";
import {
  getCsrfToken,
  getErrorMessage,
} from "../services/kratos-flow-serializer";

import PasswordHiddenIcon from "../../public/images/eye-fill.svg";
import PasswordVisibleIcon from "../../public/images/eye-off-fill.svg";
import { AxiosError } from "axios";

interface Values {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
}

const Page: NextPageWithLayout<{
  currentUser: CurrentUserSession;
  handleWhoami: () => Promise<CurrentUserSession>;
}> = ({ currentUser, handleWhoami }) => {
  const methods = UsePasswordSchema();

  const [flow, setFlow] = useState<SettingsFlow>();

  const [flowId, setFlowId] = useState("");
  const [inputError, setInputError] = useState("");

  const [values, setValues] = useState({} as Values);
  const [isLoading, setIsLoading] = useState(false);

  const [config, setConfig] = useState({
    setPasswordHidden: true,
    confirmPasswordHidden: true,
  });

  const [csrfToken, setCsrfToken] = useState();
  const [has2faConfigured, setHas2faConfigured] = useState<boolean | null>(
    null
  );

  // Get ?flow=... from the URL
  const router = useRouter();
  const { flow: flowIdUrl, return_to: returnTo, screen } = router.query;

  const { showToast } = useToaster();

  useEffect(() => {
    (async () => {
      const isTwoFactorConfigured = await isCurrentUserTwoFactorConfigured();

      if (isTwoFactorConfigured) {
        setHas2faConfigured(!!isTwoFactorConfigured.totp);
      }
    })();
  }, []);

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady) {
      return;
    }

    // // If ?flow=.. was in the URL, we fetch it
    if (flowIdUrl) {
      ory.frontend
        .getSettingsFlow({ id: String(flowIdUrl) })
        .then(({ data }) => {
          const csrfToken = getCsrfToken(data);
          if (!csrfToken) {
            console.log("failed to get CSRF token");
            return;
          }

          setCsrfToken(csrfToken);
          setFlow(data);
        })
        .catch((err) => {
          if (err.response?.data.error?.id === "session_aal2_required") {
            location.href = `/login?aal=aal2&return_to=${location.href}`;
            return;
          }
          return Promise.reject(err);
        })
        .catch(handleFlowError(router, "settings", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserSettingsFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        const csrfToken = getCsrfToken(data);
        if (!csrfToken) {
          console.log("failed to get CSRF token");
          return;
        }

        setFlow(data);
        setCsrfToken(csrfToken);
        setFlowId(data.id);
      })
      .catch((err) => {
        if (err.response?.data.error?.id === "session_aal2_required") {
          location.href = encodeURIComponent(`/login?aal=aal2`);
          // location.href = encodeURIComponent(
          //   `/login?aal=aal2&return_to=/${location.pathname}`
          // );
          return;
        }
        return Promise.reject(err);
      })
      .catch(handleFlowError(router, "settings", setFlow));
  }, [router.isReady]);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    setInputError("");

    try {
      schema.validateSync(values);
    } catch (e) {
      setInputError(e.errors[0]);
      return;
    }

    if (isLoading) {
      return Promise.resolve();
    }

    setIsLoading(true);

    if (!values.password) {
      return;
    }

    const body = {
      csrf_token: csrfToken,
      method: "password",
      password: values.password,
    } as const;

    await router.push(
      `?flow=${flowId}${screen ? `&screen=${screen}` : ""}`,
      undefined,
      {
        shallow: true,
      }
    );

    ory.frontend
      .updateSettingsFlow({
        flow: String(flow?.id),
        updateSettingsFlowBody: body,
      })
      .then(async ({ data }) => {
        showToast({
          title: "Gegevens aangepast",
          variant: "success",
        });

        const userSession = await handleWhoami();

        if (!userSession) {
          return;
        }

        if (isUserTwoFactorRequired({ user: userSession })) {
          if (has2faConfigured === true) {
            location.href = "/login?aal=aal2";
            return;
          }

          location.href = `/settings/2fa-app${
            screen ? `?screen=${screen}` : ""
          }`;
          return;
        }

        location.href = `/recovery-done?screen=${screen}`;
        return;
      })
      .catch(handleFlowError(router, "settings", setFlow))
      .catch(async (err: AxiosError) => {
        // If the previous handler did not catch the error it's most likely a form validation error
        if (err.response?.status === 400) {
          const getInputError = getErrorMessage(err);
          setInputError(getInputError);
          return { error: getInputError };
        }

        return;
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const handleConfig = (event: InputEvent, key: string, value: unknown) => {
    event.stopPropagation();
    setConfig({ ...config, [key]: value });
  };

  return (
    <>
      <Head>
        <title>Account instelling wijzigen</title>
        <meta name="description" content="Gegevens aanpassen" />
      </Head>

      <ProgressIndicators
        progressions={
          screen === "verify"
            ? verifyProgressions
            : screen === "password-recovery"
            ? recoveryProgressions
            : []
        }
        currentprogress={1}
      />

      <FormProvider {...methods}>
        <FormContainer>
          <form>
            <h1>Wachtwoord instellen</h1>

            <p>
              Vul hier een eigen wachtwoord in om toegang tot je account te
              krijgen.
            </p>

            <label>
              <p>Nieuw wachtwoord</p>
              <SingleTextInput
                onChange={(e) =>
                  setValues({ ...values, password: e.target.value })
                }
                type={config.setPasswordHidden ? "password" : "text"}
                name="password"
                value={values.password || ""}
                disabled={isLoading}
                iconAtEnd={
                  config.setPasswordHidden
                    ? PasswordHiddenIcon
                    : PasswordVisibleIcon
                }
                handleIconAtEnd={(event) =>
                  handleConfig(
                    event,
                    "setPasswordHidden",
                    !config.setPasswordHidden
                  )
                }
                style={{ width: "100%" }}
              />
            </label>

            <label>
              <p>Wachtwoord bevestigen</p>
              <SingleTextInput
                onChange={(e) =>
                  setValues({ ...values, confirmPassword: e.target.value })
                }
                type={config.confirmPasswordHidden ? "password" : "text"}
                name="confirmPassword"
                value={values.confirmPassword || ""}
                disabled={isLoading}
                iconAtEnd={
                  config.confirmPasswordHidden
                    ? PasswordHiddenIcon
                    : PasswordVisibleIcon
                }
                handleIconAtEnd={(event) =>
                  handleConfig(
                    event,
                    "confirmPasswordHidden",
                    !config.confirmPasswordHidden
                  )
                }
                style={{ width: "100%" }}
              />
            </label>

            <p>
              Een wachtwoord moet minimaal uit 8 tekens bestaan en minstens een
              hoofdletter en teken bevatten.
            </p>

            {inputError && <p style={{ color: "red" }}>{t(inputError)}</p>}

            <Button
              onClick={(e: React.FormEvent) => handleSubmit(e)}
              disabled={isLoading}
              type="submit"
              style={{ marginLeft: "auto" }}
            >
              Wachtwoord instellen
            </Button>
          </form>
        </FormContainer>
      </FormProvider>
    </>
  );
};

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <SidebarLayout>{page}</SidebarLayout>;
};

export default Page;
