// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Session } from "@ory/client";

export type UUID = `${string}-${string}-${string}-${string}-${string}`;

export type PermissionMeta = {
  namespace: "organizations" | "subdivisions" | "data-sources";
  object: string;
  relation: "access" | "issuer" | "admin";
  subject_id: string;
  id: string;
  label: string;
  type: string;
};

export interface CurrentUserSession extends Session {
  permissions: [
    {
      namespace: string;
      object: string;
      relation: string;
      subject_id: string;
      id: string;
      label: string;
      type: string;
    }
  ];
  userType: {
    id: string;
    type: string;
    label: string;
  };
  sortedPermissions: {
    organizations?: {
      access?: PermissionMeta[];
      member?: PermissionMeta[];
    };
    subdivisons?: {
      access?: PermissionMeta[];
      member?: PermissionMeta[];
    };
  };
}
