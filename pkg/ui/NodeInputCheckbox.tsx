// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { NodeInputProps } from "./helpers";
import { getNodeLabel } from "@ory/integrations/ui";
import { Checkbox } from "@ory/themes";

export function NodeInputCheckbox<T>({
  node,
  attributes,
  setValue,
  disabled,
}: NodeInputProps) {
  return (
    <>
      <Checkbox
        name={attributes.name}
        defaultChecked={attributes.value === true}
        onChange={(e) => setValue(e.target.checked)}
        disabled={attributes.disabled || disabled}
        label={getNodeLabel(node)}
        state={
          node.messages.find(({ type }) => type === "error")
            ? "error"
            : undefined
        }
        subtitle={node.messages.map(({ text }) => text).join("\n")}
      />
    </>
  );
}
