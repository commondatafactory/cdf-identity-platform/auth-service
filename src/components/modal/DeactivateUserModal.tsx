// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Spinner } from "@commonground/design-system";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

export const DeactivateUserModal = ({
  isActive,
  selectedRowId,
  onConfirm,
  onCancel,
  handleClose = undefined,
  isLoading = false,
}) => (
  <Dialog
    open={isActive}
    onClose={handleClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">Gebruiker blokkeren</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        Weet je zeker dat je de gebruiker wil blokkeren?
      </DialogContentText>
    </DialogContent>

    <DialogActions>
      <Button variant="secondary" onClick={onCancel}>
        Annuleren
      </Button>
      <Button onClick={onConfirm} autoFocus>
        Blokkeren
      </Button>
    </DialogActions>
  </Dialog>
);
