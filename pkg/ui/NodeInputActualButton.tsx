// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { Button } from "@commonground/design-system";
import { getNodeLabel } from "@ory/integrations/ui";

import { FormEvent } from "react";
import { t } from "../../translations";
import { NodeInputProps } from "./helpers";

export function NodeInputActualButton<T>({
  node,
  attributes,
  disabled,
}: NodeInputProps) {
  const onClick = (e: MouseEvent | FormEvent) => {
    e.stopPropagation();
    e.preventDefault();

    (attributes as any).onclick();
  };

  return (
    <>
      <Button
        name={attributes.name}
        onClick={onClick}
        variant="secondary"
        value={attributes.value || ""}
        disabled={attributes.disabled || disabled}
        style={(attributes as any).style}
      >
        {t(getNodeLabel(node))}
      </Button>
    </>
  );
}
