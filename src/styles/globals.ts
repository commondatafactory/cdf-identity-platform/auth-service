// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    #__next {
        height: 100vh;
    }

    * {
        box-sizing: border-box;
    }

    h1, h2 {
        margin-bottom: 0;
    }

    ul {
        padding-left: 12px;
        margin: 0;
    }

    * + p {
        margin-top: 0.5rem;
    }

    /* Form styling */
    form fieldset {
        padding: 0;
        margin-top: 1.5rem;
    }

    form > button {
        margin-top: 1.5em;
    }

    form label > p {
        margin-top: 1rem;
    }
`;
