// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const verifyProgressions = [
  { title: "Account verifieëren" },
  { title: "2FA configureren" },
  { title: "Klaar!" },
];

export const recoveryProgressions = [
  { title: "Account verifieëren" },
  { title: "Wachtwoord instellen" },
  { title: "2FA configureren" },
  { title: "Klaar!" },
];
