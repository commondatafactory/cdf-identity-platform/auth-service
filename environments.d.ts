declare global {
  namespace NodeJS {
    interface ProcessEnv {
      TARGET_ENV: "development" | "staging" | "production";
    }
  }
}
