// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { FC, useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { Identity } from "@ory/client";

import { LogoutLink } from "../../../pkg";
import { CurrentUserSession } from "../../types";
import { getUsers } from "../../services/get-users";

import * as Styled from "./UserContainer.styled";

import LogoutIcon from "../../../public/images/logout-box-r-line.svg";
import MenuAddIcon from "../../../public/images/menu-add-line.svg";
import SettingsIcon from "../../../public/images/settings-4-line.svg";
import TableIcon from "../../../public/images/table-line.svg";
import VngIcon from "../../../public/images/logo.svg";
import MailIcon from "../../../public/images/mail-inline.svg";

interface UserContainer {
  currentUser: CurrentUserSession;
  title: string;
  description: string;
}

export const UserContainer: FC<any> = ({ currentUser, title, description }) => {
  const router = useRouter();
  const { refresh, aal } = router.query;

  const [administrators, setAdministrators] = useState([] as Identity[]);

  const onLogout = LogoutLink([aal, refresh]);

  const currentUserActiveOrganization = currentUser?.permissions?.find(
    ({ namespace, relation }) =>
      namespace === "organizations" && relation === "access"
  );

  const isCurrentUserAdminOrganization =
    currentUserActiveOrganization?.object &&
    currentUser?.permissions.some(
      ({ namespace, relation, object }) =>
        namespace === "organizations" &&
        relation === "admin" &&
        object === currentUserActiveOrganization.object
    );

  useEffect(() => {
    if (!currentUserActiveOrganization) {
      setAdministrators([]);
      return;
    }

    const query = new URLSearchParams({
      namespace: "organizations",
      object: currentUserActiveOrganization.id,
    }).toString();

    getUsers(query).then((users) => {
      if (!users) {
        return;
      }
      const administrators = users.filter(
        (user) =>
          user.userType.id === "administrator" &&
          currentUser.identity.id !== user.id
      );

      setAdministrators(administrators);
    });
  }, [currentUser]);

  return (
    <Styled.Container>
      <Styled.LogoWrapper>
        <VngIcon viewBox="0 0 33 36" />
        <p>{title}</p>
        <small>{description}</small>
      </Styled.LogoWrapper>

      {currentUser?.identity?.traits ? (
        <Styled.LinksWrapper>
          <p>Welkom bij het permissieportaal</p>
          <p>
            Laatste login:{" "}
            {new Date(currentUser.issued_at).toLocaleDateString("nl-NL", {
              weekday: "long",
              year: "numeric",
              month: "long",
              day: "numeric",
              hour: "numeric",
              minute: "numeric",
            })}
            .
          </p>

          <p style={{ fontSize: 28, fontWeight: 700 }}>
            {currentUser.identity.traits.name.first}{" "}
            {currentUser.identity.traits.name.last}
          </p>
          {currentUser.sortedPermissions.organizations?.access && (
            <>
              <p>
                Je bent ingelogd{" "}
                {isCurrentUserAdminOrganization ? (
                  <strong>als beheerder </strong>
                ) : (
                  ""
                )}
                voor gemeente
              </p>
              <p style={{ fontSize: 28, fontWeight: 700, lineHeight: 1.2 }}>
                {currentUser.sortedPermissions.organizations.access[0].label}
              </p>
            </>
          )}

          {isCurrentUserAdminOrganization && (
            <Link href="/" passHref>
              <TableIcon width={20} height={20} viewBox="0 0 24 24" />
              Administratie dashboard
            </Link>
          )}
          <Link href="/permissies" passHref>
            <MenuAddIcon width={20} height={20} viewBox="0 0 24 24" />
            Datavoorzieningen
          </Link>
          <Link href="/settings" passHref>
            <SettingsIcon width={20} height={20} viewBox="0 0 24 24" />
            Account instellingen
          </Link>
          <a
            data-testid="logout-link"
            onClick={onLogout}
            style={{
              cursor: "pointer",
              textDecoration: "underline",
            }}
          >
            <LogoutIcon width={20} height={20} viewBox="0 0 24 24" />
            Uitloggen
          </a>

          {administrators.length ? (
            <>
              <p className="heading">
                <strong>Heb je een vraag? </strong>
              </p>

              {administrators.slice(0, 2).map((admin) => (
                <a href={admin.traits?.email}>
                  <MailIcon width={20} height={20} viewBox="0 0 24 24" />
                  {admin.traits?.email || ""}
                </a>
              ))}
            </>
          ) : null}

          <p className="heading">
            <strong>Voor feedback en foutmeldingen</strong>
          </p>

          <a href="mailto:vip@vng.nl">
            <MailIcon width={20} height={20} viewBox="0 0 24 24" />
            vip@vng.nl
          </a>
        </Styled.LinksWrapper>
      ) : (
        <></>
      )}
    </Styled.Container>
  );
};
