// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Icon } from "@commonground/design-system";
import { MenuItem, InputAdornment } from "@mui/material";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Tooltip from "@mui/material/Tooltip";
import React, { useEffect, useState } from "react";

import * as Styled from "./DataTable.styled";

import DeleteIcon from "../../../public/images/close.svg";
import EditIcon from "../../../public/images/edit-2-fill.svg";
import BlockIcon from "../../../public/images/lock-2-fill.svg";
import UnblockIcon from "../../../public/images/lock-unlock-fill.svg";
import { getSubdivisionsForActiveOrganization } from "../../utils/filters";

type IdentitiesTableData = {
  name;
  mail;
  permissions;
  datecreated;
  blocked;
  id;
  isAnalyst;
  role;
  userType;
  organizations;
};


const dateOptions: any = {
  weekday: "long",
  year: "numeric",
  month: "long",
  day: "numeric",
};


const handleFormIdentities = ({
  identities,
  currentUserActiveOrganization,
}): any[] => {
  if (!identities) {
    return [];
  }

  const formattedIdentities = identities.map((identity) => {
    const roleLabels = getSubdivisionsForActiveOrganization(
      currentUserActiveOrganization?.object,
      identity?.permissions
    ).reduce((acc, permission) => [...acc, permission.label], []);

    const dateCreated = new Date(identity.created_at).toLocaleDateString(
      "nl-NL",
      dateOptions
    );

    const { first = "", last = "" } = identity.traits.name || {};
    const userType = identity.userType.id === "administrator" ? "Admin" : "Onderzoeker"

    const formattedFields = {
      name: `${first} ${last}`,
      mail: identity.traits.email,
      permissions: roleLabels,
      datecreated: dateCreated,
      realdate: identity.created_at, // to make sorting work.
      blocked: identity.state !== "inactive",
      id: identity.id,
      isAnalyst: identity.userType.id === "analyst",
      role: identity.userType.id,
      userType
    };

    return formattedFields;
  });

  return formattedIdentities;
};

const handleSort = (sortedTable, sortOrder) => {
  const sortFunctions = {
    "Account aangemaakt": (a, b) => a.realdate < b.realdate,
    "Type gebruiker": (a, b) => (a.role === "administrator" ? -1 : 1),
    "E-mailadres": (a, b) => a.mail.localeCompare(b.mail),
    Naam: (a, b) => a.name.localeCompare(b.name),
  };

  return [...sortedTable].sort(sortFunctions[sortOrder] || (() => 0));
};

const sortOptions = [
  "Naam",
  "E-mailadres",
  "Type gebruiker",
  "Account aangemaakt",
] as const;

const columns = [
  "Naam",
  "E-mailadres",
  "Type gebruiker",
  "Datarol",
  "Aangemaakt",
  "Acties",
];

export const DataTable = ({
  identities,
  setModalActive,
  setSelectedRowId,
  currentUserActiveOrganization,
}) => {
  const [sortOrder, setSortOrder] =
    useState<(typeof sortOptions)[number]>("Type gebruiker");
  const [searchValue, setSearchValue] = useState("");
  const [sortedTable, setSortedTable] = useState<IdentitiesTableData[]>([]);
  const [identitiesTableData, setIdentitiesTableData] =
    useState<IdentitiesTableData[]>([]);

    const actions = [
      {
        title: "Bewerken",
        icon: EditIcon,
        event: () => setModalActive("edit-user"),
      },
      {
        title: "Blokkeer",
        icon: BlockIcon,
        event: () => setModalActive("deactivate"),
        checkIfVisible: (row => row.blocked),
      },
      {
        title: "Deblokkeer",
        icon: UnblockIcon,
        event: () => setModalActive("activate"),
        checkIfVisible: (row => !row.blocked),
        secondary: true
      },
      {
        title: "Verwijderen",
        icon: DeleteIcon,
        event: () => setModalActive("delete"),
      },
    ];

  useEffect(() => {
    const formFields = handleFormIdentities({
      identities,
      currentUserActiveOrganization,
    });
    setIdentitiesTableData(formFields);
  }, [identities]);

  useEffect(() => {
    if (!identitiesTableData) {
      return;
    }

    const updatedSortedTable = handleSort(identitiesTableData, sortOrder);
    setSortedTable(updatedSortedTable);
  }, [identitiesTableData, sortOrder]);

  const searchFilter = (value) => {
    if (searchValue === "") {
      return value;
    } else if (
      value.name
        .toLowerCase()
        .includes(searchValue.toLocaleLowerCase()) ||
      value.mail
        .toLowerCase()
        .includes(searchValue.toLocaleLowerCase())
    ) {
      return value;
    }
  }

  return (
    <>
      <Styled.Wrapper>
        <Styled.Label htmlFor="">Sorteer op</Styled.Label>
        <Styled.StyledSelect
          value={sortOrder}
          onChange={(e) =>
            setSortOrder(e.target.value as (typeof sortOptions)[number])
          }
        >
          {sortOptions.map((label) => (
            <MenuItem key={label} value={label}>
              {label}
            </MenuItem>
          ))}
        </Styled.StyledSelect>
        <Styled.StyledOutlineInput
          type="text"
          placeholder="Zoek gebruiker"
          id="input-with-icon-adornment"
          onChange={(e) => {
            setSearchValue(e.target.value);
          }}
          endAdornment={
            <InputAdornment position="end" />
          }
        />
      </Styled.Wrapper>
      <TableContainer component={Paper}>
        <Table aria-label="data table" role="table">
          <Styled.TableHead>
            <TableRow role="row">
              {columns.map((column) => (
                <Styled.TableHeader role="rowheader" key={column}>
                  <h3>{column}</h3>
                </Styled.TableHeader>
              ))}
            </TableRow>
          </Styled.TableHead>
          <TableBody>
            {sortedTable
              .filter(searchFilter)
              .map((row, i) => (
                <Styled.TableRow
                  key={row.mail + i}
                  className={row.blocked ? "verified" : "unverified"}
                >
                  <Styled.TableRowCell scope="row" role="cell">
                    <p>{row.name}</p>
                  </Styled.TableRowCell>

                  <Styled.TableRowCell role="cell">
                    <p>{row.mail}</p>
                  </Styled.TableRowCell>

                  {row.organizations && (
                    <Styled.TableRowCell role="cell">
                      {row.organizations.map((administration, i) => (
                        <Styled.Tag key={i}>{administration}</Styled.Tag>
                      ))}
                    </Styled.TableRowCell>
                  )}

                  {row.userType && (
                    <Styled.TableRowCell role="cell">
                      <Styled.Tag
                        className={`${
                          row.role === "administrator" ? "active" : ""
                        }`}
                      >
                        {row.userType}
                      </Styled.Tag>
                    </Styled.TableRowCell>
                  )}
                  <Styled.TableRowCell role="cell">
                    {row.permissions?.map((permission, i) => (
                      <Styled.Tag key={i}>{permission}</Styled.Tag>
                    ))}
                  </Styled.TableRowCell>

                  <Styled.TableRowCell role="cell">
                    <p>{row.datecreated}</p>
                  </Styled.TableRowCell>

                  <Styled.TableRowCell role="cell">
                    { actions
                        .filter(({checkIfVisible}) => checkIfVisible ? checkIfVisible(row) : true)
                        .map((action) => (
                          <Tooltip
                            title={action.title}
                            placement="left"
                            enterDelay={400}
                            key={action.title}
                          >
                            <Button
                              classname="tableCellIcons"
                              variant={
                                action.secondary
                                  ? "secondary"
                                  : "primary"
                              }
                              onClick={() => {
                                setSelectedRowId(row.id);
                                action.event();
                              }}
                            >
                              {" "}
                              <Icon as={action.icon} viewBox="0 0 24 24" />
                            </Button>
                          </Tooltip>
                      ))}
                  </Styled.TableRowCell>
                </Styled.TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};
