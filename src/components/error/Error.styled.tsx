// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled, { keyframes, css } from "styled-components";

export const Title = styled.strong`
  font-size: 2rem;
  line-height: 2.2;
`;

export const Description = styled.p`
  font-size: 1rem;
`;
