// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { CurrentUserSession } from "../types";
import { fetchWithTimeout } from "./fetchWithTimeout";

export const whoami = async (): Promise<CurrentUserSession | null> => {
  try {
    const response = await fetchWithTimeout(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users/whoami`,
      {
        credentials: "include",
      }
    );

    if (!response.ok) {
      return await response.json();
    }

    return await response.json();
  } catch (e) {
    return null;
  }
};
