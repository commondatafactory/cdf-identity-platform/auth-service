// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { AxiosError } from "axios";
import { FlowError } from "@ory/client";
import { useEffect, useState } from "react";
import Link from "next/link";
import type { NextPage } from "next";

import { useRouter } from "next/router";
import ory from "../../pkg/sdk";

const Login: NextPage = () => {
  const [error, setError] = useState<FlowError | string>();

  // Get ?id=... from the URL
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    // If the router is not ready yet, or we already have an error, do nothing.
    if (!router.isReady || error) {
      return;
    }

    ory.frontend
      .getFlowError({ id: String(id) })
      .then(({ data }) => {
        setError(data);
      })
      .catch((err: AxiosError) => {
        switch (err.response?.status) {
          case 404:
          // The error id could not be found. Let's just redirect home!
          case 403:
          // The error id could not be fetched due to e.g. a CSRF issue. Let's just redirect home!
          case 410:
            // The error id expired. Let's just redirect home!
            return router.push("/");
        }

        return Promise.reject(err);
      });
  }, [id, router, router.isReady, error]);

  if (!error) {
    return null;
  }

  return (
    <>
      <h1>Er is een fout opgetreden. Blijft dit aanhouden </h1>
      <p>
        Stuur een e-mail naar <a href="mailto:vip@vng.nl">vip@vng.nl</a> om hier
        een melding van te maken of/en ondersteuning te krijgen."
      </p>
      {/* <CodeBox code={JSON.stringify(error, null, 2)} /> */}

      <Link href="/">Naar login</Link>
    </>
  );
};

export default Login;
