// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import PageError from "../src/pages/error";
import PageHome from "../src/pages/index";
import PageLogin from "../src/pages/login";
import PageRecovery from "../src/pages/recovery";
import PageSettings from "../src/pages/settings";
import PageVerification from "../src/pages/verification";

export {
  PageError,
  PageHome,
  PageLogin,
  PageRecovery,
  PageSettings,
  PageVerification,
};

export * from "./hooks";
export * from "./ui";
export * from "./sdk";
export * from "./styled";
