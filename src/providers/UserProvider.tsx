// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { whoami } from "../services/whoami";
import { createContext, useState } from "react";

export interface UserContextProps {}

const UserContext = createContext<UserContextProps>({} as UserContextProps);

const UserProvider = ({ children }) => {
  const [{ data, isLoading, hasError }, setCurrentUser] = useState({
    data: null,
    isLoading: true,
    hasError: false,
  });

  const handleWhoami = async () => {
    const currentUser = await whoami();

    if (!currentUser) {
      setCurrentUser({
        data: null,
        isLoading: false,
        hasError: true,
      });

      return;
    }

    setCurrentUser({
      data,
      isLoading: false,
      hasError: false,
    });

    return currentUser;
  };

  return (
    <UserContext.Provider
      value={{ handleWhoami, currentUser: { data, isLoading, hasError } }}
    >
      {children}
    </UserContext.Provider>
  );
};

export { UserContext, UserProvider };
