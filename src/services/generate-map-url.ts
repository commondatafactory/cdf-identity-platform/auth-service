// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const generateMapUrl = (url, currentUser, generatedMunicipalities) => {
  if (!currentUser?.permissions) {
    return url;
  }

  const activeDookOrganization = currentUser.permissions.find(
    (permission) =>
      permission.namespace === "organizations" &&
      permission.object !== "dego" &&
      permission.relation === "access"
  );

  if (activeDookOrganization) {
    const matchingMunicipality = generatedMunicipalities.find(
      (municipality) =>
        `gm${municipality.code}` === activeDookOrganization.object
    );

    if (matchingMunicipality) {
      return `${url}?query=${matchingMunicipality.name}`;
    }

    return url;
  }

  const returnTo = localStorage.getItem("return-to");

  if (returnTo) {
    localStorage.removeItem("return-to");
    return url + returnTo;
  }

  return url;
};
