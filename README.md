# Auth front-end

This is the front-end used to log in to the various services and projects facilitated by team VIP.

This system is built and integrated with both Ory modules Kratos and Keto, and the authorization API.

To run this application in a local development environment for further development or adaptations, you can follow these steps to get the application up and running.

## Starting up

Install and start the local server:

    yarn
    yarn dev

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Running the required API's

To run a working front-end, the following API's are required to be reachable:

1. Port forward the Kratos and Keto API's. See [their documentation](https://gitlab.com/commondatafactory/cdf-identity-platform/ory) for more information.
2. Run the Authorization API. See [its documentation](https://gitlab.com/commondatafactory/cdf-identity-platform/auth-api) for more information.

## Running in a local Kubernetes Cluster

1. Use the `.env.development.cluster`
2. In your `etc hosts`, link `login.vng.com` to your 127.0.0.1 / k8s ingress endpoint
3. Load the front-end via `http://login.vng.com:3000/login/`
4. Ensure you have local access to your k8s ingresses
