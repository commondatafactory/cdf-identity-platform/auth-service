// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const getCsrfToken = (data) => {
  return data.ui.nodes.find((node) => node.attributes.name === "csrf_token")
    .attributes.value;
};

export const getIsTotpConfigured = (data) =>
  data.ui.nodes.some(
    (node) => node.group === "totp" && node.attributes.name === "totp_unlink"
  );

export const getErrorMessage = (err) =>
  err?.response?.data?.ui?.nodes.find((node) => node.messages?.length > 0)
    ?.messages[0].text ||
  err?.response?.data?.ui?.messages?.[0]?.text ||
  "";
