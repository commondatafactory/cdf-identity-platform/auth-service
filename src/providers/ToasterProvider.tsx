// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Toasters } from "../components/toasters/Toasters";
import React, { createContext, useContext, useState } from "react";

export interface ToasterProps {
  body?: string;
  nth?: number;
  title: string;
  variant?: "success" | "error";
  visibleTime?: number;
}

export interface ToasterContextProps {
  showToast: (toaster: ToasterProps) => void;
}

const ToasterContext = createContext<ToasterContextProps>(
  {} as ToasterContextProps
);

const ToasterProvider = ({ children }) => {
  const [toasters, setToasters] = useState([]);

  const showToast = (toaster: ToasterProps) => {
    toaster.nth = toasters.reduce(
      (acc, toast) => (toast.nth >= acc ? toast.nth + 1 : acc),
      0
    );

    setToasters([...toasters, toaster]);
  };

  return (
    <ToasterContext.Provider
      value={{
        showToast,
      }}
    >
      {children}
      <Toasters toasters={toasters} setToasters={setToasters} />
    </ToasterContext.Provider>
  );
};

export const useToaster = (): {
  showToast: (toaster: ToasterProps) => void;
} => {
  const { showToast } = useContext(ToasterContext);
  return { showToast };
};

export { ToasterContext, ToasterProvider };
