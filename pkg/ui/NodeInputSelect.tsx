import { t } from "../../translations";
import { NodeInputProps } from "./helpers";
import { Button } from "@commonground/design-system";
import {
  Box,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { getNodeId, getNodeLabel } from "@ory/integrations/ui";

export function NodeInputMultipleSelect<T>(props: NodeInputProps) {
  const { node, attributes, value = [], setValue, disabled } = props;

  return (
    <FormControl fullWidth style={{ marginTop: "2rem" }}>
      <InputLabel id="select-permissions-label">
        {t(getNodeLabel(node))}
      </InputLabel>
      <Select
        labelId="select-permissions-label"
        id={getNodeId(node)}
        name={attributes.name}
        value={value}
        onChange={(e: {
          target: { value: string | number | boolean | undefined | any };
        }) => {
          setValue(e.target.value);
        }}
        // renderValue={(values) => (
        //   <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
        //     {values.map((value) => (
        //       <Chip
        //         key={value}
        //         label={
        //           (attributes as any).options.find(
        //             (option) => option.value === value
        //           )?.label
        //         }
        //       />
        //     ))}
        //   </Box>
        // )}
      >
        {(attributes as any).options.map((option) => (
          <MenuItem key={option.value} value={option.value as any}>
            {option.label}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
