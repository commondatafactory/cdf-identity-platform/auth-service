// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { t } from "../../translations";
import { NodeInputProps } from "./helpers";
import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";

export function NodeInputDefault<T>(props: NodeInputProps) {
  const { node, attributes, value = "", setValue, disabled } = props;

  // Some attributes have dynamic JavaScript - this is for example required for WebAuthn.
  const onClick = () => {
    // This section is only used for WebAuthn. The script is loaded via a <script> node
    // and the functions are available on the global window level. Unfortunately, there
    // is currently no better way than executing eval / function here at this moment.
    if (attributes.onclick) {
      const run = new Function(attributes.onclick);
      run();
    }
  };

  // Render a generic text input field.
  return (
    <label style={{ width: "100%" }}>
      <p>{t(node.meta.label?.text)}</p>
      {node.messages.map(({ text, id }, k) => (
        <span
          key={`${id}-${k}`}
          data-testid={`ui/message/${id}`}
          style={{ color: "Crimson" }}
        >
          {t(text)}
        </span>
      ))}
      <Input
        onClick={onClick}
        onChange={(e: {
          target: { value: string | number | boolean | undefined | any };
        }) => {
          setValue(
            (attributes.type as string) === "array"
              ? [e.target.value]
              : e.target.value
          );
        }}
        type={attributes.type}
        name={attributes.name}
        placeholder={(attributes as any).placeholder}
        value={value}
        disabled={attributes.disabled || disabled}
        help={node.messages.length > 0}
        state={
          node.messages.find(({ type }) => type === "error")
            ? "error"
            : undefined
        }
        style={{ width: "100%" }}
      />
    </label>
  );
}
