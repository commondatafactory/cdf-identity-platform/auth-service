// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const Container = styled.section`
  background: url("/images/bg.svg"), #dde1e8;
  background-position: top;
  padding: 0 40px;
`;

export const LogoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 164px;
  width: 100%;

  img {
    height: 3rem;
    width: 3rem;
  }
  h2 {
    margin-top: 24px;
  }
`;

export const Link = styled.a`
  display: block;
  cursor: pointer;
  margin-top: 8px;
  text-decoration: underline;
  color: #757575;
  fill: #757575;
`;
