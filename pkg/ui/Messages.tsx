// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { t } from "../../translations";
import { UiText } from "@ory/client";

interface MessageProps {
  message: UiText;
}

export const Message = ({ message }: MessageProps) => {
  return <p data-testid={`ui/message/${message.id}`}>{t(message.text)}</p>;
};

interface MessagesProps {
  messages?: Array<UiText>;
}

export const Messages = ({ messages }: MessagesProps) => {
  if (!messages) {
    // No messages? Do nothing.
    return null;
  }

  return (
    <>
      {messages.map((message) => (
        <p
          key={message.id}
          style={{ color: message.type === "error" && "Crimson" }}
        >
          {t(message.text)}
        </p>
      ))}
    </>
  );
};
