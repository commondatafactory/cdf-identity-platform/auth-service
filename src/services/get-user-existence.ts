// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { fetchWithTimeout } from "./fetchWithTimeout";

export const getUserExistence = async ({ field, value }): Promise<Boolean> => {
  try {
    const query = new URLSearchParams({ [field]: value }).toString();

    const usersResponse = await fetchWithTimeout(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users?${query}`,
      {
        credentials: "include",
        method: "HEAD",
      }
    );

    if (!usersResponse.ok) {
      return false;
    }

    return true;
  } catch (e) {
    console.log("e", e);
    return false;
  }
};
