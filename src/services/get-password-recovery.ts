// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const getPasswordRecovery = async ({ email }) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users/password-recovery?email=${email}`
    );

    if (!response.ok) {
      return null;
    }

    return true;
  } catch (e) {
    console.log(e);
  }
};
