// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const Wrapper = styled.section`
  align-items: start;
  display: flex;
  gap: 80px;
  justify-content: center;
  width: 100%;
  margin: 0 0 40px;
`;

export const Progression = styled.div<{ $isActive: boolean }>`
  align-items: center;
  display: flex;
  flex-direction: column;
  width: 80px;

  & > div {
    align-items: center;
    background: ${({ theme, $isActive }) =>
      $isActive ? theme.tokens.colorInfo : theme.tokens.colorPaletteGray400};
    border-radius: 50%;
    color: ${({ theme }) => theme.tokens.colorBackground};
    display: flex;
    height: 44px;
    justify-content: center;
    position: relative;
    font-weight: bold;
    width: 44px;

    > span {
      margin-left: 3px;
    }

    & > hr {
      background: ${({ theme, $isActive }) =>
        $isActive ? theme.tokens.colorInfo : theme.tokens.colorPaletteGray400};
      border: 0;
      height: 2px;
      position: absolute;
      margin: 0;
      top: 50%;
      right: calc(50% + 35px);
      width: calc(80px + 80px - 70px);
      z-index: -1;
    }
  }
  & > p {
    font-size: 16px;
    line-height: 1.3;
    margin: 4px 0 0;
    text-align: center;
  }
`;

export const Divider = styled.div`
  background: ${({ theme }) => theme.tokens.colorPaletteGray300};
  height: 1px;
  margin: 0 auto 64px;
  width: min(722px, 80%);
`;
