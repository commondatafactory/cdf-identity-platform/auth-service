// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { OutlinedInput, Select, Table } from "@mui/material";
import styled from "styled-components";

export const Wrapper = styled("div")({
  width: "100%",
  alignItems: "center",
  display: "flex",
  gap: 20,
  justifyContent: "flex-end",
  paddingBottom: 15,
});

export const Label = styled("label")({
  paddingRight: 10,
});

export const Tag = styled.p`
  background: rgba(0, 0, 0, 0.07);
  border-radius: 4px;
  padding: 4px 8px;
  width: max-content;

  + p {
    margin-top: 4px !important;
  }

  &.active {
    background: #d3f3fd;
  }
`;

export const StyledOutlineInput = styled(OutlinedInput)({
  borderRadius: 1,
});

export const StyledSelect = styled(Select)({
  borderRadius: 1,
  minWidth: 200,
});

export const StyledTable = styled(Table)({
  borderRadius: 1,
});

export const TableHead = styled("thead")({
  background: "#F3F3F3",
  height: "75px",
});

export const TableHeader = styled("th")`
  font-weight: bold;
  text-align: left;
  padding-left: 0.5rem;
  font-size: 1.1rem;
`;

export const TableRow = styled.tr`
  &.unverified {
    text-decoration: line-through;
  }

  :hover {
    background-color: #f2f2f2;
  }
`;

export const TableRowCell = styled("th")`
  border-bottom: 1px solid #f2f2f2;
  font-size: 1rem;
  text-align: left;
  padding: 0.5rem;
  font-weight: normal;

  p {
    margin: 0;
  }
  Button {
    margin: 0.2rem;
    padding: 0.2rem;
    min-width: 1rem;
    min-height: 1rem !important;
    border-radius: 3px !important;
  }
  svg {
    fill: #212121;
  }
`;

export const TableCellIcons = styled("th")`
  border-bottom: "1px solid #f2f2f2";
  text-align: "center";
`;
