// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const editUser = async ({ id, identity }) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users/${id}`,
      {
        method: "PUT",
        body: JSON.stringify(identity),
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (!response.ok) {
      return { error: "Kon gebruiker permissies niet bewerken" };
    }

    return response.json();
  } catch (e) {
    console.log(e);
    return { error: "Kon gebruiker niet bewerken" };
  }
};
