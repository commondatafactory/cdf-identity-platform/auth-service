// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const Main = styled.main`
  background-image: url("/images/bg.svg");
  display: grid;
  height: 100%;
  grid-template-columns: 480px minmax(0, 1fr);

  @media screen and (max-width: 940px) {
    grid-template-columns: auto;
  }
`;

export const Content = styled.section`
  align-items: flex-start;
  display: flex;
  flex-direction: column;
  padding: 96px 3rem;
  position: relative;
`;
