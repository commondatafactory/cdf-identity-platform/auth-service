// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Spinner } from "@commonground/design-system";
import { useEffect } from "react";
import { NextPage } from "next";
// import { useRouter } from "next/router";
import Head from "next/head";

import { CurrentUserSession } from "../types";
import { generatedMunicipalities } from "../generated-municipalities";
import { isUserPassing2FA } from "../services/user-passes-two-factor-check";
import { useToaster } from "../providers/ToasterProvider";
import {
  filterOnActiveFirst,
  filterOnDuplicateKeys,
  filterOnMatchingActiveOrganization,
} from "../utils/filters";
import { updatePermissions } from "../services/update-permissions";
import { deletePermission } from "../services/delete-permission";
import { generateMapUrl } from "../services/generate-map-url";
import authModel from "../../authorization-model.json";

import * as Styled from "../styles/permissies.styled";

const handleActivatePermission = async (tuple, currentUser) => {
  const updatedPermissions = await updatePermissions({
    permissions: [tuple],
    userId: currentUser.identity.id,
  });

  if (updatedPermissions.error) {
    throw (
      updatedPermissions.message ||
      "Kon momenteel de gebruiker niet updaten. Probeer het later nog eens."
    );
  }

  return true;
};

const getSubdivisionsForActiveOrganization = (
  activeOrganization,
  allUserPermissions: any[]
) =>
  (activeOrganization ? allUserPermissions : [])
    .filter(filterOnMatchingActiveOrganization(activeOrganization))
    .filter(
      ({ namespace, relation }) =>
        namespace === authModel.subdivisions.namespace &&
        [
          authModel.organizations.relations.issuer,
          authModel.organizations.relations.access,
        ].includes(relation)
    )
    .filter(filterOnActiveFirst)
    .filter(filterOnDuplicateKeys(["object", "relation"]))
    .sort((a, b) => a.label?.localeCompare(b.label));

interface PageProps {
  currentUser: CurrentUserSession;
  handleWhoami: any;
  currentUserError: any;
  currentUserIsLoading: boolean;
}

const Page: NextPage<PageProps> = ({
  currentUser,
  handleWhoami,
  currentUserError,
  currentUserIsLoading,
}) => {
  // const router = useRouter()git ad
  useEffect(() => {
    (async () => {
      await handleWhoami();
    })();
  }, []);

  useEffect(() => {
    if (currentUserIsLoading) {
      return;
    }

    if (currentUserError || !currentUser) {
      location.href = "/login";
      return;
    }

    if (!isUserPassing2FA({ user: currentUser })) {
      location.href = `/login?aal=aal2`;
    }
  }, [currentUserIsLoading]);

  const { showToast } = useToaster();

  const handleOrganizationToggle = async (
    object: string,
    isActive: boolean
  ) => {
    try {
      if (!currentUser?.identity) {
        return;
      }

      const deleteActiveOrganizations = await deletePermission({
        params: `namespace=${authModel.organizations.namespace}&relation=access&subject_id=${currentUser.identity.id}`,
      });

      const deleteActiveRoles = await deletePermission({
        params: `namespace=${authModel.subdivisions.namespace}&relation=access&subject_id=${currentUser.identity.id}`,
      });

      if (!deleteActiveOrganizations || !deleteActiveRoles) {
        throw "";
      }

      if (isActive) {
        return;
      }

      const tuple = { namespace: "organizations", object, relation: "access" };
      await handleActivatePermission(tuple, currentUser);

      const subdivisions = getSubdivisionsForActiveOrganization(
        object,
        currentUser?.permissions || []
      );

      if (subdivisions.length === 1) {
        const tuple = {
          namespace: authModel.subdivisions.namespace,
          object: subdivisions[0].object,
          relation: authModel.subdivisions.relations.access,
        };
        await handleActivatePermission(tuple, currentUser);
      }
    } catch {
      showToast({
        variant: "error",
        title: "Fout bij het wijzigen van je permissie. Ververs de pagina.",
      });
    } finally {
      await handleWhoami();
    }
  };

  const handleThemeToggle = async (object: string, isActive: boolean) => {
    try {
      if (!currentUser.identity) {
        return;
      }

      if (isActive) {
        const deleteActiveRoles = await deletePermission({
          params: `namespace=${authModel.subdivisions.namespace}&relation=${authModel.subdivisions.relations.access}&subject_id=${currentUser.identity.id}`,
        });
        if (!deleteActiveRoles) {
          throw "";
        }
        return;
      }

      const tuple = {
        namespace: authModel.subdivisions.namespace,
        object,
        relation: authModel.subdivisions.relations.access,
      };
      await handleActivatePermission(tuple, currentUser);
    } catch {
      showToast({
        variant: "error",
        title: "Fout bij het wijzigen van je permissie. Ververs de pagina.",
      });
    } finally {
      await handleWhoami();
    }
  };

  const sortedOrganizationPermissions = (currentUser?.permissions || [])
    .filter(
      ({ namespace, relation }) =>
        namespace === authModel.organizations.namespace &&
        [
          authModel.organizations.relations.issuer,
          authModel.organizations.relations.access,
        ].includes(relation)
    )
    .filter(filterOnActiveFirst)
    .filter(filterOnDuplicateKeys(["object", "relation"]))
    .sort((a, b) => a.label?.localeCompare(b.label));

  const userActiveOrganizationObject = currentUser?.permissions?.find(
    (copy) =>
      copy.namespace === authModel.organizations.namespace &&
      copy.relation === authModel.organizations.relations.access
  )?.object;

  const sortedThemePermissions = getSubdivisionsForActiveOrganization(
    userActiveOrganizationObject,
    currentUser?.permissions || []
  );

  if (
    (!currentUser?.permissions && !currentUserError) ||
    currentUserIsLoading
  ) {
    return <Spinner />;
  }

  if (currentUserError) {
    return (
      <p style={{ maxWidth: 520 }}>
        Er is een fout opgetreden. Probeer het later nog eens, of stuur een
        e-mail naar <a href="mailto:vip@vng.nl">vip@vng.nl</a> om hier een
        melding van te maken of/en ondersteuning te krijgen.
      </p>
    );
  }

  const isOrganizationActive = sortedOrganizationPermissions.length
    ? sortedOrganizationPermissions.some(
        ({ relation }) => relation === authModel.organizations.relations.access
      )
    : true;

  const isSubdivisionActive = sortedThemePermissions.length
    ? sortedThemePermissions.some(
        ({ relation }) => relation === authModel.subdivisions.relations.access
      )
    : true;

  return (
    <>
      <Head>
        <title>Voorzieningen</title>
        <meta name="description" content="Selecteer een rol" />
      </Head>

      <>
        <h2>1. Kies een organisatie</h2>
        <p>
          Hieronder zie je welke organisatie je gebruik maakt van de
          datavoorziening.
        </p>
        <Styled.ChipWrapper>
          {sortedOrganizationPermissions.map(({ label, object, relation }) => (
            <Styled.ChoiceChip
              onClick={() =>
                handleOrganizationToggle(
                  object,
                  relation === authModel.organizations.relations.access
                )
              }
              isActive={relation === authModel.organizations.relations.access}
              key={object}
            >
              {label}
            </Styled.ChoiceChip>
          ))}
        </Styled.ChipWrapper>
      </>

      <>
        <h2 style={{ marginTop: "14vh" }}>2. Kies een thema</h2>

        {sortedThemePermissions.length ? (
          <p>Hieronder zie je welk thema binnen je gemeente actief is.</p>
        ) : (
          <p>
            Je hebt geen actieve thema's binnen je geselecteerde gemeente om uit
            te kiezen.
          </p>
        )}

        <Styled.ChipWrapper>
          {sortedThemePermissions.map(({ label, object, relation }) => (
            <Styled.ChoiceChip
              onClick={() =>
                handleThemeToggle(
                  object,
                  relation === authModel.subdivisions.relations.access
                )
              }
              isActive={relation === authModel.subdivisions.relations.access}
              key={object}
            >
              {label}
            </Styled.ChoiceChip>
          ))}
        </Styled.ChipWrapper>
      </>

      <>
        <h2 style={{ marginTop: "14vh" }}>3. Ga naar de datavoorziening </h2>

        <p>Kies hieronder welke datavoorziening je wil bekijken.</p>
        <p>
          Activeer eerst een organisatie en thema indien je hiervoor toegang
          voor hebt.
        </p>

        <Styled.AppContainer>
          <Styled.AppCard>
            <img src="/images/thumbnail-dego.jpg" />
            <div>
              <p>Datavoorziening Energietransitie Gebouwde omgeving</p>
            </div>
            <Button
              as="a"
              href={generateMapUrl(
                process.env.NEXT_PUBLIC_DEGO_URL,
                currentUser,
                generatedMunicipalities
              )}
              disabled={!isSubdivisionActive || !isOrganizationActive}
            >
              Ga naar DEGO
            </Button>
          </Styled.AppCard>

          <Styled.AppCard>
            <img src="/images/thumbnail-dook.jpg" />
            <div>
              <p>Datavoorziening Onregelmatigheden op de Kaart</p>
            </div>
            <Button
              as="a"
              href={generateMapUrl(
                process.env.NEXT_PUBLIC_DOOK_URL,
                currentUser,
                generatedMunicipalities
              )}
              disabled={!isSubdivisionActive || !isOrganizationActive}
            >
              Ga naar DOOK
            </Button>
          </Styled.AppCard>
        </Styled.AppContainer>
      </>
    </>
  );
};

export default Page;
