// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { fetchWithTimeout } from "./fetchWithTimeout";

// TODO: refactor name to getUsers()
export const getIdentities = async (currentUser) => {
  try {
    // NOTE: You're fetching "member" roles, because you're only looking for users that you could have created
    // TODO: Consider moving URLSearchParams out of function to make it more generic
    const query = new URLSearchParams({
      object: currentUser.permissions.find(
        (permission) => permission.type === "organization"
      )?.id,
    }).toString();

    const identitiesResponse = await fetchWithTimeout(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users?${query}`
    );
    if (!identitiesResponse.ok) {
      return;
    }

    return await identitiesResponse.json();
  } catch (e) {
    console.log("e", e);
  }
};
