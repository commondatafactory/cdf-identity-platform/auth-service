import {defaultTheme} from "@commonground/design-system";

type ThemeInterface = typeof defaultTheme

declare module "styled-components" {
  interface DefaultTheme extends ThemeInterface {}
}
