image: node:20-alpine3.17

stages:
  - yarn-install
  - lint-typecheck-test
  - build_and_push
  - deploy

default:
  cache: &global_cache
    key: "${CI_PROJECT_PATH}"
    paths:
      - node_modules/
      - .next/cache/
      - .yarn-cache/
    policy: pull-push

variables:
  REVIEW_NAMESPACE: cdf-$CI_ENVIRONMENT_SLUG
  REVIEW_BASE_DOMAIN: nlx.reviews
  YARN_CACHE_DIR: .yarn-cache
  SHA_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
  REF_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

#########################################################################

yarn-install:
  stage: yarn-install
  script:
    - yarn install --frozen-lockfile --prefer-offline --no-progress --color=false --quiet

#########################################################################

Lint, typecheck & test:
  stage: lint-typecheck-test
  cache:
    <<: *global_cache
    policy: pull
  script:
    - yarn lint
    # - yarn type-check
    # - yarn test
  coverage: /All\sfiles.*?\s+(\d+.\d+)/
  artifacts:
    expire_in: 1 month
    paths:
      - coverage
  needs: ["yarn-install"]

#########################################################################

.build:
  image: docker:dind
  # image: registry.gitlab.com/commondatafactory/dockerimages/yarnbuilder:4fbe11c20230316v2
  cache:
    <<: *global_cache
    policy: pull
  stage: build_and_push
  services:
    - docker:20.10.24-dind
  variables:
    DOCKER_TLS_CERTDIR: "/certs"
    NEXT_TELEMETRY_DISABLED: 1
  before_script:
    - echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - apk --update add nodejs npm yarn
  needs: ["Lint, typecheck & test"]

#########################################################################

Build and push acceptation:
  extends: .build
  script:
    - cp .env.staging .env.local
    - yarn build
    - find out -name "*.js" | xargs -I % sh -c 'gzip %'
    - docker pull $CI_REGISTRY_IMAGE:latest
    - docker build
      --build-arg NEXT_PUBLIC_DEGO_URL=https://acc.tvw.commondatafactory.nl
      --build-arg NEXT_PUBLIC_DOOK_URL=https://acc.dook.commondatafactory.nl
      --build-arg NEXT_PUBLIC_AUTH_API_URL=https://acc.login.commondatafactory.nl
      --build-arg NEXT_PUBLIC_ANALYST_LOGIN_URL=https://acc.login.commondatafactory.nl/login
      --build-arg NEXT_PUBLIC_ADMIN_LOGIN_URL=https://acc.login.commondatafactory.nl/admin-login
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $SHA_TAG --tag $REF_TAG .
    - docker push $SHA_TAG
    - docker push $REF_TAG
  only:
    - develop

Build and push production:
  extends: .build
  script:
    - cp .env.production .env.local
    - yarn build
    - find out -name "*.js" | xargs -I % sh -c 'gzip %'
    - docker pull $CI_REGISTRY_IMAGE:latest
    - docker build
      --build-arg NEXT_PUBLIC_KRATOS_PUBLIC_URL=https://id.data.vng.nl
      --build-arg NEXT_PUBLIC_DEGO_URL=https://dego.vng.nl
      --build-arg NEXT_PUBLIC_DOOK_URL=https://dook.vng.nl
      --build-arg NEXT_PUBLIC_AUTH_API_URL=https://login.data.vng.nl
      --build-arg NEXT_PUBLIC_ANALYST_LOGIN_URL=https://login.data.vng.nl/login
      --build-arg NEXT_PUBLIC_ADMIN_LOGIN_URL=https://login.data.vng.nl/admin-login
      --cache-from $CI_REGISTRY_IMAGE:latest
      --tag $SHA_TAG --tag $REF_TAG .
    - docker push $SHA_TAG
    - docker push $REF_TAG
  only:
    - main

##########################################################################

.deploy:
  cache: []
  stage: deploy

#########################################################################

Deploy acceptation:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - |
      cat >./helm/overrides.yaml <<EOL
      image:
        tag: "${CI_COMMIT_SHORT_SHA}"
        env:
          NEXT_PUBLIC_KRATOS_PUBLIC_URL: https://acc.id.commondatafactory.nl
          NEXT_PUBLIC_DEGO_URL: https://acc.tvw.commondatafactory.nl
          NEXT_PUBLIC_DOOK_URL: https://acc.tvw.commondatafactory.nl
          NEXT_PUBLIC_ANALYST_LOGIN_URL: https://acc.login.commondatafactory.nl/login
          NEXT_PUBLIC_ADMIN_LOGIN_URL: https://acc.login.commondatafactory.nl/admin-login
          NEXT_PUBLIC_AUTH_API_URL: https://acc.login.commondatafactory.nl
      ingress:
        hosts:
          - host: acc.login.commondatafactory.nl
            paths:
              - path: /
                pathType: ImplementationSpecific
        tls:
          - secretName: acc-login-ingress-tls
            hosts:
              - acc.login.commondatafactory.nl
      EOL

      cat ./helm/overrides.yaml
  script:
    - kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
    - helm upgrade
      --install -n cdf-acc
      --namespace cdf-acc
      --values ./helm/values.yaml
      --values ./helm/overrides.yaml
      auth-service-front-end ./helm
  environment:
    name: acc
  only:
    - branches
  except:
    - main
  needs: ["Build and push acceptation"]

Deploy production:
  extends: .deploy
  image: registry.gitlab.com/commonground/core/review-app-deployer:latest
  before_script:
    - |
      cat >./helm/overrides.yaml <<EOL
      image:
        tag: "${CI_COMMIT_SHORT_SHA}"
        env:
          NEXT_PUBLIC_KRATOS_PUBLIC_URL: https://id.data.vng.nl
          NEXT_PUBLIC_DEGO_URL: https://dego.vng.nl
          NEXT_PUBLIC_DOOK_URL: https://dook.vng.nl
          NEXT_PUBLIC_ANALYST_LOGIN_URL: https://login.data.vng.nl/login
          NEXT_PUBLIC_ADMIN_LOGIN_URL: https://login.data.vng.nl/admin-login
          NEXT_PUBLIC_AUTH_API_URL: https://login.data.vng.nl/api
      ingress:
        hosts:
          - host: login.data.vng.nl
            paths:
              - path: /
                pathType: ImplementationSpecific
        tls:
          - secretName: login-ingress-tls-vng-nl
            hosts:
              - login.data.vng.nl
      EOL

      cat ./helm/overrides.yaml
  script:
    - kubectl config use-context commondatafactory/ci/kubernetes-agents:cdf-datacluster
    - helm upgrade
      --install -n vng
      --namespace vng
      --values ./helm/values.yaml
      --values ./helm/overrides.yaml
      auth-service-front-end ./helm
  environment:
    name: prod
  only:
    - main
  needs: ["Build and push production"]
