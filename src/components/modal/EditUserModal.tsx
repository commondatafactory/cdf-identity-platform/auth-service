// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Icon } from "@commonground/design-system";
import { useEffect, useState } from "react";
import { Box, Dialog, DialogContent, IconButton } from "@mui/material";

import { updatePermissions } from "../../services/update-permissions";
import { CreateUserForm } from "./CreateUserForm";

import { getSubdivisionsForActiveOrganization } from "../../utils/filters";
import { deletePermission } from "../../services/delete-permission";

import CloseIcon from "@mui/icons-material/Close";
import addUserIcon from "../../../public/images/user-add-line.svg";

import authModel from "../../../authorization-model.json";

interface FormValues {
  label: string;
  value: string;
}

export const EditUserModal = ({
  isActive,
  onConfirm,
  onCancel,
  currentUser,
  editUser,
}) => {
  const [formError, setFormError] = useState("");

  const [grantableOrganizations, setGrantableOrganizations] = useState<
    FormValues[]
  >([]);
  const [grantableData, setGrantableData] = useState<FormValues[]>([]);

  useEffect(() => {
    // NOTE: filter to get active user organization
    const filteredUserOrganizations = currentUser.permissions.filter(
      (permission) =>
        permission.namespace === "organizations" &&
        permission.relation === "access"
    );

    const mappedAvailableAdministrators = filteredUserOrganizations.map(
      ({ label, id }) => ({
        label,
        value: id,
      })
    );

    // TODO: don't map too early, map when needed
    setGrantableOrganizations(mappedAvailableAdministrators);

    if (!filteredUserOrganizations.length) {
      return;
    }

    // NOTE: filter to get relevant themes based on active organization
    const filteredAvailableRoles = getSubdivisionsForActiveOrganization(
      filteredUserOrganizations[0].object,
      currentUser.permissions
    );

    const mappedAvailableRoles = filteredAvailableRoles.map(
      ({ label, id }) => ({
        label,
        value: id,
      })
    );

    setGrantableData(mappedAvailableRoles);
  }, []);

  const handleSubmit = async (values) => {
    setFormError("");

    // VALIDATION

    const isValidMail = String(values?.email || "")
      .toLowerCase()
      .match(
        /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
      );

    if (!isValidMail) {
      setFormError(
        "Het opgegeven e-mailadres wordt niet herkent als valide e-mailadres."
      );
      return;
    }

    if (!values.firstName || !values.surname) {
      setFormError("De gebruiker moet een naam hebben.");
      return;
    }

    // PERMISSIONS
    // delete only the permissions that are grantable
    const deletePermissions = await Promise.all(
      grantableData.map(async ({ value }) => {
        const params = `namespace=${authModel.subdivisions.namespace}&object=${value}&subject_id=${editUser.id}`;
        return await deletePermission({ params });
      })
    );

    if (!deletePermissions.every((deletedPermission) => deletedPermission)) {
      setFormError(
        "Kon momenteel de gebruiker niet updaten. Probeer het later nog eens."
      );
      return;
    }

    const rolePermissions =
      values.data?.map((value) => ({
        namespace: authModel.subdivisions.namespace,
        object: value,
        relation: authModel.subdivisions.relations.issuer,
        subject_id: editUser.id,
      })) || [];

    // update permissions
    const updatedPermissions = await updatePermissions({
      permissions: rolePermissions,
      userId: editUser.id,
    });

    if (updatedPermissions.error) {
      setFormError(
        updatedPermissions.message ||
          "Kon momenteel de gebruiker niet updaten. Probeer het later nog eens."
      );
      return;
    }

    setFormError("");
    onConfirm(true);
  };

  const handleClose = () => {
    onCancel();
    setFormError("");
  };

  const initialValues = {
    firstName: editUser?.traits.name.first,
    surname: editUser?.traits.name.last,
    email: editUser?.traits.email,
    data: editUser?.permissions
      .filter(
        (permission) =>
          permission.namespace === authModel.subdivisions.namespace
      )
      .map((permission) => permission.id),
    administrators: editUser?.permissions
      .filter(
        (permission) =>
          permission.namespace === authModel.organizations.namespace
      )
      .map((permission) => permission.id),
  };

  return (
    <Dialog
      open={isActive}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      maxWidth="lg"
      fullWidth
    >
      <IconButton
        aria-label="close"
        onClick={handleClose}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>

      <DialogContent>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            m: "auto",
            width: "640px",
            paddingTop: "24px",
            paddingBottom: "24px",
          }}
        >
          <h2>
            <Icon size="x-large" as={addUserIcon} /> Gebruiker{" "}
            {editUser?.traits.name.first} {editUser?.traits.name.last} bewerken
          </h2>

          <CreateUserForm
            onSubmit={handleSubmit}
            grantableOrganizations={grantableOrganizations}
            grantableData={grantableData}
            currentUser={currentUser}
            handleClose={handleClose}
            formError={formError}
            setFormError={setFormError}
            editUser={editUser}
            initialValues={initialValues}
          />
        </Box>
      </DialogContent>
    </Dialog>
  );
};
