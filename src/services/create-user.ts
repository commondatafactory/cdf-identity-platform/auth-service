// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Identity } from "@ory/client";

export const createUser = async ({
  traits,
  permissions = [],
}: {
  traits: Identity["traits"];
  permissions?: any[];
}) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users`,
      {
        method: "POST",
        body: JSON.stringify({ traits, permissions }),
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const contentType = response.headers.get("content-type");

    if (!response.ok) {
      if (contentType && contentType.includes("application/json")) {
        throw await response?.json();
      }

      return null;
    }

    if (contentType && contentType.includes("application/json")) {
      return await response.json();
    }

    return true;
  } catch (e) {
    console.log(e);
    return e;
  }
};
