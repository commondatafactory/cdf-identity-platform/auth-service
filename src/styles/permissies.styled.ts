// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button } from "@commonground/design-system";
import styled, { css } from "styled-components";

export const SelectableRoleCard = styled.li<any>`
  border: 1px solid #9e9e9e;
  min-height: 104px;
  width: 100%;

  input {
    display: none;
  }

  input:checked + label {
    border: 2px solid #0b71a1;

    > div {
      background: #0b71a1;
      color: white;
    }

    > p {
      margin: auto 15px;
    }
  }

  label {
    box-sizing: border-box;
    cursor: ${({ disabled }) => (disabled ? "not-allowed" : "pointer")};
    display: flex;
    height: 100%;
    opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};

    > p {
      margin: auto 16px;
      font-size: 0.875rem;
    }

    h3 {
      font-size: 0.9rem;
    }

    > div {
      align-items: center;
      background: #e0e4ea;
      display: flex;
      flex-shrink: 0;
      justify-content: center;
      padding: 8px;
      text-align: center;
      width: 36%;

      > * {
        margin: 0;
        padding: 0;
      }
    }
  }
`;

export const AppContainer = styled.section`
  display: grid;
  gap: 60px;
  grid-auto-rows: 1fr;
  grid-template-columns: repeat(auto-fill, 352px);
  grid-template-rows: 320px;
  row-gap: 2rem;
  padding: 0;
  width: 100%;
`;

export const AppCard = styled.div`
  position: relative;

  img {
    object-fit: cover;
    height: 100%;
    width: calc(100% - 32px);
  }
  > div {
    background: rgba(252, 252, 252, 0.6);
    box-shadow: rgba(33, 35, 38, 0.2) 0px 10px 10px -10px;
    bottom: 0;
    position: absolute;
    left: 0;
    right: 0;
    height: fit-content;
    padding: 16px;
    width: calc(100% - 32px);

    p {
      margin: 0;
      font-weight: 600;
    }
  }
  ${Button} {
    position: absolute;
    right: 0;
    top: 32px;
  }
`;

export const CenterWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`;

export const ChipWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;

  gap: 16px;
  margin-top: 12px;
  width: 100%;
`;

export const ChoiceChip = styled("div")<{
  isActive?: boolean;
}>`
  display: grid;
  place-items: center;
  font-weight: bold;
  height: 35px;
  background-color: #e0e4ea;
  color: #212121;
  border-radius: 10px;
  cursor: pointer;
  padding: 0 20px;
  width: fit-content;

  ${(props) =>
    props.isActive &&
    css`
      background-color: #0b71a1;
      color: white;
    `}
`;
