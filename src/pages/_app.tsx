// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { SidebarLayout } from "../components/layout/SidebarLayout";
import { ToasterProvider } from "../providers/ToasterProvider";
import { whoami } from "../services/whoami";
import { GlobalStyle } from "../styles/globals";
import {
  GlobalStyles as GlobalStyleCommonGround,
  defaultTheme,
} from "@commonground/design-system";
import "@fontsource/source-sans-pro/latin.css";
import { NextPage } from "next";
import type { AppProps } from "next/app";
import { ReactElement, ReactNode, useEffect, useState } from "react";
import { ThemeProvider } from "styled-components";
import { CurrentUserSession } from "../types";

export type NextPageWithLayout<
  P = {
    currentUser: CurrentUserSession;
    setCurrentUser: any;
    currentUserIsLoading: any;
    currentUserError: any;
    handleWhoami: any;
  },
  IP = P
> = NextPage<P, IP> & {
  getLayout?: (
    page: ReactElement,
    currentUser: CurrentUserSession,
    handleWhoami: any
  ) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};

const MyApp = ({ Component, pageProps }: AppPropsWithLayout) => {
  const [loading, setLoading] = useState(true);
  const [
    { currentUser, currentUserIsLoading, currentUserError },
    setCurrentUser,
  ] = useState({
    currentUser: null as CurrentUserSession | null,
    currentUserIsLoading: true,
    currentUserError: false,
  });

  useEffect(() => {
    setLoading(false);

    const returnTo = location.search.includes("return-to")
      ? location.search.replace("?return-to=", "") + location.hash
      : "";

    if (returnTo) {
      localStorage.setItem("return-to", returnTo);
    }
  }, []);

  const handleWhoami = async (): Promise<CurrentUserSession | null> => {
    const currentUser = await whoami();

    if (!currentUser || currentUser?.error) {
      setCurrentUser({
        currentUser: null,
        currentUserIsLoading: false,
        currentUserError: true,
      });

      return null;
    }

    setCurrentUser({
      currentUser,
      currentUserIsLoading: false,
      currentUserError: false,
    });

    return currentUser;
  };

  useEffect(() => {
    if (!Component.getLayout) {
      return;
    }

    (async () => {
      if (!currentUser && handleWhoami) {
        const whoami = await handleWhoami();

        if (!whoami) {
          location.href = "/login";
        }
      }
    })();
  }, []);

  return (
    <ThemeProvider theme={defaultTheme}>
      <GlobalStyleCommonGround />
      <GlobalStyle />
      <ToasterProvider>
        {!loading && (
          <>
            {Component.getLayout ? (
              Component.getLayout(
                <Component
                  {...pageProps}
                  currentUser={currentUser}
                  setCurrentUser={setCurrentUser}
                  currentUserIsLoading={currentUserIsLoading}
                  currentUserError={currentUserError}
                  handleWhoami={handleWhoami}
                />,
                currentUser,
                handleWhoami
              )
            ) : (
              <SidebarLayout currentUser={currentUser}>
                <Component
                  {...pageProps}
                  currentUser={currentUser}
                  setCurrentUser={setCurrentUser}
                  currentUserIsLoading={currentUserIsLoading}
                  currentUserError={currentUserError}
                  handleWhoami={handleWhoami}
                />
              </SidebarLayout>
            )}
          </>
        )}
      </ToasterProvider>
    </ThemeProvider>
  );
};

export default MyApp;
