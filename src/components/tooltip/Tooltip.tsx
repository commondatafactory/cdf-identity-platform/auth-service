// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as Styled from "./Tooltip.styled";
import React, { FC, useState, ReactNode } from "react";
import ReactDOM from "react-dom";

interface TooltipProps {
  description: string;
  position?: "top" | "bottom" | "left";
  children: ReactNode;
}
export const Tooltip: FC<TooltipProps> = ({
  children,
  description,
  position = "top",
}) => {
  const [coords, setCoords] = useState(null);

  return (
    <>
      <Styled.TooltipWrapper
        onMouseEnter={(e) => {
          const rect = e.target.getBoundingClientRect();
          setCoords({
            left: rect.left + rect.width / 2,
            top: rect.top + document.body.scrollTop,
          });
        }}
        // onMouseLeave={() => setCoords(null)}
      >
        {children}
      </Styled.TooltipWrapper>

      {coords &&
        ReactDOM.createPortal(
          <Styled.Tooltip
            style={{ top: coords.top, left: coords.left }}
            // style={coords ? { visibility: "visible" } : { visibility: "hidden" }}
            className={position}
          >
            <p>{description}</p>
          </Styled.Tooltip>,
          document.body
        )}
    </>
  );
};
