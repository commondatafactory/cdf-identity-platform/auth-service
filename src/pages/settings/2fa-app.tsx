// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import Head from "next/head";
import Link from "next/link";
import { AxiosError } from "axios";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { SettingsFlow, UpdateSettingsFlowBody } from "@ory/client";
import { Icon } from "@commonground/design-system";

import ory from "../../../pkg/sdk";
import { handleFlowError } from "../../../pkg/errors";
import { NextPageWithLayout } from "../_app";
import { Spinner, Button, SingleTextInput } from "@commonground/design-system";
import { CurrentUserSession } from "../../types";
import { useToaster } from "../../providers/ToasterProvider";

import * as StyledSettings from "../../styles/Settings.styled";
import * as Styled from "../../styles/2fa.styled";
import {
  getCsrfToken,
  getErrorMessage,
  getIsTotpConfigured,
} from "../../services/kratos-flow-serializer";
import { t } from "../../../translations";
import { ProgressIndicators } from "../../components/progress-indicators/ProgressIndicators";
import {
  verifyProgressions,
  recoveryProgressions,
} from "../../services/page-progressions";

import backToPageIcon from "../../../public/images/arrow-left-circle-line.svg";
import MobileDevices from "../../../public/images/mobile-devices.svg";
import MobileMessage from "../../../public/images/mobile-message.svg";

const getQR = (data) => {
  const QR = data.ui.nodes.find((node) => node.attributes.id === "totp_qr");

  if (QR) {
    return QR.attributes.src;
  }
};

const Page: NextPageWithLayout<{
  currentUser: CurrentUserSession;
  handleWhoami: any;
}> = ({ currentUser, handleWhoami }) => {
  const [flow, setFlow] = useState<SettingsFlow>();
  const [qrCodeImage, setQrCodeImage] = useState();
  const [csrfToken, setCsrfToken] = useState();
  const [values, setValues] = useState({} as { totp_code: string });
  const [inputError, setInputError] = useState("");
  const [has2faConfigured, setHas2faConfigured] = useState<boolean | null>(
    null
  );

  const router = useRouter();
  const { flow: flowIdUrl, return_to: returnTo, screen } = router.query;

  const { showToast } = useToaster();

  useEffect(() => {
    handleWhoami();
  }, []);

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady) {
      return;
    }

    // // If ?flow=.. was in the URL, we fetch it
    if (flowIdUrl) {
      ory.frontend
        .getSettingsFlow({ id: String(flowIdUrl) })
        .then(({ data }) => {
          setFlow(data);
          const csrfToken = getCsrfToken(data);
          setCsrfToken(csrfToken);

          const nodesIncludeUnlink = getIsTotpConfigured(data);
          if (nodesIncludeUnlink) {
            setHas2faConfigured(true);
            return;
          }

          setHas2faConfigured(false);

          const QR = getQR(data);
          if (QR) {
            setQrCodeImage(QR);
          }
        })
        .catch(handleFlowError(router, "settings/2fa-app", setFlow))
        .catch((err) => {
          if (err.response?.status === 403 || err.response?.status === 401) {
            router.push("/login?aal=aal2");
            return;
          }
        });
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserSettingsFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        // NOTE: In the network tab the user identity is also given. This can be used to set the user in the sidepanel
        setFlow(data);
        const csrfToken = getCsrfToken(data);
        setCsrfToken(csrfToken);

        const nodesIncludeUnlink = getIsTotpConfigured(data);
        if (nodesIncludeUnlink) {
          setHas2faConfigured(true);
          return;
        }

        setHas2faConfigured(false);

        const QR = getQR(data);
        if (QR) {
          setQrCodeImage(QR);
        }
      })
      .catch((err) => {
        if (err.response?.status === 403 || err.response?.status === 401) {
          router.push("/login?aal=aal2");
          return;
        }
      });
  }, [router.isReady]);

  const onTotpSubmit = (values: UpdateSettingsFlowBody) =>
    router
      // On submission, add the flow ID to the URL but do not navigate. This prevents the user losing
      // his data when she/he reloads the page.
      .push(`/settings/2fa-app${flow ? `?flow=${flow.id}` : ""}`, undefined, {
        shallow: true,
      })
      .then(() =>
        ory.frontend
          .updateSettingsFlow({
            flow: String(flow?.id),
            updateSettingsFlowBody: values,
          })
          .then(({ data }) => {
            // The settings have been saved and the flow was updated. Let's show it to the user!
            setFlow(data);
            setInputError("");
            setHas2faConfigured(true);

            const nodesIncludeUnlink = getIsTotpConfigured(data);

            if (nodesIncludeUnlink) {
              router.push(`/recovery-done?screen=${screen}`);
              showToast({
                title:
                  "Je hebt je account succesvol gekoppeld via twee-factor-authenticatie.",
              });
              return;
            }
            location.href = "/settings/2fa-app";
          })
          .catch(handleFlowError(router, "settings/2fa-app", setFlow))
          .catch(async (err: AxiosError) => {
            // If the previous handler did not catch the error it's most likely a form validation error
            if (err.response?.status === 400) {
              // Yup, it is!
              setFlow(err.response?.data);

              const getInputError = getErrorMessage(err);
              setInputError(getInputError);
              return { error: getInputError };
            }

            return Promise.reject(err);
          })
      );

  return (
    <>
      <Head>
        <title>Configureer 2FA</title>
        <meta
          name="description"
          content="twee-factor-authenticatie configureren"
        />
      </Head>

      <ProgressIndicators
        progressions={
          screen === "verify"
            ? verifyProgressions
            : screen === "password-recovery"
            ? recoveryProgressions
            : []
        }
        currentprogress={has2faConfigured === null || has2faConfigured ? 2 : 1}
      />

      <Styled.ContentContainer>
        <StyledSettings.Heading>
          Twee-factor-authenticatie configureren
        </StyledSettings.Heading>

        <p>
          Om in te loggen vragen wij je om je twee-factor-authenticatie te
          configureren.
        </p>

        {has2faConfigured === null && <Spinner />}
        {has2faConfigured === true && (
          <Button
            onClick={(e: React.FormEvent) =>
              onTotpSubmit({
                csrf_token: csrfToken,
                totp_unlink: true,
                method: "totp",
              })
            }
            type="submit"
          >
            Twee-factor-authenticatie opnieuw instellen
          </Button>
        )}
        {has2faConfigured === false && (
          <>
            <Styled.StepContainer>
              <div>
                <MobileDevices />
              </div>
              <div>
                <p>Installeer een app</p>
                <p>
                  Download en installeer een van de volgende apps op jouw
                  telefoon of tablet:
                </p>
                <ul>
                  <a
                    href="https://www.microsoft.com/nl-nl/security/mobile-authenticator-app"
                    target="_blank"
                  >
                    <li>Microsoft Authenticator</li>
                  </a>
                  <a
                    href="https://play.google.com/store/apps/details?id=org.shadowice.flocke.andotp&hl=en_US"
                    target="_blank"
                  >
                    <li>andOTP</li>
                  </a>
                  <a
                    href="https://apps.apple.com/nl/app/google-authenticator/id388497605"
                    target="_blank"
                  >
                    <li>Google Authenticator</li>
                  </a>
                  <a href="https://authy.com/download/" target="_blank">
                    <li>Authy</li>
                  </a>
                </ul>
              </div>
            </Styled.StepContainer>

            <Styled.StepContainer>
              <div>{qrCodeImage && <img src={qrCodeImage} />}</div>
              <div>
                <p>Scan de QR-code</p>
                <p>Open de authenticatie app en:</p>
                <ul>
                  <li>
                    Voeg een autenticatie toe door de “+” icon aan te klikken in
                    de app.
                  </li>
                  <li>Scan de QR-code aan de linkerzijde met de telefoon.</li>
                </ul>
              </div>
            </Styled.StepContainer>

            <Styled.StepContainer>
              <div>
                <MobileMessage width="90" height="90" viewBox="0 0 80 80" />
              </div>
              <div>
                <p>Vul de verificatiecode in</p>
                <p>
                  Nadat de bovenstaande code is gescanned, vul de 6-cijferige
                  verificatiecode in die wordt gegenereerd door de app.
                </p>

                <div style={{ display: "flex" }}>
                  <SingleTextInput
                    onChange={(e) =>
                      setValues({
                        ...values,
                        totp_code: String(e.target.value),
                      })
                    }
                    name="totp_code"
                    size="s"
                  />
                  <Button
                    onClick={(e: React.FormEvent) =>
                      onTotpSubmit({
                        ...values,
                        csrf_token: csrfToken,
                        method: "totp",
                      })
                    }
                    // disabled={isLoading}
                    type="submit"
                    style={{ marginLeft: "1rem" }}
                  >
                    Opslaan
                  </Button>
                </div>
              </div>
            </Styled.StepContainer>
          </>
        )}

        {inputError && <p style={{ color: "red" }}>{t(inputError)}</p>}

        <Link href="/settings" passHref>
          <StyledSettings.NavigateWrapper>
            <Icon as={backToPageIcon} />
            Naar instellingen
          </StyledSettings.NavigateWrapper>
        </Link>
      </Styled.ContentContainer>
    </>
  );
};

export default Page;
