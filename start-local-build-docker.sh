#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error

# yarn build

# find out -name "*.js" | xargs -I % sh -c 'gzip %'

docker build --tag auth-service-front-end:1.0.0 .

GREEN=$'\e[0;32m'
RED=$'\e[0;31m'
NC=$'\e[0m'
echo "✓ ${RED}STARTING${NC} ${GREEN}DOCKER${NC} ON PORT 8080"

docker run --rm --name auth-service-front-end -p 8080:8080 auth-service-front-end:1.0.0
