// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { AxiosError } from "axios";
import { Button, Spinner, SingleTextInput } from "@commonground/design-system";
import { LoginFlow, UpdateLoginFlowBody } from "@ory/client";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Head from "next/head";

import { FormContainerCentered } from "../components/layout/SmallFormContainer.styled";
import { handleFlowError, handleGetFlowError } from "../../pkg/errors";
import { LogoutLink, Flow } from "../../pkg";
import { NextPageWithLayout } from "./_app";
import { t } from "../../translations";
import { useToaster } from "../providers/ToasterProvider";
import { whoami } from "../services/whoami";
import ory from "../../pkg/sdk";

import MobileMessage from "../../public/images/mobile-message.svg";

import {
  isCurrentUserTwoFactorConfigured,
  isUserPassing2FA,
} from "../services/user-passes-two-factor-check";
import {
  getCsrfToken,
  getErrorMessage,
} from "../services/kratos-flow-serializer";

import * as Styled from "../components/layout/OtherLink.styled";

const Page: NextPageWithLayout<any> = ({ currentUser, setCurrentUser }) => {
  const { showToast } = useToaster();

  const [hasSession, setHasSession] = useState(false);
  const [isFlowVisible, setIsFlowVisible] = useState(true);

  const [csrfToken, setCsrfToken] = useState();
  const [values, setValues] = useState({} as { totp_code: string });
  const [inputError, setInputError] = useState("");

  const [flow, setFlow] = useState<LoginFlow>();
  const [hasError, setHasError] = useState(false);

  const router = useRouter();
  const {
    return_to: returnTo,
    flow: flowId,
    // Refresh means we want to refresh the session. This is needed, for example, when we want to update the password of a user.
    refresh,
    // AAL = Authorization Assurance Level. This implies that we want to upgrade the AAL, meaning that we want to perform two-factor authentication/verification.
    aal,
  } = router.query;

  // This might be confusing, but we want to show the user an option
  // to sign out if they are performing two-factor authentication!
  const onLogout = LogoutLink([aal, refresh]);

  const isAdminPage = router.asPath.includes("admin");

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady || flow) {
      return;
    }

    (async () => {
      let isUserPassing2fa = true;

      if (!refresh) {
        const currentUserSession = await whoami();

        if (currentUserSession && !currentUserSession?.error) {
          // TODO: how often does this function run?
          if (!isUserPassing2FA({ user: currentUserSession })) {
            isUserPassing2fa = false;

            const configuredCredentials =
              await isCurrentUserTwoFactorConfigured();

            if (!configuredCredentials?.totp) {
              await router.push("/settings/2fa-app");
              return;
            }

            await router.push("/login?aal=aal2", undefined, { shallow: true });
          }
        }
      }

      // If ?flow=.. was in the URL, we fetch it
      if (flowId) {
        ory.frontend
          .getLoginFlow({ id: String(flowId) })
          .then(async ({ data }) => {
            const csrfToken = getCsrfToken(data);
            if (!data.id || !csrfToken) {
              return;
            }
            setFlow(data);
            setCsrfToken(csrfToken);
          })
          .catch((err) => {
            if (
              err.response?.data.error?.id === "session_already_available" &&
              !isUserPassing2fa
            ) {
              return;
            }
            return Promise.reject(err);
          })
          .catch((error) => {
            setHasError(true);
            error.response?.data.error?.id;
            console.warn("Error from Kratos:", error.response?.data.error);
          })
          .catch(handleGetFlowError(router, "login", setFlow))
          .catch((error) => {
            setHasError(true);
            console.warn("Error from Kratos:", error.response?.data);
          });
        return;
      }

      // Otherwise we initialize it
      ory.frontend
        .createBrowserLoginFlow({
          refresh: Boolean(refresh),
          aal: aal ? String(aal) : undefined,
          returnTo: returnTo ? String(returnTo) : undefined,
        })
        .then(({ data }) => {
          const csrfToken = getCsrfToken(data);
          if (!data.id || !csrfToken) {
            return;
          }
          setFlow(data);
          setCsrfToken(csrfToken);
        })
        .catch((err) => {
          // if (err.response?.data.error?.id === "session_already_available") {
          //   setInputError(
          //     "Je bent al ingelogd. Probeer te de pagina te verversen."
          //   );
          //   return;
          // }
          if (err.message === "Network Error") {
            setInputError(
              "Kon geen verbinding maken met de gebruikers API. Probeer het later nog eens of neem contact op met vip@vng.nl."
            );
            return;
          }

          if (
            err.response?.data.error?.id === "session_already_available" &&
            !isUserPassing2fa
          ) {
            return;
          }
          return Promise.reject(err);
        })
        .catch(handleFlowError(router, "login", setFlow))
        .catch((error) => {
          setHasError(true);
          console.warn("Error from Kratos:", error.response?.data.error);
        });
    })();
  }, [flowId, router, router.isReady, aal, refresh, returnTo, flow]);

  const onSubmit = (values: UpdateLoginFlowBody) =>
    router
      // On submission, add the flow ID to the URL but do not navigate. This prevents the user losing their data when they reload the page.
      .push(
        `/login?${flow ? `&flow=${flow.id}` : ""}${aal ? `&aal=${aal}` : ""}${
          refresh ? `&refresh=${refresh}` : ""
        }`,
        undefined,
        { shallow: true }
      )
      .then(() =>
        ory.frontend
          .updateLoginFlow({
            flow: String(flow?.id),
            updateLoginFlowBody: values,
          })
          // We logged in successfully! Let's bring the user home.
          .then(async ({ data }) => {
            if (flow?.return_to) {
              window.location.href = flow?.return_to;
              return;
            }

            setInputError("");

            // NOTE: We currently refetch the user as it returns the userType which the selfservice flow does not provide
            // TODO: Consider creating separate endpoints for usertype and permission. It would be a tiny bit faster as the request for the identity has already happened

            const currentUserSession = await whoami();

            if (!currentUserSession || currentUserSession?.error) {
              setCurrentUser({
                currentUser: null,
                currentUserIsLoading: false,
                currentUserError: true,
              });

              setInputError(
                "Kon geen verbinding maken met de gebruikers API. Probeer het later nog eens of neem contact op met vip@vng.nl."
              );

              return;
            }

            setHasSession(true);

            setCurrentUser({
              currentUser,
              currentUserIsLoading: false,
              currentUserError: false,
            });

            if (!isUserPassing2FA({ user: currentUserSession })) {
              setFlow(undefined);

              return Promise.reject({
                response: {
                  data: {
                    error: { id: "session_aal2_required" },
                  },
                },
              });
            }

            setIsFlowVisible(false);

            await new Promise((resolve) => setTimeout(resolve, 1000));

            if (router.asPath.includes("/admin-login")) {
              if (currentUserSession.userType.id === "administrator") {
                location.href = "/";
                return;
              }
            }

            location.href = "/permissies";
          })
          .catch(handleFlowError(router, "login", setFlow))
          .catch((err: AxiosError) => {
            if (err.response?.status === 401) {
              showToast({
                title: "Je kan momenteel niet inloggen",
                variant: "error",
              });
              return;
            }

            // If the previous handler did not catch the error it's most likely a form validation error
            if (err.response?.status === 400) {
              // Yup, it is!
              setFlow(err.response?.data);

              console.log(err.response);

              const getInputError = getErrorMessage(err);
              setInputError(getInputError);
              return;
            }
          })
      );

  if (hasError) {
    <h1>Er heeft zich een fout voorgedaan. Probeer het later nog eens</h1>;
  }

  if (!flow && inputError) {
    return (
      <p
        style={{
          color: "red",
          width: "100%",
          textAlign: "right",
          marginTop: "1rem",
        }}
      >
        {t(inputError)}
      </p>
    );
  }

  if (!flow) {
    return <Spinner />;
  }

  return (
    <>
      <Head>
        <title>Inloggen</title>
        <meta name="description" content="Login for VIP" />
      </Head>

      <FormContainerCentered>
        <h1>Welkom</h1>

        {!hasSession && (
          <>
            {isAdminPage ? (
              <p>Hier kunt u inloggen voor het Administrator dashboard.</p>
            ) : (
              <p>
                Hier kan je inloggen voor de datavoorzieningen van VNG
                Realisatie.
              </p>
            )}
          </>
        )}

        {isFlowVisible && (
          <>
            {aal !== "aal2" ? (
              <>
                <Flow onSubmit={onSubmit} flow={flow} />
                {!flow && inputError && (
                  <p
                    style={{
                      color: "red",
                      width: "100%",
                      textAlign: "right",
                      marginTop: "1rem",
                    }}
                  >
                    {t(inputError)}
                  </p>
                )}
              </>
            ) : (
              <form style={{ display: "flex", flexDirection: "column" }}>
                <div
                  style={{
                    height: "130px",
                    display: "grid",
                    gridTemplateColumns: "38% 1fr",
                    alignItems: "center",
                    marginBottom: "2rem",
                    marginTop: "1.5rem",
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      width: "124px",
                      height: "124px",
                    }}
                  >
                    <MobileMessage
                      viewBox="0 0 80 81"
                      style={{ width: "100%", height: "100%" }}
                    />
                  </div>

                  <p>
                    Open de authentiatie app op uw telefoon of tablet en voer de
                    verificatiecode in.
                  </p>
                </div>

                <label>
                  <p>Verificatiecode</p>
                  <SingleTextInput
                    onChange={(e) =>
                      setValues({ ...values, totp_code: e.target.value })
                    }
                    name="totp_code"
                    value={values.totp_code || ""}
                    style={{ width: "100%" }}
                  />
                </label>
                <Button
                  onClick={(e: React.FormEvent) => {
                    e.preventDefault();
                    onSubmit({
                      csrf_token: csrfToken,
                      totp_code: values.totp_code,
                      method: "totp",
                    });
                  }}
                  type="submit"
                  style={{ marginLeft: "auto" }}
                >
                  Log in
                </Button>

                {inputError && (
                  <p
                    style={{
                      color: "red",
                      width: "100%",
                      textAlign: "right",
                      marginTop: "1rem",
                    }}
                  >
                    {t(inputError)}
                  </p>
                )}

                <p
                  style={{
                    color: "#212121",
                    textAlign: "right",
                    width: "100%",
                    marginTop: "1rem",
                  }}
                >
                  <em>
                    2FA kwijt? Vraag de administrator van jouw gemeente een
                    nieuw account voor je aan te maken.
                  </em>
                </p>
              </form>
            )}

            {/* {aal === "aal2" && (
              <Link href="settings/2fa-app">
                <Styled.OtherLink style={{ marginTop: "2rem" }}>
                  Twee-factor-authenticatie configureren
                </Styled.OtherLink>
              </Link>
            )} */}

            {aal || refresh ? (
              <Styled.OtherLinkExternal
                data-testid="logout-link"
                onClick={onLogout}
              >
                Uitloggen
              </Styled.OtherLinkExternal>
            ) : (
              <>
                {!hasSession && (
                  <>
                    <Styled.OtherLink href="/verification">
                      Account verifiëren
                    </Styled.OtherLink>

                    <Styled.OtherLink href="/recovery">
                      Wachtwoord vergeten
                    </Styled.OtherLink>

                    <Styled.OtherLink href="/registration">
                      Account maken
                    </Styled.OtherLink>
                  </>
                )}
              </>
            )}
          </>
        )}
      </FormContainerCentered>
    </>
  );
};

export default Page;
