#!/usr/bin/env bash

set -u   # crash on missing env variables
set -e   # stop on any error
set -x   # print what we are doing

NAMESPACE=vng

export NEXT_PUBLIC_DEGO_URL=http://map.vng.com
export NEXT_PUBLIC_DOOK_URL=http://map.vng.com
export NEXT_PUBLIC_AUTH_API_URL=http://login.vng.com
export NEXT_PUBLIC_ANALYST_LOGIN_URL=http://login.vng.com/login
export NEXT_PUBLIC_ADMIN_LOGIN_URL=http://login.vng.com/admin-login
export NEXT_PUBLIC_KRATOS_PUBLIC_URL=http://id.vng.com

yarn build

docker build . -f Dockerfile -t localhost:32000/auth-service-front-end:1.0.0
docker push localhost:32000/auth-service-front-end:1.0.0


helm delete -n $NAMESPACE auth-service-front-end || true
helm upgrade --install  -n $NAMESPACE -f ./helm/values.vng.com.yaml auth-service-front-end ./helm
