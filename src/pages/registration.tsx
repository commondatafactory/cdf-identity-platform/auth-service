// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";
import { SingleTextInput, Button, Spinner } from "@commonground/design-system/";
import { RegistrationFlow } from "@ory/client";
import Head from "next/head";
import { useRouter } from "next/router";
import { FormEvent, useEffect, useState } from "react";
import { AxiosError } from "axios";

import ory from "../../pkg/sdk";
import { handleFlowError } from "../../pkg/errors";
import { FormContainerCentered } from "../components/layout/SmallFormContainer.styled";
import { NextPageWithLayout } from "./_app";
import { SidebarLayout } from "../components/layout/SidebarLayout";
import { useToaster } from "../providers/ToasterProvider";
import { createUser } from "../services/create-user";

import PasswordHiddenIcon from "../../public/images/eye-fill.svg";
import PasswordVisibleIcon from "../../public/images/eye-off-fill.svg";

import * as Styled from "../components/layout/OtherLink.styled";
import { t } from "../../translations";
import {
  getCsrfToken,
  getErrorMessage,
} from "../services/kratos-flow-serializer";
import { requestDegoPermissions } from "../services/request-dego-permissions";

interface NewAccountFormProps {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
}

const Page: NextPageWithLayout<any> = () => {
  const router = useRouter();

  const [inputError, setInputError] = useState("");
  const [csrfToken, setCsrfToken] = useState(null);

  // The "flow" represents a registration process and contains
  // information about the form we need to render (e.g. username + password)
  const [flow, setFlow] = useState<RegistrationFlow>();

  const [isPasswordHidden, setIsPasswordHidden] = useState(true);

  const { showToast } = useToaster();

  // Get ?flow=... from the URL
  const { flow: flowId, return_to: returnTo } = router.query;

  // In this effect we either initiate a new registration flow, or we fetch an existing registration flow.
  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady || flow) {
      return;
    }

    // If ?flow=.. was in the URL, we fetch it
    if (flowId) {
      ory.frontend
        .getRegistrationFlow({ id: String(flowId) })
        .then(({ data }) => {
          const csrfToken = getCsrfToken(data);
          if (!csrfToken) {
            console.log("failed to get CSRF token");
            return;
          }

          setFlow(data);
          setCsrfToken(csrfToken);
        })
        .catch(handleFlowError(router, "registration", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserRegistrationFlow({
        returnTo: returnTo ? String(returnTo) : undefined,
      })
      .then(({ data }) => {
        const csrfToken = getCsrfToken(data);
        if (!csrfToken) {
          console.log("failed to get CSRF token");
          return;
        }

        setFlow(data);
        setCsrfToken(csrfToken);
      })
      .catch(handleFlowError(router, "registration", setFlow));
  }, [flowId, router, router.isReady, returnTo, flow]);

  const handleSubmit = async (event: FormEvent) => {
    event.stopPropagation();
    event.preventDefault();

    const formData = new FormData(event.target as any);
    const formValues = {} as NewAccountFormProps;
    for (const pair of formData.entries()) {
      formValues[pair[0]] = pair[1];
    }

    const { email, firstName, lastName, password, confirmPassword } =
      formValues;

    if (!Object.values(formValues).every((value) => value)) {
      showToast({
        title: "Vul alle velden in.",
        variant: "error",
      });
      return;
    }

    if (password.length < 8) {
      showToast({
        title: "Maak je wachtwoord minimaal 8 karakters.",
        variant: "error",
      });
      return;
    }

    if (password !== confirmPassword) {
      showToast({
        title: "De wachtwoorden komen niet overeen.",
        variant: "error",
      });
      return;
    }

    if (!csrfToken) {
      showToast({
        title:
          "Er is iets misgegaan met de beveiliging van deze pagina. Ververs de pagina.",
        variant: "error",
      });
      return;
    }

    const values = {
      method: "password",
      password,
      traits: { email, name: { first: firstName, last: lastName } },
      csrf_token: csrfToken,
    } as const;

    await router
      // On submission, add the flow ID to the URL but do not navigate. This prevents the user loosing
      // his data when she/he reloads the page.
      .push(`/registration?flow=${flow?.id}`, undefined, { shallow: true });

    ory.frontend
      .updateRegistrationFlow({
        flow: String(flow?.id),
        updateRegistrationFlowBody: values,
      })
      .then(async ({ data }) => {
        // Add potential permissions for DEGO users
        await requestDegoPermissions();

        // continue_with is a list of actions that the user might need to take before the registration is complete.
        // It could, for example, contain a link to the verification form.
        if (data.continue_with) {
          for (const item of data.continue_with) {
            switch (item.action) {
              case "show_verification_ui":
                await router.push("/verification?flow=" + item.flow.id);
                return;
            }
          }
        }

        // If continue_with did not contain anything, we can just return to the home page.
        await router.push(flow?.return_to || "/");
      })
      .catch(handleFlowError(router, "registration", setFlow))
      .catch((err: AxiosError) => {
        // If the previous handler did not catch the error it's most likely a form validation error
        if (err.response?.status === 400) {
          // Yup, it is!
          const getInputError = getErrorMessage(err);
          setInputError(getInputError);

          setFlow(err.response?.data);
          return;
        }

        return Promise.reject(err);
      });
  };

  const handlePasswordVisibility = () => {
    setIsPasswordHidden(!isPasswordHidden);
  };

  return (
    <>
      <Head>
        <title>Nieuw account</title>
        <meta name="description" content="Account maken" />
      </Head>

      <FormContainerCentered>
        <h1>Welkom</h1>

        <p>
          Maak een nieuw account. Let op, als je een account voor DOOK wil
          maken, neem dan contact op via onderstaande e-mailadres. Wil je een
          account maken voor DEGO? Dan krijg je alleen toegang tot afgeschermde
          data, mits je e-mailadres namens een gemeente is en bij ons systeem
          bekend is.
        </p>
        <p>
          Heb je een vraag? Mail ons op{" "}
          <a href="mailto: vip@vng.nl">vip@vng.nl</a>.
        </p>

        <form
          onSubmit={handleSubmit}
          style={{ display: "flex", flexDirection: "column" }}
        >
          <label>
            <p>E-mail</p>
            <Input
              name="email"
              type="email"
              placeholder="jan@jannsen.nl"
              style={{ width: "100%" }}
              autocomplete="email"
            />
          </label>
          <label>
            <p>Voornaam</p>
            <Input
              type="text"
              placeholder="Jan"
              name="firstName"
              style={{ width: "100%" }}
              autocomplete="given-name"
            />
          </label>
          <label>
            <p>Achternaam</p>
            <Input
              type="text"
              name="lastName"
              placeholder="Jannsen"
              style={{ width: "100%" }}
              autocomplete="family-name"
            />
          </label>
          <label>
            <p>Wachtwoord</p>
            <SingleTextInput
              type={isPasswordHidden ? "password" : "text"}
              name="password"
              placeholder=""
              style={{ width: "100%" }}
              autocomplete="new-password"
              iconAtEnd={
                isPasswordHidden ? PasswordHiddenIcon : PasswordVisibleIcon
              }
              handleIconAtEnd={handlePasswordVisibility}
            />
          </label>
          <label>
            <p>Wachtwoord bevestigen</p>
            <SingleTextInput
              type={isPasswordHidden ? "password" : "text"}
              name="confirmPassword"
              placeholder=""
              style={{ width: "100%" }}
              autocomplete="new-password"
              iconAtEnd={
                isPasswordHidden ? PasswordHiddenIcon : PasswordVisibleIcon
              }
              handleIconAtEnd={handlePasswordVisibility}
            />
          </label>

          <p>
            Een wachtwoord moet minimaal uit 8 tekens bestaan en minstens een
            hoofdletter en teken bevatten.
          </p>

          <Button type="submit">Registeren</Button>

          {inputError && <p style={{ color: "red" }}>{t(inputError)}</p>}
        </form>

        <Styled.OtherLink href="/login">Inloggen</Styled.OtherLink>
      </FormContainerCentered>
    </>
  );
};

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <SidebarLayout>{page}</SidebarLayout>;
};

export default Page;
