// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Icon, Button } from "@commonground/design-system";
import Head from "next/head";
import Link from "next/link";
import { useRouter } from "next/router";
import { ReactElement, useEffect, useState } from "react";
import { FormProvider } from "react-hook-form";
import { SingleTextInput } from "@commonground/design-system/";
import { SettingsFlow } from "@ory/client";

import { CurrentUserSession } from "../../types";
import { getCsrfToken } from "../../services/kratos-flow-serializer";
import { handleFlowError } from "../../../pkg/errors";
import { isCurrentUserTwoFactorConfigured } from "../../services/user-passes-two-factor-check";
import { NextPageWithLayout } from "../_app";
import { SidebarLayout } from "../../components/layout/SidebarLayout";
import { t } from "../../../translations";
import { UsePasswordSchema, schema } from "../../hooks/use-password-schema";
import { useToaster } from "../../providers/ToasterProvider";
import ory from "../../../pkg/sdk";

import * as Styled from "../../styles/Settings.styled";

import BackToPageIcon from "../../../public/images/arrow-left-circle-line.svg";
import PasswordHiddenIcon from "../../../public/images/eye-fill.svg";
import PasswordVisibleIcon from "../../../public/images/eye-off-fill.svg";
import UserSettingsIcon from "../../../public/images/user-settings-line.svg";

interface Values {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  confirmPassword?: string;
}

const Page: NextPageWithLayout<{
  currentUser: CurrentUserSession;
  handleWhoami: any;
}> = ({ currentUser, handleWhoami }) => {
  const [flow, setFlow] = useState<SettingsFlow>();

  const [flowId, setFlowId] = useState("");
  const [inputError, setInputError] = useState("");

  const [values, setValues] = useState({} as Values);
  const [isLoading, setIsLoading] = useState(false);
  const [has2faConfigured, setHas2faConfigured] = useState<boolean | null>(
    null
  );

  const initialValues = currentUser?.identity?.traits;

  const [config, setConfig] = useState({
    setPasswordHidden: true,
    confirmPasswordHidden: true,
  });

  const [csrfToken, setCsrfToken] = useState("");

  const methods = UsePasswordSchema();

  // Get ?flow=... from the URL
  const router = useRouter();
  const { flow: flowIdUrl, return_to: returnTo } = router.query;

  const { showToast } = useToaster();

  useEffect(() => {
    (async () => {
      const isTwoFactorConfigured = await isCurrentUserTwoFactorConfigured();
      setHas2faConfigured(!!isTwoFactorConfigured?.totp);
    })();
  }, []);

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady) {
      return;
    }

    // // If ?flow=.. was in the URL, we fetch it
    if (flowIdUrl) {
      ory.frontend
        .getSettingsFlow({ id: String(flowIdUrl) })
        .then(({ data }) => {
          const csrfToken = getCsrfToken(data);
          if (!data.id || !csrfToken) {
            return;
          }
          setFlow(data);
          setCsrfToken(csrfToken);
        })
        .catch(handleFlowError(router, "settings", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserSettingsFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        const csrfToken = getCsrfToken(data);
        if (!data.id || !csrfToken) {
          return;
        }

        setFlow(data);
        setCsrfToken(csrfToken);
        setFlowId(data.id);
      })
      .catch(handleFlowError(router, "settings", setFlow));
  }, [router.isReady]);

  const onSubmit = async (values: Values) => {
    const updateUser = async (body) => {
      await router.push(`/settings?flow=${flowId}`, undefined, {
        shallow: true,
      });

      return await ory.frontend
        .updateSettingsFlow({
          flow: String(flow?.id),
          updateSettingsFlowBody: body,
        })
        .catch(handleFlowError(router, "settings", setFlow))
        .catch(async (err: AxiosError) => {
          // If the previous handler did not catch the error it's most likely a form validation error
          if (err.response?.status === 400) {
            const getInputError = err.response.data.ui.nodes.find(
              (node) => node.messages?.length > 0
            )?.messages[0].text;

            setInputError(getInputError);
            return { error: getInputError };
          }
        });
    };

    if (values.firstName || values.lastName || values.email) {
      const newUserData = {
        csrf_token: csrfToken,
        method: "profile",
        traits: {
          name: {
            first: values.firstName || initialValues?.name.first,
            last: values.lastName || initialValues?.name.last,
          },
          email: values.email || initialValues?.email,
        },
      };

      const update = await updateUser(newUserData);

      if (update?.error || update === undefined) {
        showToast({
          title:
            "Er is iets misgegaan bij het updaten van je gegevens. Probeer het nog eens.",
          variant: "error",
        });
        return;
      }

      await handleWhoami();
    }

    if (values.password) {
      const newUserData = {
        csrf_token: csrfToken,
        method: "password",
        password: values.password,
      };

      const update = await updateUser(newUserData);

      if (update?.error || update === undefined) {
        showToast({
          title:
            "Er is iets misgegaan bij het updaten van je nieuwe wachtwoord. Probeer het nog eens.",
          variant: "error",
        });
        return;
      }
    }

    showToast({
      title: "Je gegevens zijn aangepast.",
      variant: "success",
    });
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.stopPropagation();
    e.preventDefault();
    setInputError("");

    try {
      schema.validateSync(values);
    } catch (e) {
      setInputError(e.errors[0]);
      return;
    }

    if (isLoading) {
      return Promise.resolve();
    }

    setIsLoading(true);

    try {
      return await onSubmit(values);
    } finally {
      setIsLoading(false);
      setValues({});
    }
  };

  const handleConfig = (event: InputEvent, key: string, value: unknown) => {
    event.stopPropagation();
    setConfig({ ...config, [key]: value });
  };

  return (
    <>
      <Head>
        <title>Account instelling wijzigen</title>
        <meta name="description" content="Gegevens aanpassen" />
      </Head>
      <Styled.Heading>
        <Icon as={UserSettingsIcon} size="x-large" /> Account instelling
        wijzigen
      </Styled.Heading>

      <FormProvider {...methods}>
        <Styled.Form>
          <label>
            <p>Naam</p>
            <SingleTextInput
              onChange={(e) =>
                setValues({ ...values, firstName: e.target.value })
              }
              type="text"
              name="firstName"
              placeholder={initialValues?.name.first}
              value={values.firstName || ""}
              disabled={isLoading}
            />
          </label>
          <label>
            <p>Achternaam</p>
            <SingleTextInput
              onChange={(e) =>
                setValues({ ...values, lastName: e.target.value })
              }
              type="text"
              name="lastName"
              placeholder={initialValues?.name.last}
              value={values.lastName || ""}
              disabled={isLoading}
            />
          </label>
          <label>
            <p>E-mailadres</p>
            <SingleTextInput
              onChange={(e) => setValues({ ...values, email: e.target.value })}
              type="text"
              name="email"
              placeholder={initialValues?.email}
              value={values.email || ""}
              disabled={isLoading}
            />
          </label>

          <h3>Twee-factor-authenticatie configureren</h3>

          {has2faConfigured === false && (
            <p>
              <strong style={{ color: "red" }}>Let op!</strong> Je hebt nog geen
              2FA geconfigureerd.
            </p>
          )}

          <div style={{ width: "100%", marginBottom: "1rem" }}>
            <Link href={`/${"settings/2fa-app"}`}>
              <Button variant="secondary">2FA configureren</Button>
            </Link>
          </div>

          <h3>Wachtwoord aanpassen</h3>

          <label>
            <p>Nieuw wachtwoord</p>
            <SingleTextInput
              onChange={(e) =>
                setValues({ ...values, password: e.target.value })
              }
              type={config.setPasswordHidden ? "password" : "text"}
              name="password"
              value={values.password || ""}
              disabled={isLoading}
              iconAtEnd={
                config.setPasswordHidden
                  ? PasswordHiddenIcon
                  : PasswordVisibleIcon
              }
              handleIconAtEnd={(event) =>
                handleConfig(
                  event,
                  "setPasswordHidden",
                  !config.setPasswordHidden
                )
              }
            />
          </label>

          <label>
            <p>Wachtwoord bevestigen</p>
            <SingleTextInput
              onChange={(e) =>
                setValues({ ...values, confirmPassword: e.target.value })
              }
              type={config.confirmPasswordHidden ? "password" : "text"}
              name="confirmPassword"
              value={values.confirmPassword || ""}
              disabled={isLoading}
              iconAtEnd={
                config.confirmPasswordHidden
                  ? PasswordHiddenIcon
                  : PasswordVisibleIcon
              }
              handleIconAtEnd={(event) =>
                handleConfig(
                  event,
                  "confirmPasswordHidden",
                  !config.confirmPasswordHidden
                )
              }
            />
          </label>
          <p>Het wachtwoord moet minimaal 8 tekens zijn. </p>

          {inputError && <p style={{ color: "tomato" }}>{t(inputError)}</p>}

          <Button
            onClick={(e: React.FormEvent) => handleSubmit(e)}
            disabled={isLoading}
            type="submit"
          >
            Bewerkingen opslaan
          </Button>
        </Styled.Form>
      </FormProvider>

      <Link href={"/permissies"} passHref>
        <Styled.NavigateWrapper>
          <Icon as={BackToPageIcon} /> Terug naar overzicht - bewerkingen worden
          niet opgeslagen
        </Styled.NavigateWrapper>
      </Link>
    </>
  );
};

Page.getLayout = function getLayout(page: ReactElement, currentUser) {
  return <SidebarLayout currentUser={currentUser}>{page}</SidebarLayout>;
};

export default Page;
