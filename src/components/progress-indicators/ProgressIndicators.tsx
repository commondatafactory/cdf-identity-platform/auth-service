// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import CheckIcon from "../../../public/images/check.svg";
import { Button, Icon } from "@commonground/design-system";
import React, { useEffect, useState } from "react";

import * as Styled from "./ProgressIndicators.styled";

interface ProgressIndicators {
  progressions: {
    title: string;
  }[];
  currentprogress?: number;
}

export const ProgressIndicators = ({
  currentprogress = 0,
  progressions,
}: ProgressIndicators) => {
  return (
    <>
      <Styled.Wrapper>
        {progressions.map(({ title }, index) => (
          <React.Fragment key={title}>
            <Styled.Progression $isActive={currentprogress >= index}>
              <div>
                {index > 0 && <hr />}
                <span>
                  {index < currentprogress ? <CheckIcon /> : `${index + 1}.`}
                </span>
              </div>
              <p>{title}</p>
            </Styled.Progression>
          </React.Fragment>
        ))}
      </Styled.Wrapper>
      <Styled.Divider />
    </>
  );
};
