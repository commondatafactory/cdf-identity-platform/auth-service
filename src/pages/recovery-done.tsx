// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button, Spinner } from "@commonground/design-system";
import { NextPage } from "next";
import Head from "next/head";

import * as StyledLayout from "../components/layout/SmallFormContainer.styled";
import { ProgressIndicators } from "../components/progress-indicators/ProgressIndicators";
import {
  recoveryProgressions,
  verifyProgressions,
} from "../services/page-progressions";
import { useRouter } from "next/router";

const Page: NextPage<any> = () => {
  const router = useRouter();
  const { screen } = router.query;

  return (
    <>
      <Head>
        <title>Account</title>
        <meta name="description" content="Account" />
      </Head>

      <ProgressIndicators
        progressions={
          screen === "verify"
            ? verifyProgressions
            : "password-recovery"
            ? recoveryProgressions
            : []
        }
        currentprogress={9}
      />

      <StyledLayout.FormContainerCentered>
        <h1>Je account is nu compleet</h1>
        <p>
          Je bent nu ingelogd en kan je permissies voor de datavoorzieningen van
          VNG aan en uit zetten.
        </p>
        {/* <form style={{ display: "flex", flexDirection: "column" }}> */}
        <div style={{ display: "flex", marginTop: "3rem" }}>
          <Button style={{ marginLeft: "auto" }} as="a" href="/permissies">
            Naar permissies
          </Button>
        </div>
        {/* </form> */}
      </StyledLayout.FormContainerCentered>
    </>
  );
};

export default Page;
