// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { AxiosError } from "axios";
import { Button, Spinner } from "@commonground/design-system";
import { FormEvent, useEffect, useState } from "react";
import { NextPage } from "next";
import { RecoveryFlow } from "@ory/client";
import { useRouter } from "next/router";
import Head from "next/head";
import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";

import { handleFlowError } from "../../pkg/errors";
import { isEmailValid } from "../services/check-email-validity";
import { useToaster } from "../providers/ToasterProvider";
import ory from "../../pkg/sdk";

import * as Styled from "../components/layout/OtherLink.styled";
import * as StyledLayout from "../components/layout/SmallFormContainer.styled";
import { getPasswordRecovery } from "../services/get-password-recovery";
import { ProgressIndicators } from "../components/progress-indicators/ProgressIndicators";
import { recoveryProgressions } from "../services/page-progressions";

const Page: NextPage<any> = () => {
  const [flow, setFlow] = useState<RecoveryFlow | null>(null);
  const [hasSentRecoveryEmail, setHasSentRecoveryEmail] = useState(false);
  const [formError, setFormError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const { showToast } = useToaster();

  // Get ?flow=... from the URL
  const router = useRouter();
  const { flow: flowId, return_to: returnTo } = router.query;

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady || flow) {
      return;
    }

    // If ?flow=.. was in the URL, we fetch it
    if (flowId) {
      ory.frontend
        .getRecoveryFlow({ id: String(flowId) })
        .then(({ data }) => {
          setFlow(data);
        })
        .catch(handleFlowError(router, "recovery", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserRecoveryFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        setFlow(data);
      })
      .catch(handleFlowError(router, "recovery", setFlow))
      .catch((err: AxiosError) => {
        if (err.response?.status === 400) {
          setFlow(err.response?.data);
          return;
        }

        return Promise.reject(err);
      });
  }, [flowId, router, router.isReady, returnTo, flow]);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setFormError("");

    const email = (
      new FormData(event.currentTarget).get("email") as string
    ).toLowerCase();

    if (!email || !isEmailValid(email)) {
      setFormError("E-mail is niet valide.");
      return;
    }

    setIsLoading(true);

    const isPasswordRecovered = await getPasswordRecovery({ email });

    setIsLoading(false);

    if (!isPasswordRecovered) {
      showToast({
        variant: "error",
        title:
          "Er is iets misgegaan bij het herstellen van je wachtwoord. Probeer het later nog eens.",
      });
      return;
    }

    setHasSentRecoveryEmail(true);
  };

  return (
    <>
      <Head>
        <title>Account</title>
        <meta name="description" content="Account" />
      </Head>

      <ProgressIndicators
        progressions={recoveryProgressions}
        currentprogress={-1}
      />

      <StyledLayout.FormContainerCentered>
        <h1>Herstel je wachtwoord</h1>

        {(flow as any)?.error && <>{(flow as any).error.reason}</>}

        <form
          style={{ display: "flex", flexDirection: "column" }}
          onSubmit={onSubmit}
        >
          {formError && <p style={{ color: "red" }}>{formError}</p>}

          {!hasSentRecoveryEmail && !isLoading && (
            <>
              <label htmlFor="email">
                <p>E-mailadres</p>
              </label>
              <Input
                id="email"
                name="email"
                placeholder="annie@mail.nl"
                style={{ width: "100%" }}
              />
              <Button type="submit">Versturen</Button>
            </>
          )}
        </form>

        {hasSentRecoveryEmail && !isLoading && (
          <>
            <p>
              Als het opgegeven e-mailadres een geldig account is, wordt er een
              e-mail verzonden met instructies om het wachtwoord opnieuw in te
              stellen.
            </p>
            <p>
              E-mail niet ontvangen? Neem contact op met de administrator van
              jouw gemeente.
            </p>
          </>
        )}

        {isLoading && <Spinner style={{ marginTop: "16px" }} />}

        <Styled.OtherLink href="/login" style={{ marginTop: "16px" }}>
          Ga naar de login pagina
        </Styled.OtherLink>
      </StyledLayout.FormContainerCentered>
    </>
  );
};

export default Page;
