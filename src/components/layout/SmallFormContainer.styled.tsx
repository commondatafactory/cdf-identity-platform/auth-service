// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const FormContainerCentered = styled.div`
  margin: auto;
  width: 416px;

  & > form {
    display: flex;
    flex-direction: column;
  }

  button {
    margin-left: auto;
  }
`;

export const FormContainer = styled.div`
  margin: 0 auto;
  width: 416px;

  & > form {
    display: flex;
    flex-direction: column;
  }

  button {
    margin-left: auto;
  }
`;
