// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const fetchWithTimeout = async (
  resource,
  options?: RequestInit & { timeout?: number }
) => {
  const { timeout = 50000, ...fetchOptions } = options || {};

  const controller = new AbortController();
  const id = setTimeout(() => controller.abort(), timeout);
  const response = await fetch(resource, {
    ...fetchOptions,
    signal: controller.signal,
  });
  clearTimeout(id);

  return response;
};
