// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { SidebarLayout } from "../components/layout/SidebarLayout";
import { NextPageWithLayout } from "./_app";

import Head from "next/head";

const Page: NextPageWithLayout<any> = ({ currentUser, setCurrentUser }) => {
  return (
    <>
      <Head>
        <title>404</title>
        <meta name="description" content="404" />
      </Head>

      <h1>404</h1>
      <p>Deze pagina kan niet worden gevonden.</p>
    </>
  );
};

Page.getLayout = function getLayout(page: React.ReactElement) {
  return <SidebarLayout>{page}</SidebarLayout>;
};

export default Page;
