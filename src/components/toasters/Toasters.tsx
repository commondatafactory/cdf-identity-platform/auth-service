// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Toaster } from "./Toaster";
import React, { FC } from "react";
import { createPortal } from "react-dom";

type ToastersProps = {
  setToasters: any;
  toasters: any;
};

export const Toasters: FC<ToastersProps> = ({ toasters, setToasters }) => {
  return (
    <>
      {typeof window !== "undefined" &&
        createPortal(
          <>
            {toasters.map((toaster) => (
              <Toaster
                key={toaster.nth}
                setToasters={setToasters}
                {...toaster}
              />
            ))}
          </>,
          document.body
        )}
    </>
  );
};
