// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import Head from "next/head";
import { useEffect, useState } from "react";
import { Button, Icon, Spinner } from "@commonground/design-system";
import { Identity, IdentityState } from "@ory/client";

import { ActivateUserModal } from "../components/modal/ActivateUserModal";
import { CreateUserModal } from "../components/modal/CreateUserModal";
import { DataTable } from "../components/data-table/DataTable";
import { DeactivateUserModal } from "../components/modal/DeactivateUserModal";
import { deleteUser } from "../services/delete-user";
import { DeleteUserModal } from "../components/modal/DeleteUserModal";
import { editUser } from "../services/edit-user";
import { EditUserModal } from "../components/modal/EditUserModal";
import { Error } from "../components/error/Error";
import { getUsers } from "../services/get-users";
import { isUserPassing2FA } from "../services/user-passes-two-factor-check";
import { NextPageWithLayout } from "./_app";
import { useToaster } from "../providers/ToasterProvider";

import AddUserIcon from "../../public/images/add-circle-fill.svg";

type ModalStates =
  | "new-user"
  | "edit-user"
  | "delete"
  | "activate"
  | "deactivate";

const getIdentities = async (currentUserActiveOrganization) => {
  if (!currentUserActiveOrganization?.id) {
    return;
  }

  const query = new URLSearchParams({
    namespace: "organizations",
    object: currentUserActiveOrganization.id,
  }).toString();

  const identities = await getUsers(query);

  if (!identities) {
    throw "";
  }

  return identities;
};

const Page: NextPageWithLayout = ({
  handleWhoami,
  currentUser,
  currentUserIsLoading,
  currentUserError,
}) => {
  const [modalActive, setModalActive] = useState<null | ModalStates>(null);
  const [selectedRowId, setSelectedRowId] = useState("");
  const [{ identities, identitiesIsLoading, identitiesError }, setIdentities] =
    useState({
      identities: [] as Identity[],
      identitiesIsLoading: true,
      identitiesError: false,
    });
  const { showToast } = useToaster();

  useEffect(() => {
    (async () => {
      await handleWhoami();
    })();
  }, []);

  const currentUserActiveOrganization = currentUser?.permissions.find(
    ({ namespace, relation }) =>
      namespace === "organizations" && relation === "access"
  );

  useEffect(() => {
    if (currentUserIsLoading) {
      return;
    }

    if (currentUserError || !currentUser) {
      location.replace("/login");
      // location.replace("/admin-login");
      return;
    }

    if (!isUserPassing2FA({ user: currentUser })) {
      // router.push(`/admin-login?aal=aal2`);
      location.replace(`/login?aal=aal2`);
      return;
    }

    if (
      !currentUserActiveOrganization ||
      !currentUser.permissions.some(
        (permission) =>
          permission.object === currentUserActiveOrganization.object &&
          permission.relation === "admin"
      )
    ) {
      // router.push("/permissies");
      location.href = `/permissies`;
      return;
    }

    getIdentities(currentUserActiveOrganization)
      .then((identities) => {
        setIdentities({
          identities,
          identitiesIsLoading: false,
          identitiesError: false,
        });
      })
      .catch((e) => {
        showToast({
          title: "Kan geen gebruikers ophalen, probeer het later nog eens.",
          variant: "error",
        });
        setIdentities({
          identities: [],
          identitiesIsLoading: false,
          identitiesError: true,
        });
      });
  }, [currentUserIsLoading]);

  const updateIdentities = async () => {
    await getIdentities(currentUserActiveOrganization)
      .then((identities) => {
        setIdentities({
          identities,
          identitiesIsLoading: false,
          identitiesError: false,
        });
      })
      .catch((e) => {
        showToast({
          title: "Kan geen gebruikers ophalen, probeer het later nog eens.",
          variant: "error",
        });
        setIdentities({
          identities: [],
          identitiesIsLoading: false,
          identitiesError: true,
        });
      });

    setModalActive(null);
    setSelectedRowId("");
  };

  const handleCreatedNewUser = async (isExistingUser = false) => {
    await updateIdentities();

    showToast({
      title: isExistingUser
        ? "Bestaande gebruiker aangepast"
        : "Nieuwe gebruiker aangemaakt",
      variant: "success",
    });
  };

  const handleEditUser = async () => {
    await updateIdentities();
    showToast({
      title: "Gebruiker aangepast",
      variant: "success",
    });
  };

  const handleUsersActiveState = async (active: boolean) => {
    try {
      const identity = identities.find(
        (identity) => identity.id === selectedRowId
      );

      if (!identity) {
        throw "No similar identity found";
      }

      const updatedUserData = {
        schema_id: identity.schema_id,
        state: active ? "active" : ("inactive" as IdentityState),
        traits: identity.traits,
      };

      const editedUser = await editUser({
        id: identity.id,
        identity: updatedUserData,
      });

      if (editedUser.error) {
        throw editedUser.error;
      }

      await updateIdentities();

      showToast({
        title: active ? "Gebruiker gedeblokkeerd." : "Gebruiker geblokkeerd.",
        variant: "success",
      });
    } catch (e) {
      console.log(e);
      showToast({
        title: "Fout opgetreden bij wijzigen gebruiker status.",
        variant: "error",
      });
    }
  };

  const handleDeleteUsers = async () => {
    const identity = identities.find(
      (identity) => identity.id === selectedRowId
    );

    if (!identity) {
      showToast({
        title: "Fout opgetreden bij verwijderen gebruiker.",
        variant: "error",
      });
      return;
    }

    const deletedUser = await deleteUser({ id: identity.id });

    if (!deletedUser) {
      showToast({
        title: "Fout opgetreden bij verwijderen gebruiker.",
        variant: "error",
      });
      return;
    }

    await updateIdentities();
    showToast({
      title: "Gebruiker verwijderd.",
      variant: "success",
    });
  };

  if (currentUserIsLoading) {
    <Spinner />;
  }

  return (
    <>
      <Head>
        <title>Overzicht</title>
        <meta name="description" content="Overzicht" />
      </Head>

      <Button
        onClick={() => setModalActive("new-user")}
        disabled={identitiesIsLoading || identitiesError}
      >
        <Icon as={AddUserIcon} style={{ marginRight: "4px" }} />
        {!(identitiesIsLoading || identitiesError) && (
          <>Nieuwe gebruiker toevoegen</>
        )}
      </Button>

      <DataTable
        setModalActive={setModalActive}
        setSelectedRowId={setSelectedRowId}
        identities={identities}
        currentUserActiveOrganization={currentUserActiveOrganization}
      />

      {identitiesIsLoading ? (
        <div style={{ marginTop: "1rem" }}>
          <Spinner />{" "}
        </div>
      ) : identitiesError ? (
        <Error />
      ) : (
        <>
          <CreateUserModal
            isActive={modalActive === "new-user"}
            onConfirm={handleCreatedNewUser}
            onCancel={() => setModalActive(null)}
            currentUser={currentUser}
          />
          <EditUserModal
            isActive={modalActive === "edit-user"}
            editUser={identities.find(
              (identity) => identity.id === selectedRowId
            )}
            currentUser={currentUser}
            onConfirm={() => handleEditUser()}
            onCancel={() => setModalActive(null)}
          />
          <ActivateUserModal
            isActive={modalActive === "activate"}
            onConfirm={() => handleUsersActiveState(true)}
            onCancel={() => setModalActive(null)}
          />
          <DeactivateUserModal
            isActive={modalActive === "deactivate"}
            selectedRowId={selectedRowId}
            onConfirm={() => handleUsersActiveState(false)}
            onCancel={() => setModalActive(null)}
          />
          <DeleteUserModal
            isActive={modalActive === "delete"}
            selectedRowId={selectedRowId}
            onConfirm={handleDeleteUsers}
            onCancel={() => setModalActive(null)}
          />
        </>
      )}
    </>
  );
};

export default Page;
