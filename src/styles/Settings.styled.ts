// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const Heading = styled.h1`
  align-items: center;
  display: flex;

  > svg {
    margin-right: 0.5rem;
  }
`;

export const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
  padding-top: 2rem;
  width: 500px;

  label,
  input {
    width: 100%;
  }

  > h3 {
    font-weight: bold;
    margin-top: 3rem;
  }

  > button {
    display: block;
  }

  > p {
    margin-top: 1rem;
    width: 100%;
  }
`;

export const NavigateWrapper = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  margin-top: 5rem;

  svg {
    margin-right: 0.5rem;
  }

  :hover {
    text-decoration: underline;
  }
`;

export const FlowContainer = styled.div`
  margin-top: 1rem;

  img {
    min-width: 200px;
    max-width: 15vw;
  }
  pre,
  code {
    margin: 0;
    padding: 0;
    color: skyblue;
    font-family: inherit;
  }
`;
