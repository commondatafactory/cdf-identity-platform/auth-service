// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const isEmailValid = (email: string) =>
  email
    .toLowerCase()
    .match(
      /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
    );
