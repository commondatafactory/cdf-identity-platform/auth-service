// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import {
  Box,
  Chip,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { Button, Spinner } from "@commonground/design-system";
import { FC, FormEvent, useState } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";

import { Messages } from "../../../pkg";
import { getUserExistence } from "../../services/get-user-existence";

import InfoIcon from "../../../public/images/information-line.svg";
import WarningIcon from "../../../public/images/alert-line.svg";
import CheckIcon from "../../../public/images/check.svg";

export interface Values {
  firstName: string;
  surname: string;
  email: string;
  data: any[];
  administrators: any[];
}

export type Props = {
  onSubmit: (values: any, isExistingUser: boolean) => Promise<void>;
  hideGlobalMessages?: boolean;
  grantableOrganizations: any;
  grantableData: any;
  handleClose: any;
  currentUser: any;
  editUser: any | null;
  formError: string;
  setFormError: any;
  initialValues?: Values;
};

const schema = Yup.object().shape({
  firstName: Yup.string()
    .required("Naam is vereist.")
    .min(1, "Naam is te kort.")
    .max(50, "Naam te is lang."),
  surName: Yup.string()
    .required("Naam is vereist.")
    .min(1, "Naam is te kort.")
    .max(50, "Naam te is lang."),
  email: Yup.string()
    .required("Email is vereist.")
    .email("Is herkend valide e-mailadres."),
});

export const CreateUserForm: FC<Props> = ({
  onSubmit,
  hideGlobalMessages = false,
  grantableOrganizations,
  grantableData,
  handleClose: onClose,
  currentUser,
  formError,
  setFormError,
  editUser,
  initialValues = {},
}) => {
  const [values, setValues] = useState(initialValues as Values);
  const [isLoading, setIsLoading] = useState(false);
  const [isExistingUser, setIsExistingUser] = useState(false);

  const handleSubmit = (e: FormEvent) => {
    e.stopPropagation();
    e.preventDefault();

    if (isLoading) {
      return Promise.resolve();
    }

    setIsLoading(true);

    return onSubmit(values, isExistingUser).finally(() => {
      setIsLoading(false);
    });
  };

  const handleClose = (e: FormEvent) => {
    e.preventDefault();
    onClose();
  };

  const handleInputBlur = async ({ field, value }) => {
    if (!value) {
      return;
    }

    setIsExistingUser(false);
    setIsLoading(true);
    setFormError("");

    if (await getUserExistence({ field, value })) {
      setIsExistingUser(true);
    }

    setIsLoading(false);
  };

  const methods = useForm({
    mode: "onTouched",
    defaultValues: {
      firstName: "",
      surname: "",
      email: "",
      administrators: [],
      data: [],
    },
    resolver: yupResolver(schema),
    // handleSubmit
  });

  return (
    <FormProvider {...methods}>
      <form
        onSubmit={handleSubmit}
        style={{ display: "flex", flexWrap: "wrap" }}
      >
        {!hideGlobalMessages && <Messages messages={[]} />}

        <label style={{ width: "100%" }}>
          <p>Naam</p>
          <Input
            onChange={(e) =>
              setValues({ ...values, firstName: e.target.value })
            }
            type="text"
            placeholder="Jan"
            value={values.firstName || ""}
            disabled={isExistingUser || editUser || isLoading}
            style={{ width: "100%" }}
          />
        </label>

        <label style={{ width: "100%" }}>
          <p>Achternaam</p>
          <Input
            onChange={(e) => setValues({ ...values, surname: e.target.value })}
            type="text"
            placeholder="Janssen"
            value={values.surname || ""}
            disabled={isExistingUser || editUser || isLoading}
            style={{ width: "100%" }}
          />
        </label>

        <label style={{ width: "100%" }}>
          <p>E-mailadres</p>
          <Input
            onChange={(e) => setValues({ ...values, email: e.target.value })}
            type="email"
            disabled={editUser || isLoading}
            placeholder="jan@janssen.nl"
            value={values.email || ""}
            onBlur={(e) =>
              !editUser &&
              handleInputBlur({
                field: "email",
                value: e.target.value.trim(),
              })
            }
            style={{ width: "100%" }}
          />
        </label>

        {!editUser &&
          (isExistingUser ? (
            <span style={{ fontSize: "0.75rem", marginTop: "0.5rem" }}>
              <InfoIcon
                fill="rgb(11, 113, 161)"
                viewBox="0 0 24 24"
                width="18px"
                height="18px"
                style={{ verticalAlign: "text-top", marginRight: "0.25rem" }}
              />
              Deze gebruiker heeft al een account.
            </span>
          ) : (
            !isLoading &&
            values.email && (
              <span style={{ fontSize: "0.75rem", marginTop: "0.5rem" }}>
                <CheckIcon
                  viewBox="0 0 24 24"
                  color="green"
                  width="18px"
                  height="18px"
                  style={{ verticalAlign: "text-top", marginRight: "0.25rem" }}
                />
                E-mailadres is nog niet in gebruik.
              </span>
            )
          ))}

        <FormControl fullWidth style={{ marginTop: "2rem" }}>
          <InputLabel id="dataInputLabel">Datarol</InputLabel>
          <Select
            labelId="dataInputLabel"
            id="dataInput"
            label="Data toegang"
            multiple
            disabled={isLoading || isExistingUser}
            value={values.data || []}
            onChange={(e) =>
              setValues({
                ...values,
                data: e.target.value as typeof grantableData,
              })
            }
            renderValue={(muiValues) => (
              <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                {muiValues.map((value) => (
                  <Chip
                    key={value}
                    label={
                      grantableData.find((option) => option.value === value)
                        ?.label
                    }
                  />
                ))}
              </Box>
            )}
          >
            {grantableData.map((option) => (
              <MenuItem key={option.value} value={option.value as any}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
        </FormControl>

        <>
          <span style={{ fontSize: "0.75rem", marginTop: "0.5rem" }}>
            <WarningIcon
              fill="#DC3B30"
              width="18px"
              height="18px"
              style={{ verticalAlign: "text-top", marginRight: "0.25rem" }}
            />
            Let op! Je verleent hiermee rechten om beschermde data in te zien.
            Geef deze rechten altijd met beleid.
          </span>

          {!editUser && (
            <>
              <p style={{ marginTop: "1.5rem", marginBottom: "0.5rem" }}>
                Deze gebruiker wordt aangemaakt onder de organisatie:
              </p>

              <h4
                style={{
                  marginTop: 0,
                  marginBottom: "1.5rem",
                  fontSize: "1.5rem",
                  width: "100%",
                }}
              >
                <strong>{grantableOrganizations?.[0]?.label}</strong>
              </h4>
            </>
          )}
        </>

        {!editUser && (
          <p style={{ width: "100%", marginTop: "2rem" }}>
            {isExistingUser
              ? "De gebruiker zal een e-mail ontvangen met het bericht dat hun permisies zijn aangepast."
              : "De gebruiker zal een e-mail ontvangen om mee in te kunnen loggen."}
          </p>
        )}

        {formError && (
          <p style={{ color: "#DC3B30", width: "100%" }}>{formError}</p>
        )}

        {isLoading && (
          <div style={{ width: "100%" }}>
            <Spinner />
          </div>
        )}

        <Button
          onClick={handleClose}
          variant="secondary"
          disabled={isLoading}
          style={{ marginRight: "1rem" }}
        >
          Annuleren
        </Button>
        <Button
          onClick={(e: FormEvent) => handleSubmit(e)}
          disabled={isLoading || isExistingUser}
          type="submit"
        >
          {editUser
            ? "Update gebruiker en verstuur mailbevestiging"
            : "Maak gebruiker en verstuur mailbevestiging"}
        </Button>
      </form>
    </FormProvider>
  );
};
