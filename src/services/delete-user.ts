// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const deleteUser = async ({ id }) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users?id=${id}`,
      {
        method: "DELETE",
        credentials: "include",
      }
    );

    if (!response.ok) {
      return null;
    }

    return true;
  } catch (e) {
    console.log(e);
  }
};
