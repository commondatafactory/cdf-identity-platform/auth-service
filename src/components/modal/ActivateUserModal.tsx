// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Button } from "@commonground/design-system";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

export const ActivateUserModal = ({
  isActive,
  onConfirm,
  onCancel,
  handleClose = undefined,
}) => (
  <Dialog
    open={isActive}
    onClose={handleClose}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
  >
    <DialogTitle id="alert-dialog-title">Gebruiker deblokkeren</DialogTitle>
    <DialogContent>
      <DialogContentText id="alert-dialog-description">
        Weet je zeker dat je de gebruiker wil deblokkeren?
      </DialogContentText>
    </DialogContent>

    <DialogActions>
      <Button variant="secondary" onClick={onCancel}>
        Annuleren
      </Button>
      <Button onClick={onConfirm} autoFocus>
        Deblokkeren
      </Button>
    </DialogActions>
  </Dialog>
);
