// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as Yup from "yup";

export const schema = Yup.object()
  .shape({
    firstName: Yup.string()
      .min(2, "Naam is te kort.")
      .max(50, "Naam te is lang.")
      .nullable()
      .transform((value) => (!!value ? value : null)),
    lastName: Yup.string()
      .min(2, "Naam is te kort.")
      .max(50, "Naam te is lang.")
      .nullable()
      .transform((value) => (!!value ? value : null)),
    email: Yup.string()
      .email("Het e-mailadres veld is niet herkend als e-mailadres.")
      .nullable()
      .transform((value) => (!!value ? value : null)),
    password: Yup.string()
      .min(8, "Het nieuwe wachtwoord bevat geen 8 tekens.")
      .nullable()
      .transform((value) => (!!value ? value : null)),
    confirmPassword: Yup.string()
      .when("password", (password, schema) =>
        password
          ? schema
              .required(`"Wachtwoord bevestigen" is een verplicht veld.`)
              .test(
                "passwords-match",
                "Wachtwoorden komen niet overeen.",
                (value) => value === password
              )
          : schema
      )
      .transform((value) => (!!value ? value : null)),
  })
  .test("At least one of these fields needs to be filled", function (value) {
    if (
      !value.firstName &&
      !value.lastName &&
      !value.email &&
      !value.password &&
      !value.confirmPassword
    ) {
      return this.createError({
        message: "Geen van de velden is aangepast.",
      });
    }
    return true;
  });

export const UsePasswordSchema = () => {
  const methods = useForm({
    // mode: "onTouched",
    mode: "onSubmit",
    // defaultValues: {
    //   firstName: "",
    //   lastName: "",
    //   email: "",
    //   password: "",
    //   confirmPassword: "",
    // },
    resolver: yupResolver(schema),
    // onClick={(e: React.FormEvent) => handleSubmit(e)}
  });

  return methods;
};
