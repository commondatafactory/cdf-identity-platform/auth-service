// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const ContentContainer = styled.div`
  margin: 0 auto;
  width: min(646px, 80%);
`;

export const StepContainer = styled.div`
  display: grid;
  grid-template-columns: 0.3fr 0.7fr;
  height: 200px;
  padding: 20px 0;
  width: min(100%, 646px);

  & * {
    font-size: 14px;
    line-height: 1.5;
  }
  & a {
    color: ${({ theme }) => theme.colorTextLink};
  }
  & + & {
    border-top: 1px solid ${({ theme }) => theme.tokens.colorPaletteGray300};
  }
  & > div:first-child {
    margin: auto 0;

    & > img {
      max-width: 77%;
    }
  }
  & > div:last-child {
    & > p {
      margin: 0 0 10px;
    }
    & > p:first-child {
      font-size: 16px;
      font-weight: bold;
      margin-bottom: 6px;
    }
  }
`;
