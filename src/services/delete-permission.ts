// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const deletePermission = async ({ params }) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/permissions?${params}`,
      {
        credentials: "include",
        method: "DELETE",
      }
    );

    if (!response.ok) {
      return false;
    }

    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
};
