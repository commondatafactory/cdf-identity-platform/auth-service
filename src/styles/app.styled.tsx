// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const ButtonsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 0.5rem;
`;

export const Hr = styled.hr`
  width: 100%;
`;

// export const ButtonWrapper = styled(Button)`
//   svg {
//     margin-right: 0.3rem;
//   }
// `;

export const CustomButton = styled.button`
  align-items: center;
  display: flex;
  padding: 0.5rem;
  cursor: pointer;
  border: 0;
  background: transparent;

  :disabled {
    cursor: not-allowed;
  }

  svg {
    margin-right: 0.3rem;
  }
`;
