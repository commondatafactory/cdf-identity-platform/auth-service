// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import styled from "styled-components";

export const Container = styled.section`
  background: url("/images/bg.svg"), #dde1e8;
  background-position: top;
  padding: 48px;
`;

export const LogoWrapper = styled.div`
  align-items: center;
  color: #212121;
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 20px;
  width: 100%;

  svg {
    width: 56px;
  }
  p {
    font-weight: 600;
    margin: 10px 0 0;
    font-size: 1.5rem;
    color: #212121;
  }
  small {
    margin: 8px 0 0;
    font-size: 1rem;
    color: #9e9e9e;
  }
`;

export const LinksWrapper = styled.div`
  margin-top: 44px;
  padding: 20px;

  a {
    color: #757575;
    display: block;

    + a {
      margin-top: 8px;
    }

    svg {
      vertical-align: bottom;
      fill: #757575;
      margin-right: 4px;
    }
  }

  span + a {
    margin-top: 0.2rem;
  }

  .heading {
    margin: 2rem 0 0.5rem;
  }
`;
