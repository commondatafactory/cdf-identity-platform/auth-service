// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { NextPage } from "next";
import { AxiosError } from "axios";
import Head from "next/head";
import { useRouter } from "next/router";
import { FormEvent, useEffect, useState } from "react";
import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";
import { Button, Spinner } from "@commonground/design-system";

import { handleFlowError } from "../../pkg/errors";
import ory from "../../pkg/sdk";
import * as Styled from "../components/layout/OtherLink.styled";
import * as StyledLayout from "../components/layout/SmallFormContainer.styled";
import { ProgressIndicators } from "../components/progress-indicators/ProgressIndicators";
import { t } from "../../translations";
import {
  recoveryProgressions,
  verifyProgressions,
} from "../services/page-progressions";
import { RecoveryFlow } from "@ory/kratos-client";

const Page: NextPage<any> = () => {
  const [flow, setFlow] = useState<RecoveryFlow>();
  const [isLoading, setIsLoading] = useState(false);

  // Get ?flow=... from the URL
  const router = useRouter();
  const { flow: flowId, return_to: returnTo, screen } = router.query;

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady || flow) {
      return;
    }

    if (screen === "password-recovery") {
      if (flowId) {
        ory.frontend
          .getRecoveryFlow({ id: String(flowId) })
          .then(({ data }) => {
            setFlow(data);
          })
          .catch(handleFlowError(router, "recovery", setFlow));
        return;
      }

      ory.frontend
        .createBrowserRecoveryFlow({
          returnTo: String(returnTo || ""),
        })
        .then(({ data }) => {
          setFlow(data);
        })
        .catch(handleFlowError(router, "recovery", setFlow))
        .catch((err: AxiosError) => {
          if (err.response?.status === 400) {
            setFlow(err.response?.data);
            return;
          }

          return Promise.reject(err);
        });

      return;
    }

    // If ?flow=.. was in the URL, we fetch it
    if (flowId) {
      ory.frontend
        .getRecoveryFlow({ id: String(flowId) })
        .then(({ data }) => {
          setFlow(data);
        })
        .catch(handleFlowError(router, "recovery", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserRecoveryFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        setFlow(data);
      })
      .catch(handleFlowError(router, "recovery", setFlow))
      .catch((err: AxiosError) => {
        if (err.response?.status === 400) {
          setFlow(err.response?.data);
          return;
        }

        return Promise.reject(err);
      });
  }, [flowId, router, router.isReady, returnTo, flow]);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const code = new FormData(event.currentTarget).get("code") as string;

    if (!code) {
      return;
    }

    setIsLoading(true);

    ory.frontend
      .updateRecoveryFlow({
        flow: String(flow?.id),
        updateRecoveryFlowBody: { code, method: "code" },
      })
      .then(({ data }) => {
        setFlow(data);
      });
    ory.frontend
      .updateRecoveryFlow({
        flow: String(flow?.id),
        updateRecoveryFlowBody: { code, method: "code" },
      })
      .then(({ data }) => {
        setFlow(data);
      })
      .catch((err) => {
        if (err.response?.data.error?.id === "security_csrf_violation") {
          // router.push("/", undefined, { shallow: false });
        }

        // NOTE: KRATOS wants to initiate a redirect - after successfully using the activation code
        if (
          err.response?.data.error?.id === "browser_location_change_required"
        ) {
          const regex = /.*\?/; // NOTE: matches '?' character and everything before it

          const text = err.response.data.redirect_browser_to;
          const result = text.replace(regex, "");

          if (screen === "verify") {
            router.push(`/settings/2fa-app?screen=${screen}`);
            return;
          }

          router.push(
            `/recovery-new-password?${result}${
              screen ? `&screen=${screen}` : ""
            }`
          );
          return;
        }
        return Promise.reject(err);
      })
      .catch(handleFlowError(router, "recovery-code", setFlow))
      .catch((err: AxiosError) => {
        switch (err.response?.status) {
          case 400:
            // Status code 400 implies the form validation had an error
            setFlow(err.response?.data);
            return;
        }

        throw err;
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  if (!screen) {
    return <>
      <h1>Screen query parameter mist</h1>
      <Spinner />
    </>;
  }

  return (
    <>
      <Head>
        <title>Account</title>
        <meta name="description" content="Account" />
      </Head>

      <ProgressIndicators
        progressions={
          screen === "verify"
            ? verifyProgressions
            : screen === "password-recovery"
            ? recoveryProgressions
            : []
        }
        currentprogress={0}
      />

      <StyledLayout.FormContainer>
        <form
          style={{ display: "flex", flexDirection: "column" }}
          onSubmit={onSubmit}
        >
          <h1>Account verifieëren</h1>

          <p>
            Vul hier je verificatiecode in om je account in te stellen voor de
            datavoorzieningen DOOK & DEGO.
          </p>

          {flow?.ui?.messages && (
            <p style={{ color: "red" }}>{t(flow?.ui.messages[0].text)}</p>
          )}

          <label htmlFor="code">Verificatiecode</label>
          <Input id="code" name="code" style={{ width: "100%" }} />
          <Button type="submit" disabled={isLoading}>
            Code verifiëren
          </Button>
        </form>

        {isLoading && <Spinner style={{ marginTop: "16px" }} />}

        <Styled.OtherLink href="/login" style={{ marginTop: "16px" }}>
          Ga naar de login pagina
        </Styled.OtherLink>
      </StyledLayout.FormContainer>
    </>
  );
};

export default Page;
