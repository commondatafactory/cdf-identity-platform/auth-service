// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const updatePermissions = async ({ permissions, userId }) => {
  try {
    const response = await fetch(
      `${process.env.NEXT_PUBLIC_AUTH_API_URL}/permissions/${userId}`,
      {
        method: "PUT",
        body: JSON.stringify(permissions),
        credentials: "include",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    if (!response.ok) {
      return { error: "Kon gebruiker permissies niet bewerken" };
    }

    return await response.json();
  } catch (e) {
    console.log(e);
    return { error: "Kon gebruiker permissies niet bewerken" };
  }
};
