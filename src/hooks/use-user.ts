// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { UserContext, UserContextProps } from "../providers/UserProvider";
import { useContext } from "react";

export const useUser = (): UserContextProps => {
  return useContext(UserContext);
};
