// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { UserContainer } from "../user-container/UserContainer";
import * as Styled from "./SidebarLayout.styled";

export const SidebarLayout = ({
  children,
  currentUser,
  title = "Datavoorzieningen VNG",
  description = "Datavoorziening VNG Realisatie",
  // handleWhoami = undefined,
}) => {
  return (
    <Styled.Main>
      <UserContainer
        currentUser={currentUser}
        title={title}
        description={description}
      />
      <Styled.Content>{children}</Styled.Content>
    </Styled.Main>
  );
};
