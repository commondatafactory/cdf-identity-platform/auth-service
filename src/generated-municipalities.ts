// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

export const generatedMunicipalities = [
  {
    code: "1680",
    name: "Aa en Hunze",
    geographicCoordinate: [6.72726218, 52.99299743],
  },
  {
    code: "0358",
    name: "Aalsmeer",
    geographicCoordinate: [4.75502264, 52.25769678],
  },
  {
    code: "0197",
    name: "Aalten",
    geographicCoordinate: [6.56058823, 51.92174151],
  },
  {
    code: "0059",
    name: "Achtkarspelen",
    geographicCoordinate: [6.14313445, 53.2166695],
  },
  {
    code: "0482",
    name: "Alblasserdam",
    geographicCoordinate: [4.6657289, 51.86465403],
  },
  {
    code: "0613",
    name: "Albrandswaard",
    geographicCoordinate: [4.43571499, 51.85204293],
  },
  {
    code: "0361",
    name: "Alkmaar",
    geographicCoordinate: [4.80330755, 52.60179151],
  },
  {
    code: "0141",
    name: "Almelo",
    geographicCoordinate: [6.65819554, 52.34789061],
  },
  {
    code: "0034",
    name: "Almere",
    geographicCoordinate: [5.20565106, 52.40247126],
  },
  {
    code: "0484",
    name: "Alphen aan den Rijn",
    geographicCoordinate: [4.64095971, 52.11332171],
  },
  {
    code: "1723",
    name: "Alphen-Chaam",
    geographicCoordinate: [4.88736051, 51.50138141],
  },
  {
    code: "1959",
    name: "Altena",
    geographicCoordinate: [4.94593141, 51.76566264],
  },
  {
    code: "0060",
    name: "Ameland",
    geographicCoordinate: [5.76137119, 53.43072793],
  },
  {
    code: "0307",
    name: "Amersfoort",
    geographicCoordinate: [5.38469621, 52.17364981],
  },
  {
    code: "0362",
    name: "Amstelveen",
    geographicCoordinate: [4.85148663, 52.28926595],
  },
  {
    code: "0363",
    name: "Amsterdam",
    geographicCoordinate: [4.91978668, 52.36648685],
  },
  {
    code: "0200",
    name: "Apeldoorn",
    geographicCoordinate: [5.92190912, 52.18985812],
  },
  {
    code: "0202",
    name: "Arnhem",
    geographicCoordinate: [5.89259249, 52.00113721],
  },
  {
    code: "0106",
    name: "Assen",
    geographicCoordinate: [6.55246624, 53.00127298],
  },
  {
    code: "0743",
    name: "Asten",
    geographicCoordinate: [5.7792343, 51.3864068],
  },
  {
    code: "0744",
    name: "Baarle-Nassau",
    geographicCoordinate: [4.89314533, 51.43901683],
  },
  {
    code: "0308",
    name: "Baarn",
    geographicCoordinate: [5.26414159, 52.20383396],
  },
  {
    code: "0489",
    name: "Barendrecht",
    geographicCoordinate: [4.52543794, 51.85106556],
  },
  {
    code: "0203",
    name: "Barneveld",
    geographicCoordinate: [5.64185428, 52.16829943],
  },
  {
    code: "0888",
    name: "Beek",
    geographicCoordinate: [5.80913169, 50.93086354],
  },
  {
    code: "1954",
    name: "Beekdaelen",
    geographicCoordinate: [5.89712108, 50.93672588],
  },
  {
    code: "0889",
    name: "Beesel",
    geographicCoordinate: [6.06895713, 51.26916625],
  },
  {
    code: "1945",
    name: "Berg en Dal",
    geographicCoordinate: [5.94981026, 51.81994929],
  },
  {
    code: "1724",
    name: "Bergeijk",
    geographicCoordinate: [5.34000155, 51.31014121],
  },
  {
    code: "0893",
    name: "Bergen (L)",
    geographicCoordinate: [6.08938186, 51.59190802],
  },
  {
    code: "0373",
    name: "Bergen (NH)",
    geographicCoordinate: [4.66138652, 52.6631744],
  },
  {
    code: "0748",
    name: "Bergen op Zoom",
    geographicCoordinate: [4.28830357, 51.50345837],
  },
  {
    code: "1859",
    name: "Berkelland",
    geographicCoordinate: [6.57072545, 52.10092416],
  },
  {
    code: "1721",
    name: "Bernheze",
    geographicCoordinate: [5.52066425, 51.68468261],
  },
  {
    code: "0753",
    name: "Best",
    geographicCoordinate: [5.39652742, 51.5141149],
  },
  {
    code: "0209",
    name: "Beuningen",
    geographicCoordinate: [5.74027556, 51.86281538],
  },
  {
    code: "0375",
    name: "Beverwijk",
    geographicCoordinate: [4.64910613, 52.48480822],
  },
  {
    code: "0310",
    name: "De Bilt",
    geographicCoordinate: [5.17431243, 52.1417326],
  },
  {
    code: "1728",
    name: "Bladel",
    geographicCoordinate: [5.23873501, 51.3722883],
  },
  {
    code: "0376",
    name: "Blaricum",
    geographicCoordinate: [5.27423136, 52.28295893],
  },
  {
    code: "0377",
    name: "Bloemendaal",
    geographicCoordinate: [4.58153531, 52.38592744],
  },
  {
    code: "1901",
    name: "Bodegraven-Reeuwijk",
    geographicCoordinate: [4.76609101, 52.06541281],
  },
  {
    code: "0755",
    name: "Boekel",
    geographicCoordinate: [5.69372595, 51.60667429],
  },
  {
    code: "1681",
    name: "Borger-Odoorn",
    geographicCoordinate: [6.87198236, 52.90588184],
  },
  {
    code: "0147",
    name: "Borne",
    geographicCoordinate: [6.7435643, 52.31233067],
  },
  {
    code: "0654",
    name: "Borsele",
    geographicCoordinate: [3.82504756, 51.43204576],
  },
  {
    code: "0757",
    name: "Boxtel",
    geographicCoordinate: [5.32404125, 51.58115372],
  },
  {
    code: "0758",
    name: "Breda",
    geographicCoordinate: [4.76144145, 51.5851205],
  },
  {
    code: "1992",
    name: "Voorne aan Zee",
    geographicCoordinate: [4.16678789, 51.90665514],
  },
  {
    code: "1876",
    name: "Bronckhorst",
    geographicCoordinate: [6.30295508, 52.04523166],
  },
  {
    code: "0213",
    name: "Brummen",
    geographicCoordinate: [6.11850428, 52.10897314],
  },
  {
    code: "0899",
    name: "Brunssum",
    geographicCoordinate: [5.97800086, 50.94294215],
  },
  {
    code: "0312",
    name: "Bunnik",
    geographicCoordinate: [5.21694392, 52.04091567],
  },
  {
    code: "0313",
    name: "Bunschoten",
    geographicCoordinate: [5.35812068, 52.24230627],
  },
  {
    code: "0214",
    name: "Buren",
    geographicCoordinate: [5.40362739, 51.93833635],
  },
  {
    code: "0502",
    name: "Capelle aan den IJssel",
    geographicCoordinate: [4.5801377, 51.934929],
  },
  {
    code: "0383",
    name: "Castricum",
    geographicCoordinate: [4.67539409, 52.55668977],
  },
  {
    code: "0109",
    name: "Coevorden",
    geographicCoordinate: [6.73591162, 52.74025547],
  },
  {
    code: "1706",
    name: "Cranendonck",
    geographicCoordinate: [5.59617882, 51.28627898],
  },
  {
    code: "0216",
    name: "Culemborg",
    geographicCoordinate: [5.21038034, 51.94577786],
  },
  {
    code: "0148",
    name: "Dalfsen",
    geographicCoordinate: [6.27666931, 52.51472783],
  },
  {
    code: "1891",
    name: "Dantumadiel",
    geographicCoordinate: [5.98452117, 53.28004562],
  },
  {
    code: "0503",
    name: "Delft",
    geographicCoordinate: [4.36310563, 51.99845672],
  },
  {
    code: "0762",
    name: "Deurne",
    geographicCoordinate: [5.82697627, 51.43622051],
  },
  {
    code: "0150",
    name: "Deventer",
    geographicCoordinate: [6.23580956, 52.26823599],
  },
  {
    code: "0384",
    name: "Diemen",
    geographicCoordinate: [4.99049646, 52.33733243],
  },
  {
    code: "1980",
    name: "Dijk en Waard",
    geographicCoordinate: [4.82234626, 52.68215105],
  },
  {
    code: "1774",
    name: "Dinkelland",
    geographicCoordinate: [6.914139, 52.3730105],
  },
  {
    code: "0221",
    name: "Doesburg",
    geographicCoordinate: [6.14690009, 52.01867192],
  },
  {
    code: "0222",
    name: "Doetinchem",
    geographicCoordinate: [6.28670209, 51.96078705],
  },
  {
    code: "0766",
    name: "Dongen",
    geographicCoordinate: [4.95544944, 51.6358844],
  },
  {
    code: "0505",
    name: "Dordrecht",
    geographicCoordinate: [4.7088743, 51.7815073],
  },
  {
    code: "0498",
    name: "Drechterland",
    geographicCoordinate: [5.17323066, 52.65713565],
  },
  {
    code: "1719",
    name: "Drimmelen",
    geographicCoordinate: [4.75875212, 51.70014961],
  },
  {
    code: "0303",
    name: "Dronten",
    geographicCoordinate: [5.69535334, 52.51000993],
  },
  {
    code: "0225",
    name: "Druten",
    geographicCoordinate: [5.61214772, 51.87501197],
  },
  {
    code: "0226",
    name: "Duiven",
    geographicCoordinate: [6.01817144, 51.94654847],
  },
  {
    code: "1711",
    name: "Echt-Susteren",
    geographicCoordinate: [5.90820095, 51.08504222],
  },
  {
    code: "0385",
    name: "Edam-Volendam",
    geographicCoordinate: [5.03119916, 52.54383226],
  },
  {
    code: "0228",
    name: "Ede",
    geographicCoordinate: [5.72799921, 52.0764302],
  },
  {
    code: "0317",
    name: "Eemnes",
    geographicCoordinate: [5.28267822, 52.25189397],
  },
  {
    code: "1979",
    name: "Eemsdelta",
    geographicCoordinate: [6.87576115, 53.33383985],
  },
  {
    code: "0770",
    name: "Eersel",
    geographicCoordinate: [5.31461765, 51.39649044],
  },
  {
    code: "1903",
    name: "Eijsden-Margraten",
    geographicCoordinate: [5.77368729, 50.80106242],
  },
  {
    code: "0772",
    name: "Eindhoven",
    geographicCoordinate: [5.45853529, 51.45016177],
  },
  {
    code: "0230",
    name: "Elburg",
    geographicCoordinate: [5.84903486, 52.4132395],
  },
  {
    code: "0114",
    name: "Emmen",
    geographicCoordinate: [6.96196045, 52.74965272],
  },
  {
    code: "0388",
    name: "Enkhuizen",
    geographicCoordinate: [5.30336767, 52.75519161],
  },
  {
    code: "0153",
    name: "Enschede",
    geographicCoordinate: [6.8777881, 52.22080698],
  },
  {
    code: "0232",
    name: "Epe",
    geographicCoordinate: [5.95496795, 52.32551004],
  },
  {
    code: "0233",
    name: "Ermelo",
    geographicCoordinate: [5.66616371, 52.2882477],
  },
  {
    code: "0777",
    name: "Etten-Leur",
    geographicCoordinate: [4.64496614, 51.57831509],
  },
  {
    code: "1940",
    name: "De Fryske Marren",
    geographicCoordinate: [5.67065467, 52.89202769],
  },
  {
    code: "0779",
    name: "Geertruidenberg",
    geographicCoordinate: [4.8815833, 51.6971436],
  },
  {
    code: "1771",
    name: "Geldrop-Mierlo",
    geographicCoordinate: [5.58756033, 51.43127471],
  },
  {
    code: "1652",
    name: "Gemert-Bakel",
    geographicCoordinate: [5.75429063, 51.5407484],
  },
  {
    code: "0907",
    name: "Gennep",
    geographicCoordinate: [5.98710598, 51.69927475],
  },
  {
    code: "0784",
    name: "Gilze en Rijen",
    geographicCoordinate: [4.91857499, 51.55958578],
  },
  {
    code: "1924",
    name: "Goeree-Overflakkee",
    geographicCoordinate: [4.09998865, 51.75226813],
  },
  {
    code: "0664",
    name: "Goes",
    geographicCoordinate: [3.84439137, 51.51710719],
  },
  {
    code: "0785",
    name: "Goirle",
    geographicCoordinate: [5.03307, 51.51227457],
  },
  {
    code: "1942",
    name: "Gooise Meren",
    geographicCoordinate: [5.12326429, 52.31805224],
  },
  {
    code: "0512",
    name: "Gorinchem",
    geographicCoordinate: [4.97708275, 51.83846017],
  },
  {
    code: "0513",
    name: "Gouda",
    geographicCoordinate: [4.70620369, 52.0153015],
  },
  {
    code: "0518",
    name: "'s-Gravenhage",
    geographicCoordinate: [4.29300241, 52.07207297],
  },
  {
    code: "0014",
    name: "Groningen",
    geographicCoordinate: [6.62063474, 53.21859225],
  },
  {
    code: "1729",
    name: "Gulpen-Wittem",
    geographicCoordinate: [5.90014363, 50.8023435],
  },
  {
    code: "0158",
    name: "Haaksbergen",
    geographicCoordinate: [6.75586451, 52.15550341],
  },
  {
    code: "0392",
    name: "Haarlem",
    geographicCoordinate: [4.64753044, 52.38340808],
  },
  {
    code: "0394",
    name: "Haarlemmermeer",
    geographicCoordinate: [4.68645603, 52.30807947],
  },
  {
    code: "1655",
    name: "Halderberge",
    geographicCoordinate: [4.5231143, 51.59054023],
  },
  {
    code: "0160",
    name: "Hardenberg",
    geographicCoordinate: [6.57413563, 52.58564096],
  },
  {
    code: "0243",
    name: "Harderwijk",
    geographicCoordinate: [5.64415173, 52.34132254],
  },
  {
    code: "0523",
    name: "Hardinxveld-Giessendam",
    geographicCoordinate: [4.85242411, 51.83261469],
  },
  {
    code: "0072",
    name: "Harlingen",
    geographicCoordinate: [5.26759609, 53.14309469],
  },
  {
    code: "0244",
    name: "Hattem",
    geographicCoordinate: [6.05326318, 52.47477964],
  },
  {
    code: "0396",
    name: "Heemskerk",
    geographicCoordinate: [4.64044923, 52.51249252],
  },
  {
    code: "0397",
    name: "Heemstede",
    geographicCoordinate: [4.61536029, 52.34477975],
  },
  {
    code: "0246",
    name: "Heerde",
    geographicCoordinate: [6.05032967, 52.4060583],
  },
  {
    code: "0074",
    name: "Heerenveen",
    geographicCoordinate: [5.97800174, 52.99599109],
  },
  {
    code: "0917",
    name: "Heerlen",
    geographicCoordinate: [5.96943384, 50.89332342],
  },
  {
    code: "1658",
    name: "Heeze-Leende",
    geographicCoordinate: [5.56723844, 51.36086348],
  },
  {
    code: "0399",
    name: "Heiloo",
    geographicCoordinate: [4.70857318, 52.59985334],
  },
  {
    code: "0400",
    name: "Den Helder",
    geographicCoordinate: [4.78522541, 52.93997585],
  },
  {
    code: "0163",
    name: "Hellendoorn",
    geographicCoordinate: [6.45086008, 52.38636752],
  },
  {
    code: "1992",
    name: "Voorne aan Zee",
    geographicCoordinate: [4.12552199, 51.83956163],
  },
  {
    code: "0794",
    name: "Helmond",
    geographicCoordinate: [5.6569698, 51.47615123],
  },
  {
    code: "0531",
    name: "Hendrik-Ido-Ambacht",
    geographicCoordinate: [4.63879045, 51.84179458],
  },
  {
    code: "0164",
    name: "Hengelo (O)",
    geographicCoordinate: [6.77819416, 52.25490615],
  },
  {
    code: "0796",
    name: "'s-Hertogenbosch",
    geographicCoordinate: [5.35192613, 51.71680934],
  },
  {
    code: "0252",
    name: "Heumen",
    geographicCoordinate: [5.81848239, 51.77788922],
  },
  {
    code: "0797",
    name: "Heusden",
    geographicCoordinate: [5.16019081, 51.69676199],
  },
  {
    code: "0534",
    name: "Hillegom",
    geographicCoordinate: [4.57695285, 52.29677242],
  },
  {
    code: "0798",
    name: "Hilvarenbeek",
    geographicCoordinate: [5.14545605, 51.47952464],
  },
  {
    code: "0402",
    name: "Hilversum",
    geographicCoordinate: [5.16803652, 52.22310922],
  },
  {
    code: "1963",
    name: "Hoeksche Waard",
    geographicCoordinate: [4.44870502, 51.76456719],
  },
  {
    code: "1735",
    name: "Hof van Twente",
    geographicCoordinate: [6.58995897, 52.2364413],
  },
  {
    code: "1966",
    name: "Het Hogeland",
    geographicCoordinate: [6.53801174, 53.42818599],
  },
  {
    code: "1911",
    name: "Hollands Kroon",
    geographicCoordinate: [4.99274391, 52.88571599],
  },
  {
    code: "0118",
    name: "Hoogeveen",
    geographicCoordinate: [6.51359302, 52.72309409],
  },
  {
    code: "0405",
    name: "Hoorn",
    geographicCoordinate: [5.07193643, 52.62992633],
  },
  {
    code: "1507",
    name: "Horst aan de Maas",
    geographicCoordinate: [6.04163949, 51.4448658],
  },
  {
    code: "0321",
    name: "Houten",
    geographicCoordinate: [5.18171526, 52.00409513],
  },
  {
    code: "0406",
    name: "Huizen",
    geographicCoordinate: [5.2420639, 52.2986469],
  },
  {
    code: "0677",
    name: "Hulst",
    geographicCoordinate: [4.06497303, 51.33324925],
  },
  {
    code: "0353",
    name: "IJsselstein",
    geographicCoordinate: [5.02879098, 52.02771247],
  },
  {
    code: "1884",
    name: "Kaag en Braassem",
    geographicCoordinate: [4.62919402, 52.19258136],
  },
  {
    code: "0166",
    name: "Kampen",
    geographicCoordinate: [5.92332641, 52.55835835],
  },
  {
    code: "0678",
    name: "Kapelle",
    geographicCoordinate: [3.97045109, 51.49498012],
  },
  {
    code: "0537",
    name: "Katwijk",
    geographicCoordinate: [4.41629104, 52.19230128],
  },
  {
    code: "0928",
    name: "Kerkrade",
    geographicCoordinate: [6.05097354, 50.87182571],
  },
  {
    code: "1598",
    name: "Koggenland",
    geographicCoordinate: [4.94884689, 52.64445574],
  },
  {
    code: "0542",
    name: "Krimpen aan den IJssel",
    geographicCoordinate: [4.60210186, 51.91469695],
  },
  {
    code: "1931",
    name: "Krimpenerwaard",
    geographicCoordinate: [4.73718195, 51.95238959],
  },
  {
    code: "1659",
    name: "Laarbeek",
    geographicCoordinate: [5.61177235, 51.53092236],
  },
  {
    code: "1982",
    name: "Land van Cuijk",
    geographicCoordinate: [5.85913205, 51.65548544],
  },
  {
    code: "0882",
    name: "Landgraaf",
    geographicCoordinate: [6.03298576, 50.90642908],
  },
  {
    code: "0415",
    name: "Landsmeer",
    geographicCoordinate: [4.92455555, 52.45058311],
  },
  {
    code: "1621",
    name: "Lansingerland",
    geographicCoordinate: [4.50610722, 52.00400827],
  },
  {
    code: "0417",
    name: "Laren",
    geographicCoordinate: [5.2170741, 52.24718046],
  },
  {
    code: "0080",
    name: "Leeuwarden",
    geographicCoordinate: [5.79791655, 53.17002143],
  },
  {
    code: "0546",
    name: "Leiden",
    geographicCoordinate: [4.48511551, 52.15498245],
  },
  {
    code: "0547",
    name: "Leiderdorp",
    geographicCoordinate: [4.54201231, 52.15665179],
  },
  {
    code: "1916",
    name: "Leidschendam-Voorburg",
    geographicCoordinate: [4.42229432, 52.09033051],
  },
  {
    code: "0995",
    name: "Lelystad",
    geographicCoordinate: [5.37476449, 52.54272549],
  },
  {
    code: "1640",
    name: "Leudal",
    geographicCoordinate: [5.88481708, 51.23944287],
  },
  {
    code: "0327",
    name: "Leusden",
    geographicCoordinate: [5.41644531, 52.12222391],
  },
  {
    code: "1705",
    name: "Lingewaard",
    geographicCoordinate: [5.94222835, 51.90153125],
  },
  {
    code: "0553",
    name: "Lisse",
    geographicCoordinate: [4.54583719, 52.25555166],
  },
  {
    code: "0262",
    name: "Lochem",
    geographicCoordinate: [6.34685481, 52.17948247],
  },
  {
    code: "0809",
    name: "Loon op Zand",
    geographicCoordinate: [5.04754005, 51.64056952],
  },
  {
    code: "0331",
    name: "Lopik",
    geographicCoordinate: [4.94552285, 51.98535812],
  },
  {
    code: "0168",
    name: "Losser",
    geographicCoordinate: [7.00274789, 52.3028096],
  },
  {
    code: "0263",
    name: "Maasdriel",
    geographicCoordinate: [5.29692443, 51.77625082],
  },
  {
    code: "1641",
    name: "Maasgouw",
    geographicCoordinate: [5.88238537, 51.16157134],
  },
  {
    code: "1991",
    name: "Maashorst",
    geographicCoordinate: [5.65548228, 51.68677198],
  },
  {
    code: "0556",
    name: "Maassluis",
    geographicCoordinate: [4.24886654, 51.92519893],
  },
  {
    code: "0935",
    name: "Maastricht",
    geographicCoordinate: [5.6995303, 50.85296715],
  },
  {
    code: "0420",
    name: "Medemblik",
    geographicCoordinate: [5.14403891, 52.7701836],
  },
  {
    code: "0938",
    name: "Meerssen",
    geographicCoordinate: [5.75632227, 50.90131431],
  },
  {
    code: "1948",
    name: "Meierijstad",
    geographicCoordinate: [5.50639696, 51.59223671],
  },
  {
    code: "0119",
    name: "Meppel",
    geographicCoordinate: [6.19384026, 52.71593088],
  },
  {
    code: "0687",
    name: "Middelburg",
    geographicCoordinate: [3.65083605, 51.50315836],
  },
  {
    code: "1842",
    name: "Midden-Delfland",
    geographicCoordinate: [4.30614282, 51.96475322],
  },
  {
    code: "1731",
    name: "Midden-Drenthe",
    geographicCoordinate: [6.54275983, 52.86735934],
  },
  {
    code: "1952",
    name: "Midden-Groningen",
    geographicCoordinate: [6.80744467, 53.19533336],
  },
  {
    code: "1709",
    name: "Moerdijk",
    geographicCoordinate: [4.53547659, 51.66257589],
  },
  {
    code: "1978",
    name: "Molenlanden",
    geographicCoordinate: [4.8451901, 51.88467125],
  },
  {
    code: "1955",
    name: "Montferland",
    geographicCoordinate: [6.21377868, 51.91689332],
  },
  {
    code: "0335",
    name: "Montfoort",
    geographicCoordinate: [4.94404799, 52.04770843],
  },
  {
    code: "0944",
    name: "Mook en Middelaar",
    geographicCoordinate: [5.89966499, 51.74736551],
  },
  {
    code: "1740",
    name: "Neder-Betuwe",
    geographicCoordinate: [5.58686712, 51.91588378],
  },
  {
    code: "0946",
    name: "Nederweert",
    geographicCoordinate: [5.7733212, 51.29435762],
  },
  {
    code: "0356",
    name: "Nieuwegein",
    geographicCoordinate: [5.09467157, 52.03081752],
  },
  {
    code: "0569",
    name: "Nieuwkoop",
    geographicCoordinate: [4.77946913, 52.17078647],
  },
  {
    code: "0267",
    name: "Nijkerk",
    geographicCoordinate: [5.48436285, 52.21212636],
  },
  {
    code: "0268",
    name: "Nijmegen",
    geographicCoordinate: [5.83679287, 51.83850928],
  },
  {
    code: "1930",
    name: "Nissewaard",
    geographicCoordinate: [4.28205443, 51.83171434],
  },
  {
    code: "1970",
    name: "Noardeast-Fryslân",
    geographicCoordinate: [6.00414185, 53.35193015],
  },
  {
    code: "1695",
    name: "Noord-Beveland",
    geographicCoordinate: [3.79644089, 51.57324444],
  },
  {
    code: "1699",
    name: "Noordenveld",
    geographicCoordinate: [6.44030741, 53.09562481],
  },
  {
    code: "0171",
    name: "Noordoostpolder",
    geographicCoordinate: [5.72979141, 52.71600419],
  },
  {
    code: "0575",
    name: "Noordwijk",
    geographicCoordinate: [4.47618959, 52.27114511],
  },
  {
    code: "0820",
    name: "Nuenen, Gerwen en Nederwetten",
    geographicCoordinate: [5.54946413, 51.47860189],
  },
  {
    code: "0302",
    name: "Nunspeet",
    geographicCoordinate: [5.78624642, 52.33925119],
  },
  {
    code: "0579",
    name: "Oegstgeest",
    geographicCoordinate: [4.47121053, 52.18555271],
  },
  {
    code: "0823",
    name: "Oirschot",
    geographicCoordinate: [5.28978042, 51.49261399],
  },
  {
    code: "0824",
    name: "Oisterwijk",
    geographicCoordinate: [5.20448126, 51.56246603],
  },
  {
    code: "1895",
    name: "Oldambt",
    geographicCoordinate: [7.07466754, 53.212347],
  },
  {
    code: "0269",
    name: "Oldebroek",
    geographicCoordinate: [5.93714281, 52.45694092],
  },
  {
    code: "0173",
    name: "Oldenzaal",
    geographicCoordinate: [6.91104324, 52.30772581],
  },
  {
    code: "1773",
    name: "Olst-Wijhe",
    geographicCoordinate: [6.15334559, 52.36678235],
  },
  {
    code: "0175",
    name: "Ommen",
    geographicCoordinate: [6.43476189, 52.51612993],
  },
  {
    code: "1586",
    name: "Oost Gelre",
    geographicCoordinate: [6.57573452, 52.00983656],
  },
  {
    code: "0826",
    name: "Oosterhout",
    geographicCoordinate: [4.86406798, 51.6377738],
  },
  {
    code: "0085",
    name: "Ooststellingwerf",
    geographicCoordinate: [6.28296848, 52.98236914],
  },
  {
    code: "0431",
    name: "Oostzaan",
    geographicCoordinate: [4.87610965, 52.45196353],
  },
  {
    code: "0432",
    name: "Opmeer",
    geographicCoordinate: [4.95347368, 52.7152802],
  },
  {
    code: "0086",
    name: "Opsterland",
    geographicCoordinate: [6.1127567, 53.04915044],
  },
  {
    code: "0828",
    name: "Oss",
    geographicCoordinate: [5.52640134, 51.78156829],
  },
  {
    code: "1509",
    name: "Oude IJsselstreek",
    geographicCoordinate: [6.40949596, 51.90782484],
  },
  {
    code: "0437",
    name: "Ouder-Amstel",
    geographicCoordinate: [4.91351553, 52.28937604],
  },
  {
    code: "0589",
    name: "Oudewater",
    geographicCoordinate: [4.86423913, 52.027455],
  },
  {
    code: "1734",
    name: "Overbetuwe",
    geographicCoordinate: [5.77833014, 51.92430329],
  },
  {
    code: "0590",
    name: "Papendrecht",
    geographicCoordinate: [4.703344, 51.83323777],
  },
  {
    code: "1894",
    name: "Peel en Maas",
    geographicCoordinate: [5.99484977, 51.33630217],
  },
  {
    code: "0765",
    name: "Pekela",
    geographicCoordinate: [6.9729452, 53.07058761],
  },
  {
    code: "1926",
    name: "Pijnacker-Nootdorp",
    geographicCoordinate: [4.4219536, 52.01752998],
  },
  {
    code: "0439",
    name: "Purmerend",
    geographicCoordinate: [4.93068585, 52.54358285],
  },
  {
    code: "0273",
    name: "Putten",
    geographicCoordinate: [5.5826715, 52.24390009],
  },
  {
    code: "0177",
    name: "Raalte",
    geographicCoordinate: [6.27904954, 52.38782664],
  },
  {
    code: "0703",
    name: "Reimerswaal",
    geographicCoordinate: [4.12082762, 51.44147312],
  },
  {
    code: "0274",
    name: "Renkum",
    geographicCoordinate: [5.78484538, 51.9896193],
  },
  {
    code: "0339",
    name: "Renswoude",
    geographicCoordinate: [5.53416273, 52.06894743],
  },
  {
    code: "1667",
    name: "Reusel-De Mierden",
    geographicCoordinate: [5.14775308, 51.37825533],
  },
  {
    code: "0275",
    name: "Rheden",
    geographicCoordinate: [6.05125191, 52.03354008],
  },
  {
    code: "0340",
    name: "Rhenen",
    geographicCoordinate: [5.56052991, 51.9807674],
  },
  {
    code: "0597",
    name: "Ridderkerk",
    geographicCoordinate: [4.59610831, 51.86879441],
  },
  {
    code: "1742",
    name: "Rijssen-Holten",
    geographicCoordinate: [6.43396864, 52.29142979],
  },
  {
    code: "0603",
    name: "Rijswijk",
    geographicCoordinate: [4.32603246, 52.03619628],
  },
  {
    code: "1669",
    name: "Roerdalen",
    geographicCoordinate: [6.04109003, 51.14569774],
  },
  {
    code: "0957",
    name: "Roermond",
    geographicCoordinate: [6.01147665, 51.20119012],
  },
  {
    code: "0736",
    name: "De Ronde Venen",
    geographicCoordinate: [4.91728366, 52.22633474],
  },
  {
    code: "1674",
    name: "Roosendaal",
    geographicCoordinate: [4.42451062, 51.51319146],
  },
  {
    code: "0599",
    name: "Rotterdam",
    geographicCoordinate: [4.27687952, 51.93101775],
  },
  {
    code: "0277",
    name: "Rozendaal",
    geographicCoordinate: [5.96789298, 52.04253753],
  },
  {
    code: "0840",
    name: "Rucphen",
    geographicCoordinate: [4.55856248, 51.52267275],
  },
  {
    code: "0441",
    name: "Schagen",
    geographicCoordinate: [4.74221741, 52.78732555],
  },
  {
    code: "0279",
    name: "Scherpenzeel",
    geographicCoordinate: [5.50049086, 52.08932339],
  },
  {
    code: "0606",
    name: "Schiedam",
    geographicCoordinate: [4.38526221, 51.92675598],
  },
  {
    code: "0088",
    name: "Schiermonnikoog",
    geographicCoordinate: [6.22231667, 53.4744383],
  },
  {
    code: "1676",
    name: "Schouwen-Duiveland",
    geographicCoordinate: [3.88283783, 51.68217471],
  },
  {
    code: "0965",
    name: "Simpelveld",
    geographicCoordinate: [5.98664482, 50.82880409],
  },
  {
    code: "0845",
    name: "Sint-Michielsgestel",
    geographicCoordinate: [5.37812154, 51.65706848],
  },
  {
    code: "1883",
    name: "Sittard-Geleen",
    geographicCoordinate: [5.82820441, 51.00485878],
  },
  {
    code: "0610",
    name: "Sliedrecht",
    geographicCoordinate: [4.7729476, 51.83068699],
  },
  {
    code: "1714",
    name: "Sluis",
    geographicCoordinate: [3.52310338, 51.33341393],
  },
  {
    code: "0090",
    name: "Smallingerland",
    geographicCoordinate: [6.03892973, 53.1134227],
  },
  {
    code: "0342",
    name: "Soest",
    geographicCoordinate: [5.29628873, 52.15619081],
  },
  {
    code: "0847",
    name: "Someren",
    geographicCoordinate: [5.69142591, 51.37872919],
  },
  {
    code: "0848",
    name: "Son en Breugel",
    geographicCoordinate: [5.48246519, 51.51637576],
  },
  {
    code: "0037",
    name: "Stadskanaal",
    geographicCoordinate: [7.01032824, 53.00215291],
  },
  {
    code: "0180",
    name: "Staphorst",
    geographicCoordinate: [6.20708156, 52.63350692],
  },
  {
    code: "0532",
    name: "Stede Broec",
    geographicCoordinate: [5.22685099, 52.70006845],
  },
  {
    code: "0851",
    name: "Steenbergen",
    geographicCoordinate: [4.32362694, 51.59988855],
  },
  {
    code: "1708",
    name: "Steenwijkerland",
    geographicCoordinate: [6.02999074, 52.75554514],
  },
  {
    code: "0971",
    name: "Stein",
    geographicCoordinate: [5.76366653, 50.9689919],
  },
  {
    code: "1904",
    name: "Stichtse Vecht",
    geographicCoordinate: [5.01258714, 52.17694825],
  },
  {
    code: "1900",
    name: "Súdwest-Fryslân",
    geographicCoordinate: [5.46127185, 52.9978609],
  },
  {
    code: "0715",
    name: "Terneuzen",
    geographicCoordinate: [3.83623576, 51.30164475],
  },
  {
    code: "0093",
    name: "Terschelling",
    geographicCoordinate: [5.37477634, 53.33985123],
  },
  {
    code: "0448",
    name: "Texel",
    geographicCoordinate: [4.87378888, 53.07911777],
  },
  {
    code: "1525",
    name: "Teylingen",
    geographicCoordinate: [4.51190063, 52.21399745],
  },
  {
    code: "0716",
    name: "Tholen",
    geographicCoordinate: [4.10635358, 51.57039186],
  },
  {
    code: "0281",
    name: "Tiel",
    geographicCoordinate: [5.40686009, 51.88508236],
  },
  {
    code: "0855",
    name: "Tilburg",
    geographicCoordinate: [5.07736986, 51.58342354],
  },
  {
    code: "0183",
    name: "Tubbergen",
    geographicCoordinate: [6.77667127, 52.4105818],
  },
  {
    code: "1700",
    name: "Twenterand",
    geographicCoordinate: [6.59808108, 52.44788923],
  },
  {
    code: "1730",
    name: "Tynaarlo",
    geographicCoordinate: [6.60437143, 53.09710817],
  },
  {
    code: "0737",
    name: "Tytsjerksteradiel",
    geographicCoordinate: [5.96271652, 53.19986427],
  },
  {
    code: "0450",
    name: "Uitgeest",
    geographicCoordinate: [4.7286079, 52.52342808],
  },
  {
    code: "0451",
    name: "Uithoorn",
    geographicCoordinate: [4.79084592, 52.23690856],
  },
  {
    code: "0184",
    name: "Urk",
    geographicCoordinate: [5.4815978, 52.69314676],
  },
  {
    code: "0344",
    name: "Utrecht",
    geographicCoordinate: [5.0747543, 52.09113798],
  },
  {
    code: "1581",
    name: "Utrechtse Heuvelrug",
    geographicCoordinate: [5.38832134, 52.03471452],
  },
  {
    code: "0981",
    name: "Vaals",
    geographicCoordinate: [5.97584744, 50.77433492],
  },
  {
    code: "0994",
    name: "Valkenburg aan de Geul",
    geographicCoordinate: [5.82387069, 50.86071063],
  },
  {
    code: "0858",
    name: "Valkenswaard",
    geographicCoordinate: [5.44922705, 51.32375174],
  },
  {
    code: "0047",
    name: "Veendam",
    geographicCoordinate: [6.88009225, 53.08898498],
  },
  {
    code: "0345",
    name: "Veenendaal",
    geographicCoordinate: [5.55565707, 52.02367797],
  },
  {
    code: "0717",
    name: "Veere",
    geographicCoordinate: [3.58463765, 51.56259998],
  },
  {
    code: "0861",
    name: "Veldhoven",
    geographicCoordinate: [5.38321431, 51.41185654],
  },
  {
    code: "0453",
    name: "Velsen",
    geographicCoordinate: [4.61352531, 52.45293519],
  },
  {
    code: "0983",
    name: "Venlo",
    geographicCoordinate: [6.15911182, 51.39095482],
  },
  {
    code: "0984",
    name: "Venray",
    geographicCoordinate: [5.96288433, 51.51536765],
  },
  {
    code: "1961",
    name: "Vijfheerenlanden",
    geographicCoordinate: [5.05675527, 51.93598384],
  },
  {
    code: "0622",
    name: "Vlaardingen",
    geographicCoordinate: [4.32785888, 51.91738748],
  },
  {
    code: "0096",
    name: "Vlieland",
    geographicCoordinate: [5.03464993, 53.21969428],
  },
  {
    code: "0718",
    name: "Vlissingen",
    geographicCoordinate: [3.4810497, 51.46533058],
  },
  {
    code: "0986",
    name: "Voerendaal",
    geographicCoordinate: [5.9164942, 50.86936538],
  },
  {
    code: "0626",
    name: "Voorschoten",
    geographicCoordinate: [4.43862798, 52.12482218],
  },
  {
    code: "0285",
    name: "Voorst",
    geographicCoordinate: [6.09911646, 52.22556774],
  },
  {
    code: "0865",
    name: "Vught",
    geographicCoordinate: [5.24260833, 51.65035998],
  },
  {
    code: "1949",
    name: "Waadhoeke",
    geographicCoordinate: [5.61310762, 53.23138399],
  },
  {
    code: "0866",
    name: "Waalre",
    geographicCoordinate: [5.46487011, 51.38729181],
  },
  {
    code: "0867",
    name: "Waalwijk",
    geographicCoordinate: [5.01182154, 51.69232009],
  },
  {
    code: "0627",
    name: "Waddinxveen",
    geographicCoordinate: [4.64070725, 52.0423941],
  },
  {
    code: "0289",
    name: "Wageningen",
    geographicCoordinate: [5.66351011, 51.97347594],
  },
  {
    code: "0629",
    name: "Wassenaar",
    geographicCoordinate: [4.36705193, 52.14106114],
  },
  {
    code: "0852",
    name: "Waterland",
    geographicCoordinate: [5.06324683, 52.44896399],
  },
  {
    code: "0988",
    name: "Weert",
    geographicCoordinate: [5.69169608, 51.23360506],
  },
  {
    code: "0363",
    name: "Amsterdam",
    geographicCoordinate: [5.05804338, 52.30320518],
  },
  {
    code: "1960",
    name: "West Betuwe",
    geographicCoordinate: [5.20935055, 51.86160154],
  },
  {
    code: "0668",
    name: "West Maas en Waal",
    geographicCoordinate: [5.49659321, 51.85162949],
  },
  {
    code: "1969",
    name: "Westerkwartier",
    geographicCoordinate: [6.33802917, 53.21850623],
  },
  {
    code: "1701",
    name: "Westerveld",
    geographicCoordinate: [6.29552663, 52.83819246],
  },
  {
    code: "0293",
    name: "Westervoort",
    geographicCoordinate: [5.97071826, 51.95751823],
  },
  {
    code: "1950",
    name: "Westerwolde",
    geographicCoordinate: [7.12018572, 53.01400111],
  },
  {
    code: "1783",
    name: "Westland",
    geographicCoordinate: [4.21106411, 52.00069805],
  },
  {
    code: "0098",
    name: "Weststellingwerf",
    geographicCoordinate: [6.01768869, 52.87576261],
  },
  {
    code: "1992",
    name: "Voorne aan Zee",
    geographicCoordinate: [4.11864917, 51.90900185],
  },
  {
    code: "0189",
    name: "Wierden",
    geographicCoordinate: [6.56063185, 52.34443901],
  },
  {
    code: "0296",
    name: "Wijchen",
    geographicCoordinate: [5.70191338, 51.81673503],
  },
  {
    code: "1696",
    name: "Wijdemeren",
    geographicCoordinate: [5.08484039, 52.22617925],
  },
  {
    code: "0352",
    name: "Wijk bij Duurstede",
    geographicCoordinate: [5.32525398, 51.98920124],
  },
  {
    code: "0294",
    name: "Winterswijk",
    geographicCoordinate: [6.72586167, 51.96985392],
  },
  {
    code: "0873",
    name: "Woensdrecht",
    geographicCoordinate: [4.34227065, 51.40828991],
  },
  {
    code: "0632",
    name: "Woerden",
    geographicCoordinate: [4.89889927, 52.10693981],
  },
  {
    code: "1690",
    name: "De Wolden",
    geographicCoordinate: [6.37349554, 52.70166684],
  },
  {
    code: "0880",
    name: "Wormerland",
    geographicCoordinate: [4.85512991, 52.5020004],
  },
  {
    code: "0351",
    name: "Woudenberg",
    geographicCoordinate: [5.42663916, 52.08115655],
  },
  {
    code: "0479",
    name: "Zaanstad",
    geographicCoordinate: [4.7730126, 52.46290889],
  },
  {
    code: "0297",
    name: "Zaltbommel",
    geographicCoordinate: [5.1584405, 51.7922907],
  },
  {
    code: "0473",
    name: "Zandvoort",
    geographicCoordinate: [4.53573696, 52.35998559],
  },
  {
    code: "0050",
    name: "Zeewolde",
    geographicCoordinate: [5.45524194, 52.34436139],
  },
  {
    code: "0355",
    name: "Zeist",
    geographicCoordinate: [5.25518558, 52.10196522],
  },
  {
    code: "0299",
    name: "Zevenaar",
    geographicCoordinate: [6.08523451, 51.9241029],
  },
  {
    code: "0637",
    name: "Zoetermeer",
    geographicCoordinate: [4.48977382, 52.06091036],
  },
  {
    code: "0638",
    name: "Zoeterwoude",
    geographicCoordinate: [4.51383216, 52.11480172],
  },
  {
    code: "1892",
    name: "Zuidplas",
    geographicCoordinate: [4.60707151, 51.99887742],
  },
  {
    code: "0879",
    name: "Zundert",
    geographicCoordinate: [4.64261844, 51.47979899],
  },
  {
    code: "0301",
    name: "Zutphen",
    geographicCoordinate: [6.23395013, 52.13634665],
  },
  {
    code: "1896",
    name: "Zwartewaterland",
    geographicCoordinate: [6.06371395, 52.60275257],
  },
  {
    code: "0642",
    name: "Zwijndrecht",
    geographicCoordinate: [4.6058021, 51.82184389],
  },
  {
    code: "0193",
    name: "Zwolle",
    geographicCoordinate: [6.11836361, 52.51868565],
  },
];
