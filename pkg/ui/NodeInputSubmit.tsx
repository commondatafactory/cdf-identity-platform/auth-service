// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import { getNodeLabel } from "@ory/integrations/ui";
import { FormEvent } from "react";
import { Button } from "@commonground/design-system";

import { NodeInputProps } from "./helpers";
import { t } from "../../translations";

export function NodeInputSubmit<T>({
  node,
  attributes,
  setValue,
  disabled,
  dispatchSubmit,
}: NodeInputProps) {
  return (
    <Button
      name={attributes.name}
      onClick={(e: MouseEvent | FormEvent<Element>) => {
        // On click, we set this value, and once set, dispatch the submission!
        setValue(attributes.value).then(() => dispatchSubmit(e));
      }}
      value={attributes.value || ""}
      disabled={attributes.disabled || disabled}
      style={(attributes as any).style}
    >
      {t(getNodeLabel(node))}
    </Button>
  );
}
