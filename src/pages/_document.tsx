// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="nl">
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
