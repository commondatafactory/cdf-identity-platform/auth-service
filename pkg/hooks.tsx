// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { AxiosError } from "axios";
import { useRouter } from "next/router";
import { useState, useEffect, DependencyList } from "react";

import ory from "./sdk";

// Returns a function which will log the user out
export function LogoutLink(deps?: DependencyList) {
  try {
    const [logoutToken, setLogoutToken] = useState<string>("");
    const router = useRouter();

    useEffect(() => {
      ory.frontend
        .createBrowserLogoutFlow()
        .then(({ data }) => {
          setLogoutToken(data.logout_token);
        })
        .catch((err: AxiosError) => {
          switch (err.response?.status) {
            case 401:
              // do nothing, the user is not logged in
              return;
          }

          // Something else happened!
          return Promise.reject(err);
        })
        .catch((error) => {
          console.warn("Error from Kratos:", error);
        });
    }, deps);

    return () => {
      if (logoutToken) {
        ory.frontend
          .updateLogoutFlow({ token: logoutToken })
          .then(() => router.push("/login"))
          .then(() => router.reload());
      }
    };
  } catch (e) {
    console.log(e);
  }
}
