// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

const languageMap = {
  "Property identifier is missing.": "Vul een e-mailadres in.",
  "Property password is missing.": "Vul een wachtwoord in.",
  "Property email is missing.": "Vul je e-mailadres in.",
  '* is not valid "email"': "* is geen valide e-mailadres",
  "The password can not be used because password length must be at least 8 characters but only got *":
    "Gebruik minimaal 8 karakters voor je wachtwoord.",
  "The provided credentials are invalid, check for spelling mistakes in your password or username, email address, or phone number.":
    "Controleer of je het juiste e-mailadres en wachtwoord gebruikt hebt. Wachtwoord kwijt? Ga naar 'wachtwoord vergeten'.",
  "Sign in": "Log in",
  "Sign in with *": "Log in via *",
  ID: "E-mailadres",
  Password: "Wachtwoord",
  "Sign up": "Registeren",
  "Sign up with *": "Koppel je account via *",
  "Property * is missing.": "Er ontbreekt een veld.",
  "First name": "Voornaam",
  "Last name": "Achternaam",
  "The password can not be used because the password has been found in data breaches and must no longer be used.":
    "Dit wachtwoord kan niet gebruikt worden, omdat het voorkomt in een lijst met wachtwoorden gebruikt bij hack-pogingen.",
  "An account with the same identifier (email, phone, username, ...) exists already.":
    "Een account met dit e-mailadres bestaat al.",
  "An email containing a recovery link has been sent to the email address you provided.":
    "Er word een e-mail verstuurd naar dit e-mailadres.",
  Submit: "Versturen",
  Email: "E-mailadres",
  "E-Mail": "E-mailadres",
  "The recovery token is invalid or has already been used. Please retry the flow.":
    "Er is via deze link al (geprobeerd) een wachtwoord te herstellen. Vraag opnieuw een link aan.",
  Save: "Opslaan",
  "You successfully recovered your account. Please change your password or set up an alternative login method (e.g. social sign in) within the next * minutes.":
    "Je kan hier binnen 15 minuten je gegevens, waaronder je wachtwoord aanpassen en updaten.",
  "The password can not be used because the password is too similar to the user identifier.":
    "Gebruik een ander wachtwoord dat minder lijkt op het e-mailadres.",
  "Account not active yet. Did you forget to verify your email address?":
    "Dit account is niet geverifieërd. Wil je je account opnieuw verifieëren? Druk hieronder op 'Account verifiëren' en verifieër je account.",
  "Your changes have been saved!": "Je aanpassingen zijn opgeslagen!",
  "A valid session was detected and thus login is not possible. Did you forget to set `?refresh=true`?":
    "Je lijkt al ingelogd te zijn. Probeer de pagina te verversen.",
  "Please confirm this action by verifying that it is you.":
    "Bevestig je account door je wachtwoord opnieuw in te vullen:",
  "Verify code": "Verifieer code:",
  "This is your authenticator app secret. Use it if you can not scan the QR code.":
    "This is your authenticator app secret. Use it if you can not scan the QR code.",
  "The provided authentication code is invalid, please try again.":
    "De opgegeven authenticatiecode is ongeldig. Probeer het opnieuw.",
  "Unlink TOTP authenticator App": "Twee-factor-authenticatie loskoppelen",
  "Please complete the second authentication challenge.":
    "Log in met twee-factor-authenticatie:",
  "Authentication code": " ",
  "Use Authenticator": "Authenticatie code verifiëren",
  "The code was already used. Please request another code.":
    "Deze code is al gebruikt. Vraag een nieuwe code aan.",
  "The recovery code is invalid or has already been used. Please try again.":
    "Deze code is niet valide of al een keer gebruikt. Probeer het opnieuw.",
  "length must be >= 6, but got *": "De code is niet valide.",
  "This identity conflicts with another identity that already exists.":
    "deze gebruiker heeft al een account.",
  "An email containing a verification code has been sent to the email address you provided. If you have not received an email, check the spelling of the address and make sure to use the address you registered with.":
    "Een e-mail met een verificatiecode is verzonden naar het door jou opgegeven e-mailadres. Als je geen e-mail hebt ontvangen, controleer dan de spelling van het adres en zorg ervoor dat je het adres gebruikt waarmee je je hebt geregistreerd.",
  "Verification code": "Verificatiecode",
  "Resend code": "Code opnieuw sturen",
  "The verification code is invalid or has already been used. Please try again.":
    "De verificatiecode is niet geldig, of is al een keer gebruikt. Probeer het opnieuw.",
  "You successfully verified your email address.":
    "Je hebt je account succesvol geverifieerd.",
  Continue: "Ga door",
  continue: "Ga door",
  "Go back": "Ga terug",
  "The request was already completed successfully and can not be retried.":
    "De verificatie is succesvol voltooid.",
};

const textToArray = (string): string[] => string?.trim().split(" ") || [];

export const t = (text: string) => {
  if (!text) {
    console.log("text niet gevonden");
    return "";
  }

  const textArray = textToArray(text);

  const directlyTranslate = () => {
    return Object.entries(languageMap).find(
      (translation) => text.toLowerCase() === translation[0].toLowerCase()
    )?.[1];
  };

  const removeWord = () => {
    const translationKeyValue = Object.entries(languageMap).find(
      (translation) => {
        if (!translation[0].includes("//")) return;

        const wordIndex = textToArray(translation[0]).indexOf("*");
        const filteredText = textArray
          .filter((string, i) => i !== wordIndex)
          .join(" ");

        return filteredText === translation[0].replace("//", "").trim();
      }
    );
    return translationKeyValue?.[1].replace(" //", "");
  };

  const replaceWord = () => {
    let word = "";
    Object.entries(languageMap).some((translation) => {
      if (!translation[0].includes("*")) return;

      const wordIndex = textToArray(translation[0]).indexOf("*");
      const filteredText = textArray.filter((string, i) => i !== wordIndex);

      if (
        filteredText.join(" ") ===
        textToArray(translation[0])
          .filter((trans, i) => i !== wordIndex)
          .join(" ")
      ) {
        const translatedTextArray = textToArray(translation[1]).map((trans) =>
          trans.includes("*") ? textArray[wordIndex] : trans
        );
        word = translatedTextArray.join(" ");

        return true;
      }
    });

    return word;
  };

  const removedWord = removeWord();
  if (removedWord) return removedWord;

  const replacedWord = replaceWord();
  if (replacedWord) return replacedWord;

  const directTranslation = directlyTranslate();
  if (directTranslation) return directTranslation;

  return text;
};
