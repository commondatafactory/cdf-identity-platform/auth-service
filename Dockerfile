FROM nginx:alpine AS web

# Add bash
RUN apk add --no-cache bash tree

# Copy nginx configuration
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/nginx.conf

# yarn build:dev fills the out folder
COPY out /usr/share/nginx/html

# Add non-privileged user
RUN adduser -D -u 1001 appuser

# Set ownership nginx.pid and cache folder in order to run nginx as non-root user
RUN touch /var/run/nginx.pid && \
    chown -R appuser /var/run/nginx.pid && \
    chown -R appuser /var/cache/nginx

ENV NEXT_PUBLIC_DEGO_URL=http://map.example.com
ENV NEXT_PUBLIC_DOOK_URL=http://map.example.com
ENV NEXT_PUBLIC_AUTH_API_URL=http://login.example.com
ENV NEXT_PUBLIC_ANALYST_LOGIN_URL=http://login.example.com/login
ENV NEXT_PUBLIC_ADMIN_LOGIN_URL=http://login.example.com/admin-login



USER appuser

# Start Nginx server
EXPOSE 8080
CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
