// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import * as Styled from "./Error.styled";

export const Error = () => {
  return (
    <>
      <Styled.Title>Er is iets misgegaan.</Styled.Title>
      <Styled.Description>
        🤷 Probeer het later nog eens. Of Mail naar{" "}
        <a href="mailto:vip@vng.nl">vip@vng.nl</a> om hier een melding van te
        maken of/en ondersteuning te krijgen.
      </Styled.Description>
    </>
  );
};
