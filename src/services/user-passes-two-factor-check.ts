// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { CurrentUserSession } from "../types";

export const isCurrentUserTwoFactorConfigured = async (): Promise<{
  password: string;
  totp: string;
} | null> => {
  const response = await fetch(
    `${process.env.NEXT_PUBLIC_AUTH_API_URL}/users/configured-credentials`,
    {
      credentials: "include",
    }
  );

  if (response.ok) {
    return await response.json();
  }

  return null;
};

export const isUserTwoFactorAuthenticated = ({
  user,
}: {
  user: CurrentUserSession;
}): boolean => {
  return user?.authenticator_assurance_level === "aal2";
};

export const isUserTwoFactorRequired = ({
  user,
}: {
  user: CurrentUserSession;
}): boolean =>
  user?.permissions?.some(
    ({ namespace, object }) =>
      namespace === "organizations" && object !== "dego"
  );

// TODO: name implies user is 2f authenticated
export const isUserPassing2FA = ({
  user,
}: {
  user: CurrentUserSession;
}): boolean => {
  if (isUserTwoFactorRequired({ user })) {
    return isUserTwoFactorAuthenticated({ user });
  }
  return true;
};
