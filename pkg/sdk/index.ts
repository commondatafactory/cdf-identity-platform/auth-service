// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

// https://www.ory.sh/docs/guides/upgrade/sdk-v1

import {
  Configuration,
  // OAuth2Api,
  IdentityApi,
  FrontendApi,
} from "@ory/kratos-client";

const config = new Configuration({
  basePath: process.env.NEXT_PUBLIC_KRATOS_PUBLIC_URL,
  baseOptions: {
    withCredentials: true,
    timeout: 12000, // 12 seconds
  },
});

export default {
  // identity: new IdentityApi(config),
  frontend: new FrontendApi(config),
  // oauth2: new OAuth2Api(config),
};
