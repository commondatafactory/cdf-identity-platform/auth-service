// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
//

import { AxiosError } from "axios";
import { Button, Spinner } from "@commonground/design-system";
import { useEffect, useState } from "react";
import { NextPage } from "next";
import { useRouter } from "next/router";
import Head from "next/head";
import Input from "@commonground/design-system/dist/components/Form/TextInput/Input";
import {
  UpdateVerificationFlowBody,
  VerificationFlow,
} from "@ory/kratos-client";

import { ProgressIndicators } from "../components/progress-indicators/ProgressIndicators";
import { verifyProgressions } from "../services/page-progressions";
import { handleFlowError } from "../../pkg/errors";
import { isEmailValid } from "../services/check-email-validity";
import { useToaster } from "../providers/ToasterProvider";
import ory from "../../pkg/sdk";
import { Flow } from "../../pkg";

import * as Styled from "../components/layout/OtherLink.styled";
import * as StyledLayout from "../components/layout/SmallFormContainer.styled";

const Page: NextPage<any> = () => {
  const [flow, setFlow] = useState<VerificationFlow>();
  const [hasSentRecoveryEmail, setHasSentRecoveryEmail] = useState(false);
  const [formError, setFormError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const { showToast } = useToaster();

  // Get ?flow=... from the URL
  const router = useRouter();
  const { flow: flowId, return_to: returnTo } = router.query;

  useEffect(() => {
    // If the router is not ready yet, or we already have a flow, do nothing.
    if (!router.isReady || flow) {
      return;
    }

    // If ?flow=.. was in the URL, we fetch it
    if (flowId) {
      ory.frontend
        .getVerificationFlow({ id: String(flowId) })
        .then(({ data }) => {
          setFlow(data);
        })
        .catch(handleFlowError(router, "recovery", setFlow));
      return;
    }

    // Otherwise we initialize it
    ory.frontend
      .createBrowserVerificationFlow({
        returnTo: String(returnTo || ""),
      })
      .then(({ data }) => {
        setFlow(data);
      })
      .catch(handleFlowError(router, "recovery", setFlow))
      .catch((err: AxiosError) => {
        if (err.response?.status === 400) {
          setFlow(err.response?.data);
          return;
        }

        return Promise.reject(err);
      });
  }, [flowId, router, router.isReady, returnTo, flow]);

  const onSubmit = async (values: UpdateVerificationFlowBody) => {
    await router
      // On submission, add the flow ID to the URL but do not navigate. This prevents the user loosing
      // their data when they reload the page.
      .push(`/verification?flow=${flow?.id}`, undefined, { shallow: true });

    ory.frontend
      .updateVerificationFlow({
        flow: String(flow?.id),
        updateVerificationFlowBody: values,
      })
      .then(({ data }) => {
        // Form submission was successful, show the message to the user!
        setFlow(data);
      })
      .catch((err: AxiosError) => {
        switch (err.response?.status) {
          case 400:
            // Status code 400 implies the form validation had an error
            setFlow(err.response?.data);
            return;
          case 410:
            const newFlowID = err.response.data.use_flow_id;
            router
              // On submission, add the flow ID to the URL but do not navigate. This prevents the user loosing
              // their data when they reload the page.
              .push(`/verification?flow=${newFlowID}`, undefined, {
                shallow: true,
              });

            ory.frontend
              .getVerificationFlow({ id: newFlowID })
              .then(({ data }) => setFlow(data));
            return;
        }

        throw err;
      });
  };

  return (
    <>
      <Head>
        <title>Account verificatie</title>
        <meta name="description" content="Account" />
      </Head>

      <ProgressIndicators
        progressions={verifyProgressions}
        currentprogress={0}
      />

      <StyledLayout.FormContainerCentered>
        <h1>Verifieër je account</h1>

        {(flow as any)?.error && <>{(flow as any).error.reason}</>}

        <Flow onSubmit={onSubmit} flow={flow} />

        {isLoading && <Spinner style={{ marginTop: "16px" }} />}

        <Styled.OtherLink href="/login" style={{ marginTop: "16px" }}>
          Ga naar de login pagina
        </Styled.OtherLink>
      </StyledLayout.FormContainerCentered>
    </>
  );
};

export default Page;
