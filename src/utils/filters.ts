// Copyright © VNG Realisatie 2024
// Licensed under the EUPL
//

import authModel from "../../authorization-model.json";

export const filterOnDuplicateKeys =
  (keys: string[]) => (original, index, array) =>
    index ===
    array.findIndex((copy) => keys.every((key) => copy[key] === original[key]));

export const filterOnActiveFirst = (permission, index, array) =>
  !(
    permission.relation === "issuer" &&
    array.some(
      (copy) => copy.object === permission.object && copy.relation === "access"
    )
  );

export const filterOnMatchingActiveOrganization =
  (userActiveOrganization: string) =>
  ({ roleId, object }: { roleId: string; object: string }) =>
    `${roleId}_${userActiveOrganization}` === object;

export const getSubdivisionsForActiveOrganization = (
  activeOrganization: string,
  permissions: any[]
) =>
  (activeOrganization ? permissions ?? [] : [])
    .filter(filterOnMatchingActiveOrganization(activeOrganization))
    .filter(
      (permission) =>
        permission.namespace === authModel.subdivisions.namespace &&
        permission.relation === authModel.subdivisions.relations.issuer
    )
    .filter(filterOnDuplicateKeys(["object"]));
